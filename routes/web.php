<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('Quest','\App\Http\Controllers\QuestController@show');
Route::post('Quest','\App\Http\Controllers\QuestController@store');
Route::get('unsubscribe/Quest','\App\Services\Orders\Controllers\CustomerController@Unsubscribe');


Route::prefix('admin')->group(function () {
  Route::get('/','\App\Http\Controllers\AdminController@stats')->name('admin.homepage');
  Route::get('login','\App\Http\Controllers\Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('login','\App\Http\Controllers\Auth\AdminLoginController@login')->name('admin.login.submit');
  Route::get('stats','\App\Http\Controllers\AdminController@stats')->name('admin.homepage');
  Route::get('details','\App\Http\Controllers\AdminController@details');
  Route::get('comments','\App\Http\Controllers\AdminController@comments');
  Route::get('mails','\App\Http\Controllers\AdminController@mails');
  Route::post('mails','\App\Http\Controllers\AdminController@storemails');
  Route::get('cancel','\App\Http\Controllers\AdminController@getCanceledQuests');
  Route::post('cancel','\App\Http\Controllers\AdminController@CancelQuest');
  Route::get('quests','\App\Http\Controllers\AdminController@Quests');
  Route::post('quests','\App\Http\Controllers\AdminController@addQuests');
  Route::get('advanced','\App\Http\Controllers\AdminController@advSearch');
  Route::post('advanced','\App\Http\Controllers\AdminController@PostadvSearch');
  Route::get('settings','\App\Http\Controllers\AdminController@settings');
});

Route::prefix('user')->group(function () {
  Route::get('/','\App\Http\Controllers\UserController@stats')->name('user.homepage');
  Route::get('stats','\App\Http\Controllers\UserController@stats')->name('user.homepage');
  Route::get('details','\App\Http\Controllers\UserController@details');
  Route::get('comments','\App\Http\Controllers\UserController@comments');
});

Route::prefix('mod')->group(function () {
  Route::get('/','\App\Http\Controllers\ModeratorController@stats')->name('mod.homepage');
  Route::get('stats','\App\Http\Controllers\ModeratorController@stats')->name('mod.homepage');
  Route::get('details','\App\Http\Controllers\ModeratorController@details');
  Route::get('comments','\App\Http\Controllers\ModeratorController@comments');
});

Route::get('/',function(){
  return redirect('/home');
});

 Route::get('/home', 'HomeController@index')->name('home');

 Route::get('/redirect', 'HomeController@redirect_user')->name('redirect');


 // Route::get('/test',function(){
 //    return  $send = app()->make('MailsToUsers')->sendMails('403574495');
 // });
