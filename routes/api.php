<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



  //Api services
  Route::group(['prefix'=>'services'],function(){
    Route::post('sendUserMails','\App\Services\Mail\Controllers\mailsController@sendUserMails');
    Route::post('cancel','\App\Services\Mail\Controllers\mailsController@CancelledOrder');
    Route::post('sendTest','\App\Services\Mail\Controllers\mailsController@SendTest');
  });


  // Route::get('UpdateProducts','ApiDataCreate@UpdateProducts');

  //kopsimo gia ligo
  Route::post('orders','\App\Services\Orders\Controllers\OrderController@store');

// Route::post('order','\App\Services\Orders\Controllers\OrderController@show');



  Route::post('Questionnaires','\App\Services\Questionnaires\Controllers\QuestControl@retrieve');
  Route::get('checkMails','\App\Services\Mail\Controllers\mailsController@checkMails');


  //Admin Api stats

  Route::post('aggregated','\App\Http\Controllers\AdminApiStatsController@aggregated');
  Route::post('detailedData','\App\Http\Controllers\AdminApiStatsController@detailedData');
  Route::post('commentsData','\App\Http\Controllers\AdminApiStatsController@Comments');
  Route::post('RestStats','\App\Http\Controllers\AdminApiStatsController@RestaurantsStats');


  // Route::post('AdminRestaurants','\App\Http\Controllers\AdminApiStatsController@AdminRestaurants');

//Admin api commands Routes
  Route::group(['prefix'=>'admin','middleware'=>'auth:api'],function(){
    //User Calls
    Route::post('RetrieveUsers','\App\Http\Controllers\AdminApiController@RetrieveUsers');
    Route::post('RetrieveAdmins','\App\Http\Controllers\AdminApiController@RetrieveAdmins');
    Route::post('RetrieveMods','\App\Http\Controllers\AdminApiController@RetrieveMods');
    Route::post('AddUserRestaurants','\App\Http\Controllers\AdminApiController@AddUserRestaurants');
    Route::post('DeleteUserRestaurants','\App\Http\Controllers\AdminApiController@DeleteUserRestaurants');
    Route::post('ConfirmUser','\App\Http\Controllers\AdminApiController@ConfirmUser');
    Route::post('EnableUser','\App\Http\Controllers\AdminApiController@EnableUser');
    Route::post('DisableUser','\App\Http\Controllers\AdminApiController@DisableUser');
    Route::post('AddUserEmails','\App\Http\Controllers\AdminApiController@AddUserEmails');
    Route::post('RemoveUserEmails','\App\Http\Controllers\AdminApiController@DeleteUserEmails');
    Route::post('Restaurants','\App\Http\Controllers\AdminApiController@Restaurants');
    Route::post('EnableArea','\App\Http\Controllers\AdminApiController@EnableArea');
    Route::post('DisableArea','\App\Http\Controllers\AdminApiController@DisableArea');
    Route::post('EnableMod','\App\Http\Controllers\AdminApiController@EnableMod');
    Route::post('MakeAdmin','\App\Http\Controllers\AdminApiController@MakeAdmin');
    Route::post('ChangeUserNotify','\App\Http\Controllers\AdminApiController@ChangeUserNotify');
    Route::post('Categories','\App\Http\Controllers\AdminApiStatsController@CategoryPicker');
    //Emails for Restaurants Calls
    Route::post('RetrieveRestEmails','\App\Http\Controllers\AdminApiController@RetrieveRestEmails');
    Route::post('CreateRestEmail','\App\Http\Controllers\AdminApiController@CreateRestEmail');
    Route::post('UpdateRestEmail','\App\Http\Controllers\AdminApiController@UpdateRestEmail');
    Route::post('ChangeRestNotify','\App\Http\Controllers\AdminApiController@ChangeRestNotify');
    //Questions Calls
    Route::post('AddQuestion','\App\Services\QuestionnaireControl\Controllers\TempControl@AddQuestion');
    Route::post('DeleteQuestion','\App\Services\QuestionnaireControl\Controllers\TempControl@DeleteQuestion');
    Route::post('EnableQuestion','\App\Services\QuestionnaireControl\Controllers\TempControl@EnableQuestion');
    Route::post('EditQuestion','\App\Services\QuestionnaireControl\Controllers\TempControl@EditQuestion');
    Route::post('RetrieveQuestions','\App\Services\QuestionnaireControl\Controllers\TempControl@Retrieve');
    Route::post('Migrator','\App\Services\QuestionnaireControl\Controllers\TempControl@Migrator');
    //  Route::post('advancedData','\App\Http\Controllers\ApiStatsController@Advanced');
  });

//User Api stats
Route::group(['prefix'=>'user','middleware'=>'auth:api'],function(){
  Route::post('aggregated','\App\Http\Controllers\UserApiStatsController@aggregated');
  Route::post('detailedData','\App\Http\Controllers\UserApiStatsController@detailedData');
  Route::post('commentsData','\App\Http\Controllers\UserApiStatsController@Comments');
  //Route::post('RestStats','\App\Http\Controllers\UserApiStatsController@RestaurantsStats');
  Route::post('UserRestaurants','\App\Http\Controllers\UserApiStatsController@UserRestaurants');

});
