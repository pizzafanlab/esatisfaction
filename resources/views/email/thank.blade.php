<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <title>Pizza Fan</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <link href="/css/questionnaire/layout.css" rel="stylesheet">
        <link href="/css/questionnaire/responsive.css" rel="stylesheet">
        <link href="/css/questionnaire/fonts.css" rel="stylesheet">
        <link href="/css/questionnaire/swiper.css" rel="stylesheet">
        <link href="/css/questionnaire/home.css" rel="stylesheet">
        <link href="/css/questionnaire/menu.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/questionnaire/smart-app-banner.css" type="text/css">
    </head>
    <body>
      <div class="wide_header_wrapper pf-home_nav">
        <div class="upperHeader">
        </div>
          <div class="main_wrapper">
              <div class="navbar-header center">
                  <a class="navbar-brand inlineBlock" href="https://www.pizzafan.gr/el" title="ΑΡΧΙΚΗ">
              <img src="/images/questionnaire/logoHor.png" alt="" class="desktop">
                      <img src="/images/questionnaire/logo_mobile.png" alt="Pizza Fan Logo" class="mobile moblogo">
                  </a>
              </div>

          <div style="clear: both;"></div>
          </div>
      </div>
        <div class="container" align="center">
          <h1>Σας ευχαριστούμε πολύ για την συμπλήρωση του ερωτηματολογίου</h1>
        </div>


    </body>
</html>
