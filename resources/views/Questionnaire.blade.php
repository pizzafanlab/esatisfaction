<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <title>Pizza Fan</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <!-- <link rel="stylesheet" href="./Pizza Fan _ Νο.1 Πίτσα Delivery στην Ελλάδα_files/font-awesome.min.css"> -->
        <link href="/css/questionnaire/layout.css" rel="stylesheet">
        <link href="/css/questionnaire/responsive.css" rel="stylesheet">
        <link href="/css/questionnaire/fonts.css" rel="stylesheet">
        <link href="/css/questionnaire/swiper.css" rel="stylesheet">
        <link href="/css/questionnaire/home.css" rel="stylesheet">
        <link href="/css/questionnaire/menu.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/questionnaire/smart-app-banner.css" type="text/css">
    </head>
    <body>
      <div class="wide_header_wrapper pf-home_nav">
      	<div class="upperHeader">
      	</div>
          <div class="main_wrapper">
              <div class="navbar-header center">
                  <a class="navbar-brand inlineBlock" href="https://www.pizzafan.gr/el" title="ΑΡΧΙΚΗ">
      				<img src="/images/questionnaire/logoHor.png" alt="" class="desktop">
                      <img src="/images/questionnaire/logo_mobile.png" alt="Pizza Fan Logo" class="mobile moblogo">
                  </a>
              </div>

      		<div style="clear: both;"></div>
          </div>
      </div>
        <div class="container" align="center">
        <h1>{{$quest->data->intro->title}}<span> </span>{{$quest->data->intro->restaurant}}</h1>
      </div><p>
        <div class="container" align="center">
          <form method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{$quest->data->quest_id}}">
            <input type="hidden" name="hash" value="{{$quest->data->hash}}">

              @foreach($quest->data->questions->products as $Prodquest)
            <div class="row">
                  <div class="col-sm">
                    <label>{{$Prodquest->questlabel}}<span> </span>{{$Prodquest->prod_name}}</label>
                    <select class="form-control" name='prodSelect[]' required>
                      <option value="" selected disabled hidden></option>
                      <option value="5">Πολύ καλό</option>
                      <option value="4">Καλό</option>
                      <option value="3">Μέτριο</option>
                      <option value="2">Πολύ κακό</option>
                      <option value="1">Κακό</option>
                    </select>
                  </div>
                  <div class="col-sm">
                    <label>{{$Prodquest->commlabel}}</label>
                    <input type="text" class="form-control" name="prodtext[]">
                  </div>
            </div>
              @endforeach
              <hr>
              <div class="row">
                @foreach($quest->data->questions->general as $Genquest)
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>{{$Genquest->questlabel}}</label>
                    @if(isset($Genquest->_id))
                      @if($Genquest->flag == 'satisf')
                      <select class="form-control" name='generalSelect[{{$Genquest->_id}}]' required>
                        <option value="" hidden></option>
                      @else
                      <select class="form-control" name='generalSelect[{{$Genquest->_id}}]'>
                        <option value="0" selected  hidden></option>
                      @endif
                    @else
                      @if($Genquest->flag == 'satisf')
                      <select class="form-control" name='generalSelect[]' required>
                        <option value="" hidden></option>
                      @else
                      <select class="form-control" name='generalSelect[]'>
                        <option value="0" selected  hidden></option>
                      @endif
                    @endif
                      <option value="10">10</option>
                      <option value="9">9</option>
                      <option value="8">8</option>
                      <option value="7">7</option>
                      <option value="6">6</option>
                      <option value="5">5</option>
                      <option value="4">4</option>
                      <option value="3">3</option>
                      <option value="2">2</option>
                      <option value="1">1</option>
                    </select>
                  </div>
                </div>
                @endforeach
              </div>
              <hr>
            <div class="row">
              @foreach($quest->data->questions->indicators as $Indquest)
              <div class="col-sm-3">
                <div class="form-group">
                  <label>{{$Indquest->questlabel}}</label>
                  @if(isset($Genquest->_id))
                  <select class="form-control" name='indicatorsSelect[{{$Indquest->_id}}]'>
                  @else
                  <select class="form-control" name='indicatorsSelect[]'>
                  @endif
                    <option value="0" selected hidden></option>
                    <option value="10">10</option>
                    <option value="9">9</option>
                    <option value="8">8</option>
                    <option value="7">7</option>
                    <option value="6">6</option>
                    <option value="5">5</option>
                    <option value="4">4</option>
                    <option value="3">3</option>
                    <option value="2">2</option>
                    <option value="1">1</option>
                  </select>
                </div>
              </div>
              @endforeach
            </div>
            <hr>

            <div class="row">
              @if ($quest->data->order_type == 'take')
              <div class="col-sm-6">
                <div class="form-group">
                  <label>{{$quest->data->questions->method->questlabel}}</label>
                  <input type="textarea" class="form-control" name='takeComment'>
                </div>
              </div>
              @else
            <div class="col-sm-6">
              <div class="form-group">
                <label>{{$quest->data->questions->method->questlabel}}</label>
                <select class="form-control"  name='delSelect' >
                  <option value="0" selected hidden></option>
                  <option value="10">10</option>
                  <option value="9">9</option>
                  <option value="8">8</option>
                  <option value="7">7</option>
                  <option value="6">6</option>
                  <option value="5">5</option>
                  <option value="4">4</option>
                  <option value="3">3</option>
                  <option value="2">2</option>
                  <option value="1">1</option>
                </select>
              </div>
              </div>
              <div class="col-sm-6">
              <div class="form-group">
                <label>{{$quest->data->questions->method->commlabel}}</label>
              <input type="textarea" class="form-control" name='delComment'>
              </div>
            </div>
              @endif
            </div>
              <div class="form-group">
                <label>{{$quest->data->questions->comment->commentlabel}}</label>
              <input type="textarea" class="form-control" name='genComment'>
              </div>
              <button type="submit" class="btn btn-default">Αποθήκευση</button><p>
              <div class="row">
                <div class="col-sm">
                  @if(!empty($errors))
                  @foreach($errors as $error)
                  <div class="alert alert-info">{{$error}}</div>
                  @endforeach
                  @endif
                </div>
              </div>

          </form>
        </div>
    </body>
</html>
