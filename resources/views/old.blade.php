<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <title>Pizza Fan</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <link href="{{url('/css/questionnaire/layout.css')}}" rel="stylesheet">
        <link href="{{url('/css/questionnaire/responsive.css')}}" rel="stylesheet">
        <link href="{{url('/css/questionnaire/fonts.css')}}" rel="stylesheet">
        <link href="{{url('/css/questionnaire/swiper.css')}}" rel="stylesheet">
        <link href="{{url('/css/questionnaire/home.css')}}" rel="stylesheet">
        <link href="{{url('/css/questionnaire/menu.css')}}" rel="stylesheet">
        <link href="{{url('/css/questionnaire/menu.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{url('/css/questionnaire/smart-app-banner.css')}}" type="text/css">
    </head>
    <body>
      <div class="wide_header_wrapper pf-home_nav">
        <div class="upperHeader">
        </div>
          <div class="main_wrapper">
              <div class="navbar-header center">
                  <a class="navbar-brand inlineBlock" href="https://www.pizzafan.gr/el" title="ΑΡΧΙΚΗ">
                  <img src="{{url('/images/questionnaire/logoHor.png')}}" alt="" class="desktop">
                  <img src="{{url('/images/questionnaire/logo_mobile.png')}}" alt="Pizza Fan Logo" class="mobile moblogo">
                  </a>
              </div>

          <div style="clear: both;"></div>
          </div>
      </div>
        <div class="container" align="center">
        <h1>{{$quest->data->intro->title}}<span> </span>{{$quest->data->intro->restaurant}}</h1>
        <h2>Παραγγελία: {{$quest->data->order_id}}<span> </span>Ημερομηνία: {{$date}}</h2>
      </div><p>
        <div class="container" align="center">
          <div class="row">

              <div class="col-sm-8">
                <h3>Σχόλιο:</h3>
              </div>
                <div class="col-sm-3">
                  <b>
                    @if($quest->data->questions->comment->comment == null)
                    -
                    @else
                    Απάντηση: {{$quest->data->questions->comment->comment}}
                    @endif
                    </b>
                </div>

          </div>







            @foreach($quest->data->questions->products as $Prodquest)
            <div class="row">
              <div class="col-sm-8">
                <label>{{$Prodquest->questlabel}}<span> </span>{{$Prodquest->prod_name}}</label>
              </div>
              <div class="col-sm-3">
                @if($Prodquest->value == 0)
                -
                @else
                Απάντηση: {{$Prodquest->value}}/5
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-sm-8">
                <label>{{$Prodquest->commlabel}}</label>
              </div>
              <div class="col-sm-3">
                @if($Prodquest->comment == null)
                -
                @else
                {{$Prodquest->comment}}
                @endif
              </div>
            </div>
            @endforeach
      </div>
        <div class="container" align="center">
            @foreach($quest->data->questions->general as $Genquest)
            <div class="row">
              <div class="col-sm-8">
              <label>{{$Genquest->questlabel}}</label>
              </div>
              <div class="col-sm-3">
                @if($Genquest->value == 0)
                -
                @else
                Απάντηση: {{$Genquest->value}}
                @endif
              </div>
            </div>
            @endforeach
            </div>
        <div class="container" align="center">
            @foreach($quest->data->questions->indicators as $Indquest)
            <div class="row">
              <div class="col-sm-8">
              <label>{{$Indquest->questlabel}}</label>
              </div>
              <div class="col-sm-3">
                @if($Indquest->value == 0)
                -
                @else
                Απάντηση: {{$Indquest->value}}
                @endif
              </div>
            </div>
            @endforeach
          </div>
        <div class="container" align="center">
            @if ($quest->data->order_type == 'take')
            <div class="row">
              <div class="col-sm-8">
                <label>{{$quest->data->questions->method->questlabel}}</label>
              </div>
              <div class="col-sm-3">
                @if($quest->data->questions->method->comment == null)
                -
                @else
                Απάντηση: {{$quest->data->questions->method->comment}}
                @endif
                </div>
            </div>
            @else
            <div class="row">
              <div class="col-sm-8">
              <label>{{$quest->data->questions->method->questlabel}}</label>
              </div>
              @if($quest->data->questions->method->value == 0)
              <div class="col-sm-3">-</div>
              @else
              <div class="col-sm-3">{{$quest->data->questions->method->value}}</div>
              @endif
            </div>
            <div class="row">
              <div class="col-sm-8">
              <label>{{$quest->data->questions->method->commlabel}}</label>
              </div>

              <div class="col-sm-3">
                @if($quest->data->questions->method->comment == null)
                -
                @else
                Απάντηση: {{$quest->data->questions->method->comment}}
                @endif
                </div>
            </div>
            @endif


          </div>
    </body>
</html>
