@extends('user.layouts.home')
@section('content')
<script type="text/javascript" src="/js/stats/stats.js"></script>
<script type="text/javascript" src="/js/customLibrs/charts.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
RestaurantPicker();
  });

  function RestaurantPicker() {
    var $form = $( '#Restaurants' ),
    url = '/api/user/UserRestaurants';
    var posting = $.post( url, $form.serialize() );
    posting.done(function( data ) {
      $.each(data.restaurants, function( index, value ) {
        var option = $("<option></option>").attr('value',index);
          option.text(value);
          $("#restPicker").append(option);
      });
    });
  }
</script>
<div class="container">
  <div class="row">
    <div class="col-sm">
      <form class="Restaurants" action="/" method="post" id="Restaurants">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="api_token" value="{{$api_token}}">
      </form>
      <form action="/api/user/aggregated" method="post" name="dateSearch" id="dateSearch">
        <input type="hidden" name="api_token" value="{{$api_token}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <!-- <input type="hidden" name="restaurant" value="12"> -->
        <div class="row">
          <div class="col-sm">
            <div class="form-group">
            <label for="startDate">Από:</label>
            <input type="date" class="form-control datepicker" name="startDate" id="startDate" required>
            </div>
          </div>
          <div class="col-sm">
            <div class="form-group">
              <label for="endDate">Έως:</label>
              <input type="date" class="form-control datepicker" name="endDate" id="endDate" required>
            </div>
          </div>
          <div class="col-sm">
            <div class="form-group">
              <label for="restPicker">Εστιατόριο:</label>
                <select class="form-control" id="restPicker" name="restaurant" required>
                  <option disabled selected value></option>
                </select>
             </div>
          </div>
          <div class="col-sm">
            <div class="form-group">
              <label for="customdate">Περιορισμένο εύρος</label>
               <select class="form-control" value='Σήμερα' id="customdate">
                 <option disabled selected value>Επιλογή</option>
                 <option value="1">Σήμερα</option>
                 <option value="2">Χθές</option>
                 <option value="3">Τελευταίες 7 ημέρες</option>
                 <option value="4">Αυτόν τον μήνα</option>
                 <option value="5">Τον προηγούμενο μήνα</option>
               </select>
             </div>
          </div>
        </div>
          <div class="row" id="compareBlock">
            <div class="col-sm">
              <div class="form-group">
                <label for="CompstartDate">Από:</label>
                <input type="date" class="form-control" name="CompstartDate" id="CompstartDate">
              </div>
            </div>
            <div class="col-sm">
              <div class="form-group">
                <label for="CompendDate">Έως:</label>
                <input type="date" class="form-control" name="CompendDate" id="CompendDate">
              </div>
            </div>
            <div class="col-sm">
              <div class="form-group">
                <label for="compareRest">Εστιατόριο:</label>
                <input type="text"  class="form-control" value="Δίκτυο" disabled id="compareRest">
               </div>
            </div>
            <div class="col-sm">
              <div class="form-group">
                <label for="customdate">Περιορισμένο εύρος</label>
                 <select class="form-control" value='Σήμερα' id="customCompdate">
                  <option disabled selected value>Επιλογή</option>
                   <option value="1">Σήμερα</option>
                   <option value="2">Χθές</option>
                   <option value="3">Τελευταίες 7 ημέρες</option>
                   <option value="4">Αυτόν τον μήνα</option>
                   <option value="5">Τον προηγούμενο μήνα</option>
                 </select>
               </div>
            </div>
          </div>
          <input type="hidden" name="compare" value="false">
        <div class="row">
          <div class="col-sm-4">
            <label for="CompareVisibility">Σύγκριση</label>
            <input type="checkbox" id="CompareVisibility">
            <!-- <button type="submit" class="btn btn-primary">Αναζήτηση</button> -->
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="row loader">
    <div class="col" align="center">
          <h4>Φόρτωση...</h4>
    </div>
  </div>
   <div class="row" id="generalStats" class="stats">
     <div class="col-sm">
       <h3>Γενικά στατιστικά</h3>
       <h4 id="genDate" class="statsElems"></h4>
       <div class="row" id="general" class="statsElems">
       </div>
     </div>
   </div><hr>
   <div class="row" id='answerStats' class="stats">
     <div class="col-sm">
      <h3>Συγκεντρωτικά απαντήσεων</h3>
      <div class="row">
        <div class="col-sm">
          <h4>Delivery</h4><span id="deliveryQuests" class="statsElems"></span>
        </div>
        <div class="col-sm">
          <h4>Take</h4><span id="takeQuests" class="statsElems"></span>
        </div>
      </div>
      <div class="row">
        <div class="col-sm" id="delIndicators" >
          <div class="card" style="width: 30rem;">
          <div class="card-header">Απαντήσεις στις ερωτήσεις delivery</div>
          <ul class="list-group list-group-flush" id="delIndlist" class="statsElems"></ul>
          </div>
        </div>
        <div class="col-sm" id="takeIndicators">
          <div class="card" style="width: 30rem;">
          <div class="card-header">Απαντήσεις στις ερωτήσεις take</div>
          <ul class="list-group list-group-flush" id="takeIndlist" class="statsElems"></ul>
          </div>
        </div>
      </div><hr>
      <div class="row">
        <div class="col-sm" id="AvgdelInd" class="statsElems">
        </div>
        <div class="col-sm" id="AvgtakeInd" class="statsElems">
        </div>
      </div><hr>
      <div class="row">
        <div class="container" id="genSatisfaction" class="statsElems">
        </div>
      </div><hr>
      <div class="row">
        <div class="container" id="NPS" class="statsElems">
        </div>
      </div>
    </div>
   </div>
   <div class="row" id="CompGenStats">
     <div class="col-sm">
       <h3>Γενικά στατιστικά</h3>
       <h4 id="cgenDate"></h4>
       <div class="row" id="cgeneral1">
       </div><hr>
       <div class="row" id="cgeneral2">
       </div>
     </div>
   </div><hr>
   <div class="row" id='CompAnsStats'>
   </div>


</div>
@endsection
