@extends('user.layouts.home')
@section('content')
<script type="text/javascript" src="/js/comments/comments.js"></script>
<script type="text/javascript" src="/js/customLibrs/exportData.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
RestaurantPicker();
  });

  function RestaurantPicker() {
    var $form = $( '#Restaurants' ),
    url = '/api/user/UserRestaurants';
    var posting = $.post( url, $form.serialize() );
    posting.done(function( data ) {
      $.each(data.restaurants, function( index, value ) {
        var option = $("<option></option>").attr('value',index);
          option.text(value);
          $("#restPicker").append(option);
      });
    });
  }
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container">
  <div class="row">
    <div class="col-sm">
      <form class="Restaurants" action="/" method="post" id="Restaurants">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="api_token" value="{{$api_token}}">
      </form>
      <form action="/api/user/commentsData" method="post" name="dateSearch" id="dateSearch">
        <input type="hidden" name="api_token" value="{{$api_token}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
          <div class="col-sm">
            <div class="form-group">
            <label for="startDate">Από:</label>
            <input type="date" class="form-control datepicker" name="startDate" id="startDate" required>
            </div>
          </div>
          <div class="col-sm">
            <div class="form-group">
              <label for="endDate">Έως:</label>
              <input type="date" class="form-control datepicker" name="endDate" id="endDate" required>
            </div>
          </div>
          <div class="col-sm">
            <div class="form-group">
              <label for="restPicker">Εστιατόριο:</label>
                <select class="form-control" id="restPicker" name="restaurant" required>
                  <option disabled selected value></option>
                </select>
             </div>
          </div>
          <div class="col-sm">
            <div class="form-group">
              <label for="customdate">Περιορισμένο εύρος</label>
               <select class="form-control" value='Σήμερα' id="customdate">
                 <option disabled selected value>Επιλογή</option>
                 <option value="1">Σήμερα</option>
                 <option value="2">Χθές</option>
                 <option value="3">Τελευταίες 7 ημέρες</option>
                 <option value="4">Αυτόν τον μήνα</option>
                 <option value="5">Τον προηγούμενο μήνα</option>
               </select>
             </div>
          </div>
          <!-- <input type="hidden" name="minSatisf" value="0">
          <input type="hidden" name="maxSatisf" value="10"> -->
        </div>
        <div class="row">
          <div class="col-sm-3">
            <div class="form-group">
              <label for="minSatisf">Ελάχιστη ικανοποίηση:</label>
                <input type="text" class="form-control" id="minSatisf" name="minSatisf" readonly>
             </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label for="maxSatisf">Μέγιστη ικανοποίηση:</label>
                <input type="text" class="form-control" id="maxSatisf" name="maxSatisf" readonly>
             </div>
          </div>
          <div class="col-sm">
            <div class="form-group">
              <label for="slider-range">Ρυθμιστής εύρους:</label>
              <div id="slider-range" align='center'></div>
             </div>
          </div>
        </div>
      </form>
    </div>
  </div>

  <div class="row loader">
    <div class="col" align="center">
          <h4>Φόρτωση...</h4>
    </div>
  </div><hr>

   <div class="row" id="generalStats">
     <div class="col-sm">
       <h3>Γενικά στατιστικά</h3>
       <h4 id="genDate"></h4>
       <div class="row" id="general">
           <div class="col-4">Ολα τα ερωτηματολόγια: <span></span></div>
           <div class="col-4">Συμπληρωμένα: <span></span></div>
           <div class="col-4">Μη Συμπληρωμένα: <span></span></div>
           <div class="col-4">Ποσοστό συμπλήρωσης: <span></span>%</div>
           <div class="col-4">Συμπληρωμένα Delivery: <span></span></div>
           <div class="col-4">Συμπληρωμένα Take: <span></span></div>
           <div class="col-4">Γενική ικανοποίηση Delivery: <span></span></div>
           <div class="col-4">Γενική ικανοποίηση Takeaway: <span></span></div>
       </div>
       <!-- <button type="button" id="export" class="btn">Export</button> -->
     </div>
   </div><hr>
   <div class="row" id='answerStats'>

      <div class="col-sm">
        <h3>Στοιχεία ερωτηματολογίων</h3>
      </div>

    <div class="row">
      <div class="col-sm">

          <table class="table table-bordered" id="commentsTable">
            <thead>
              <tr>
                <th style="width: 20%">Λεπτομέρειες Παραγγελίας</th>
                <th>Σχόλιο</th>
                <th>Σχόλιο Μεθόδου</th>
                <th>Σχόλια Προϊόντων</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
    <!-- <form action="/api/Questionnaires" method="post" id="Export">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="startDate" value="">
      <input type="hidden" name="endDate" value="">
      <input type="hidden" name="minSatisf" value="">
      <input type="hidden" name="maxSatisf" value="">
    </form> -->
   </div>
</div>
@endsection
