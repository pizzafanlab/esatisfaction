<header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/user/stats">PizzaFan customer experience</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="/user/stats">Συγκεντρωτικά</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/user/details">Αναλυτικά</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/user/comments">Σχόλια</a>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0" action="{{ route('logout') }}" method="post">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
          </form>
        </div>
      </nav>
    </header>
