@extends('admin.layouts.home')
@section('content')
<script type="text/javascript" src="/js/users/admin.js"></script>
<!-- <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"> -->

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <div id="tabs" style="width:100%; height:100%">
      <ul>
        <li><a href="#tabs-1">Χρήστες</a></li>
        <li><a href="#tabs-2">Διαχειριστές</a></li>
        <li><a href="#tabs-3">Emails Καταστημάτων</a></li>
        <li><a href="#tabs-4">Ερωτήσεις</a></li>
      </ul>
      <div id="tabs-1">
        <form class="Users" action="/" method="post" id="RetrieveUsers">
          <input type="hidden" name="api_token" value="{{$api_token}}">
        </form>

        <div class="container" id="users">
          <h4>Όλοι οι χρήστες</h4>
          <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="usersTable">
            <thead>
              <tr>
                <th>Όνομα</th>
                <th>Επίθετο</th>
                <th>email</th>
                <th>Area</th>
                <th>Τηλέφωνο</th>
                <th>Καταστήματα</th>
                <th>Ειδοποιήσεις</th>
                <th>Κατάσταση</th>
                <th>Επιβεβαίωση</th>
                <th>emails</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div><p>

        <div class="container">
          <div class="row">

              <div class="col-sm">
                <h4>Επιβεβαίωση Χρήστη</h4>
                <form class="Users" action="/" method="post" id="ConfirmUser" onkeypress="return event.keyCode != 13;">
                  <input type="hidden" name="api_token" value="{{$api_token}}">

                    <div class="form-group">
                      <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" name="email" required>
                     </div>
                    <button type="button" id="confirm" class="btn" onclick="ConfirmUser(event)">Επιβεβαίωση</button>
                </form>
              </div>

              <div class="col-sm">
                <h4>Ενεργοποίηση/Απενεργοποίηση Χρήστη</h4>
                <form class="Users" action="/" method="post" id="ChangeStatus" onkeypress="return event.keyCode != 13;">
                  <input type="hidden" name="api_token" value="{{$api_token}}">

                    <div class="form-group">
                      <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" name="email" required>
                     </div>
                    <button type="button" id="enable" class="btn" onclick="EnableUser(event)">Ενεργοποίηση</button>
                    <button type="button" id="disable" class="btn" onclick="DisableUser(event)">Απενεργοποίηση</button>
                </form>
              </div>

              <div class="col-sm">
                <h4>Αλλαγή σε Admin</h4>
                <form class="Users" action="/" method="post" id="MakeAdmin" onkeypress="return event.keyCode != 13;">
                  <input type="hidden" name="api_token" value="{{$api_token}}">

                    <div class="form-group">
                      <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" name="email" required>
                     </div>

                    <button type="button" id="addAdmin" class="btn" onclick="MakeAdmin(event)">Αλλαγή σε Διαχειριστή</button>
                </form>
              </div>

          </div><p>

          <div class="row">
            <div class="col-sm">
              <div class="alert alert-info" id="UsersMessagebox1"></div>
            </div>
          </div><p>

          <div class="row">

            <div class="col-sm">
            <h4>Προσθήκη/Αφαίρεση emails σε Χρήστη</h4>
            <form class="Users" action="/" method="post" id="ChangeEmail" onkeypress="return event.keyCode != 13;">
              <input type="hidden" name="api_token" value="{{$api_token}}">

                <div class="form-group">
                  <label for="email">Πρωτεύον Email:</label>
                    <input type="text" class="form-control" id="email" name="email" required>
                 </div>
                 <div class="form-group">
                   <label for="other_email">Δευτερεύον Email:</label>
                     <input type="text" class="form-control" id="other_email" name="other_email" required>
                  </div>
                <button type="button" id="addSecondaryEmail" class="btn" onclick="AddSecEmail(event)">Προσθήκη</button>
                <button type="button" id="removeSecondaryEmail" class="btn" onclick="RemoveSecEmail(event)">Αφαίρεση</button>
            </form>
          </div>

            <div class="col-sm">
              <h4>Προσθήκη/Αφαίρεση Εστιατορίου σε Χρήστη</h4>
              <form class="Users" action="/" method="post" id="ChangeRestaurant" onkeypress="return event.keyCode != 13;">
                <input type="hidden" name="api_token" value="{{$api_token}}">

                  <div class="form-group">
                    <label for="email">Email:</label>
                      <input type="text" class="form-control" id="email" name="email" required>
                   </div>
                   <div class="form-group">
                     <label for="restPicker">Εστιατόριο:</label>
                       <select class="form-control restPicker" id="restPicker" name="restaurants" required>
                         <option disabled selected value></option>
                       </select>
                    </div>
                  <button type="button" id="addRest" class="btn" onclick="AddRestaurant(event)">Προσθήκη</button>
                  <button type="button" id="removeRest" class="btn" onclick="RemoveRestaurant(event)">Αφαίρεση</button>
              </form>
            </div>

            <div class="col-sm">
              <h4>Αλλαγή σε Area</h4>
              <form class="Users" action="/" method="post" id="ChangeArea" onkeypress="return event.keyCode != 13;">
                <input type="hidden" name="api_token" value="{{$api_token}}">

                  <div class="form-group">
                    <label for="email">Email:</label>
                      <input type="text" class="form-control" id="email" name="email" required>
                   </div>

                  <button type="button" id="addArea" class="btn" onclick="EnableArea(event)">Ενεργοποίηση</button>
                  <button type="button" id="RemoveArea" class="btn" onclick="DisableArea(event)">Απενεργοποίηση</button>
              </form>
            </div>


          </div><p>

          <div class="row">
            <div class="col-sm">
              <div class="alert alert-info" id="UsersMessagebox2"></div>
            </div>
          </div><p>

            <div class="row">
                <div class="col-sm-4">
                  <h4>Αλλαγή Ειδοποιήσεων</h4>
                  <form class="Users" action="/" method="post" id="ChangeUserNotify" onkeypress="return event.keyCode != 13;">
                    <input type="hidden" name="api_token" value="{{$api_token}}">

                      <div class="form-group">
                        <label for="email">Email:</label>
                          <input type="text" class="form-control" id="email" name="email" required>
                       </div>

                      <button type="button" id="ChangeNotify" class="btn" onclick="ChangeUserNotify(event)">Αλλαγή</button>
                  </form>
                </div>
                <div class="col-sm-4">
                  <h4>Αλλαγή σε Moderator</h4>
                  <form class="Users" action="/" method="post" id="MakeModerator" onkeypress="return event.keyCode != 13;">
                    <input type="hidden" name="api_token" value="{{$api_token}}">

                      <div class="form-group">
                        <label for="email">Email:</label>
                          <input type="text" class="form-control" id="email" name="email" required>
                       </div>

                      <button type="button" id="MakeModeratorBtn" class="btn" onclick="MakeMod(event)">Ενεργοποίηση</button>
                  </form>
                </div>
            </div><p>

            <div class="row">
                <div class="col-sm">
                  <div class="alert alert-info" id="UsersMessagebox3"></div>
                </div>
            </div>

          </div>

        </div>

      <div id="tabs-2">
        <form class="Admins" action="/" method="post" id="RetrieveAdmins">
          <input type="hidden" name="api_token" value="{{$api_token}}">
        </form>


        <div class="container" id="admins">
          <h4>Όλοι οι Διαχειριστές</h4>
          <table class="table table-bordered" id="adminsTable">
            <thead>
              <tr>
                <th>Όνομα</th>
                <th>Επίθετο</th>
                <th>email</th>
                <th>Ειδοποιήσεις</th>
                <th>Τηλέφωνο</th>
                <th>Κατάσταση</th>
                <th>Επιβεβαίωση</th>
                <th>emails</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div><p>

          <div class="container">
            <div class="row">

              <div class="col-sm">
                <h4>Προσθήκη/Αφαίρεση emails σε Διαχειριστή</h4>
                <form class="Admins" action="/" method="post" id="ChangeAdminEmail" onkeypress="return event.keyCode != 13;">
                  <input type="hidden" name="api_token" value="{{$api_token}}">

                    <div class="form-group">
                      <label for="email">Πρωτεύον Email:</label>
                        <input type="text" class="form-control" id="email" name="email" required>
                     </div>
                     <div class="form-group">
                       <label for="other_email">Δευτερεύον Email:</label>
                         <input type="text" class="form-control" id="other_email" name="other_email" required>
                      </div>
                    <button type="button" id="addSecondaryEmail" class="btn" onclick="AddAdminSecEmail(event)">Προσθήκη</button>
                    <button type="button" id="removeSecondaryEmail" class="btn" onclick="RemoveAdminSecEmail(event)">Αφαίρεση</button>
                </form>
              </div>

              <div class="col-sm">
                <h4>Ενεργοποίηση/Απενεργοποίηση Διαχειριστή</h4>
                <form class="Admins" action="/" method="post" id="ChangeAdminStatus" onkeypress="return event.keyCode != 13;">
                  <input type="hidden" name="api_token" value="{{$api_token}}">

                    <div class="form-group">
                      <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" name="email" required>
                     </div>
                    <button type="button" id="enable" class="btn" onclick="EnableAdmin(event)">Ενεργοποίηση</button>
                    <button type="button" id="disable" class="btn" onclick="DisableAdmin(event)">Απενεργοποίηση</button>
                </form>
              </div>

              <div class="col-sm-4">
                <h4>Αλλαγή Ειδοποιήσεων Διαχειριστή</h4>
                <form class="Users" action="/" method="post" id="ChangeAdminNotify" onkeypress="return event.keyCode != 13;">
                  <input type="hidden" name="api_token" value="{{$api_token}}">

                    <div class="form-group">
                      <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" name="email" required>
                     </div>

                    <button type="button" id="ChangeNotify" class="btn" onclick="ChangeAdminNotify(event)">Αλλαγή</button>
                </form>
              </div>

            </div>
          </div><p>

          <div class="row">
            <div class="col-sm">
              <div class="alert alert-info" id="AdminMessagebox"></div>
            </div>
          </div><p></p>

          <div class="container" id="mods">
            <h4>Όλοι οι Moderators</h4>
            <table class="table table-bordered" id="modsTable">
              <thead>
                <tr>
                  <th>Όνομα</th>
                  <th>Επίθετο</th>
                  <th>email</th>
                  <th>Ειδοποιήσεις</th>
                  <th>Τηλέφωνο</th>
                  <th>Κατάσταση</th>
                  <th>Επιβεβαίωση</th>
                  <th>emails</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div><p></p>

          <div class="container">
            <div class="row">

              <div class="col-sm">
                <h4>Προσθήκη/Αφαίρεση emails σε Moderator</h4>
                <form class="Mods" action="/" method="post" id="ChangeModEmail" onkeypress="return event.keyCode != 13;">
                  <input type="hidden" name="api_token" value="{{$api_token}}">

                    <div class="form-group">
                      <label for="email">Πρωτεύον Email:</label>
                        <input type="text" class="form-control" id="email" name="email" required>
                     </div>
                     <div class="form-group">
                       <label for="other_email">Δευτερεύον Email:</label>
                         <input type="text" class="form-control" id="other_email" name="other_email" required>
                      </div>
                    <button type="button" id="addModSecEmail" class="btn" onclick="AddModSecEmail(event)">Προσθήκη</button>
                    <button type="button" id="removeModSecEmail" class="btn" onclick="RemoveModSecEmail(event)">Αφαίρεση</button>
                </form>
              </div>

              <div class="col-sm">
                <h4>Ενεργοποίηση/Απενεργοποίηση Moderator</h4>
                <form class="Mods" action="/" method="post" id="ChangeModStatus" onkeypress="return event.keyCode != 13;">
                  <input type="hidden" name="api_token" value="{{$api_token}}">

                    <div class="form-group">
                      <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" name="email" required>
                     </div>
                    <button type="button" id="enable" class="btn" onclick="EnableMod(event)">Ενεργοποίηση</button>
                    <button type="button" id="disable" class="btn" onclick="DisableMod(event)">Απενεργοποίηση</button>
                </form>
              </div>

              <div class="col-sm-4">
                <h4>Αλλαγή Ειδοποιήσεων Moderator</h4>
                <form class="Mods" action="/" method="post" id="ChangeModNotify" onkeypress="return event.keyCode != 13;">
                  <input type="hidden" name="api_token" value="{{$api_token}}">

                    <div class="form-group">
                      <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" name="email" required>
                     </div>

                    <button type="button" id="ChangeNotifyMod" class="btn" onclick="ChangeModNotify(event)">Αλλαγή</button>
                </form>
              </div>

            </div>
          </div><p>

          <div class="row">
            <div class="col-sm">
              <div class="alert alert-info" id="AdminMessagebox2"></div>
            </div>
          </div>

      </div>

      <div id="tabs-3">
        <form class="Rest_emails" action="/" method="post" id="RetrieveRestEmails">
          <input type="hidden" name="api_token" value="{{$api_token}}">
        </form>

        <div class="container" id="Rest_emails">
          <h4>Emails Καταστημάτων</h4>
          <table class="table table-bordered" id="RestEmailsTable">
            <thead>
              <tr>
                <th>Κατάστημα</th>
                <th>Email</th>
                <th>Ειδοποιήσεις</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div><p>

          <div class="container">
            <div class="row">

              <div class="col-sm">
                <h4>Προσθήκη Εστιατορίου</h4>
                <form class="Rest_emails" action="/" method="post" id="AddRestEmail" onkeypress="return event.keyCode != 13;">
                  <input type="hidden" name="api_token" value="{{$api_token}}">
                  <div class="form-group">
                    <label for="restPicker">Εστιατόριο:</label>
                      <select class="form-control restPicker" id="EmailrestPicker" name="restaurant" required>
                        <option disabled selected value></option>
                      </select>
                   </div>
                    <div class="form-group">
                      <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" name="email" required>
                     </div>
                    <button type="button" id="confirm" class="btn" onclick="AddRestEmail(event)">Προσθήκη</button>
                </form>
              </div>

              <div class="col-sm">
                <h4>Αλλαγή email Εστιατορίου</h4>
                <form class="Rest_emails" action="/" method="post" id="UpdateRestEmail" onkeypress="return event.keyCode != 13;">
                  <input type="hidden" name="api_token" value="{{$api_token}}">
                  <div class="form-group">
                    <label for="restPicker">Εστιατόριο:</label>
                      <select class="form-control restPicker" id="EmailrestPicker" name="restaurant" required>
                        <option disabled selected value></option>
                      </select>
                   </div>
                    <div class="form-group">
                      <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" name="email" required>
                     </div>
                    <button type="button" id="confirm" class="btn" onclick="UpdateRestEmail(event)">Αλλαγή</button>
                </form>
              </div>

              <div class="col-sm">
                <h4>Αλλαγή Ειδοποιήσεων Καταστημάτων:</h4>
                <form class="Rest_emails" action="/" method="post" id="ChamgeRestNotify" onkeypress="return event.keyCode != 13;">
                  <input type="hidden" name="api_token" value="{{$api_token}}">
                  <div class="form-group">
                    <label for="restPicker">Εστιατόριο:</label>
                      <select class="form-control restPicker" name="restaurant" required>
                        <option disabled selected value></option>
                      </select>
                   </div>
                    <button type="button" id="confirm" class="btn" onclick="ChangeRestNotify(event)">Αλλαγή</button>
                </form>
              </div>

            </div><p>

            <div class="row">
              <div class="col-sm">
                <div class="alert alert-info" id="EmailRestMessagebox"></div>
              </div>

            </div>
          </div>

      </div>

      <div id="tabs-4">
        <form class="Questions" action="/" method="post" id="RetrieveQuests">
          <input type="hidden" name="api_token" value="{{$api_token}}">
        </form>

        <div class="container" id="DelQuests">
          <h4>Ερωτήσεις Delivery</h4>
          <table class="table table-bordered" id="DelQuestsTable">
            <thead>
              <tr>
                <th>Κατηγορία</th>
                <th>Ερώτηση</th>
                <th>Σημαία</th>
                <th>Κατάσταση</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div><p><hr>

          <div class="container" id="TakeQuests">
            <h4>Ερωτήσεις Take</h4>
            <table class="table table-bordered" id="TakeQuestsTable">
              <thead>
                <tr>
                  <th>Κατηγορία</th>
                  <th>Ερώτηση</th>
                  <th>Σημαία</th>
                  <th>Κατάσταση</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div><p><hr>
            <div class="container">
              <div class="row">
                <div class="col">
                  <h5>Προσοχή! οι αλλαγές στις ερωτήσεις είναι άμεσες και εφαρμόζονται απευθείας στα ερωτηματολόγια.</h5>
                </div>
              </div>
            </div><p><hr>
            <div class="container">
              <div class="row">
                <div class="col-sm">
                    <h4>Προσθήκη Ερώτησης</h4>
                    <form class="AddQuest" action="/" method="post" id="AddQuest" onkeypress="return event.keyCode != 13;">
                      <input type="hidden" name="api_token" value="{{$api_token}}">
                      <div class="form-group">
                        <label for="QuestMethod">Μέθοδος:</label>
                          <select class="form-control" name="method" id="QuestMethod" required>
                            <option disabled selected value></option>
                              <option value="delivery">Delivery</option>
                              <option value="take">Take</option>
                          </select>
                       </div>
                       <div class="form-group">
                         <label for="QuestCategory">Κατηγορία:</label>
                           <select class="form-control" name="category" id="QuestCategory" required>
                             <option disabled selected value></option>
                               <option value="general">Γενικές</option>
                               <option value="indicators">Δείκτες</option>
                           </select>
                        </div>
                        <div class="form-group" id="flagform">
                          <label for="Questflag">Σημαία:</label>
                            <select class="form-control" name="flag" id="Questflag" required>
                              <option selected value="">Καμία</option>
                                <option value="satisf">Ικανοποίηση</option>
                                <option value="promo">Promotion</option>
                            </select>
                         </div>
                        <div class="form-group">
                          <label for="questlabel">Ερώτηση:</label>
                            <textarea class="form-control" name="text" rows="2" cols="10" id="questlabel" required></textarea>
                         </div>
                         <input type="hidden" name="Input" value="num">
                         <input type="hidden" name="value" value="0">

                        <button type="button" id="confirm" class="btn" onclick="AddQuest(event)">Καταχώρηση</button>
                    </form>
                </div>
                <div class="col-sm">
                    <h4>Αλλαγή Κατάστασης</h4>
                    <form class="SatusQuest" action="/" method="post" id="SatusQuest" onkeypress="return event.keyCode != 13;">
                      <input type="hidden" name="api_token" value="{{$api_token}}">
                      <div class="form-group">
                        <label for="DelStatus">Delivery:</label>
                          <select class="form-control delquestpicker changeQuestPicker" id="DelStatus" required>
                            <option disabled selected value></option>
                          </select>
                       </div>
                       <div class="form-group">
                         <label for="TakeStatus">Take:</label>
                           <select class="form-control takequestpicker changeQuestPicker" id="TakeStatus" required>
                             <option disabled selected value></option>
                           </select>
                        </div>
                        <div class="form-group">
                          <input type="hidden" name="questID" value="" id="changeQuestval">
                          <label for="changeQuestlabel">Επιλογή:</label>
                          <input class="form-control" type="text" value="" id="changeQuestlabel" readonly>
                         </div>

                        <button type="button" id="confirm" class="btn" onclick="ActivateQuest(event)">Ενεργοποίηση</button>
                        <button type="button" id="confirm" class="btn" onclick="DisableQuest(event)">Απενεργοποίηση</button>
                    </form>
                </div>
                <div class="col-sm">
                    <h4>Επεξεργασία Ερώτησης</h4>
                    <form class="EditQuest" action="/" method="post" id="EditQuest" onkeypress="return event.keyCode != 13;">
                      <input type="hidden" name="api_token" value="{{$api_token}}">
                      <div class="form-group">
                        <label for="DelEdit">Delivery:</label>
                          <select class="form-control delquestpicker changeQuestEdit" id="DelEdit" required>
                            <option disabled selected value></option>
                          </select>
                       </div>
                       <div class="form-group">
                         <label for="TakeEdit">Take:</label>
                           <select class="form-control takequestpicker changeQuestEdit" id="TakeEdit" required>
                             <option disabled selected value></option>
                           </select>
                        </div>
                        <div class="form-group">
                          <input type="hidden" name="questID" value="" id="EditQuestval">
                          <label for="EditQuestlabel">Επιλογή:</label>
                          <input class="form-control" type="text" value="" id="EditQuestlabel" readonly>
                         </div>
                         <div class="form-group">
                           <label for="EditQuestText">Αλλαγή σε:</label>
                           <input class="form-control" type="text" name="questText" value="" id="EditQuestText">
                          </div>
                        <button type="button" id="confirm" class="btn" onclick="EditQuest(event)">Αλλαγή</button>

                    </form>
                </div>


              </div><p></p>
              <div class="row">
                <div class="col-sm">
                  <div class="alert alert-info" id="QuestMessagebox"></div>
                </div>

              </div>
            </div>

      </div>





    </div>
@endsection
