<?php

use Illuminate\Support\Facades\Schema;
use Jenssegers\Mongodb\Schema\Blueprint;
// use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('mails', function($collection)
      {
          $collection->increments('mail_id');
          $collection->boolean('sent')->index();
          $collection->dateTime('stamp')->index();
          $collection->boolean('cancelled')->index();
          $collection->bigInteger('quest_id')->unique();
          $collection->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mails');
    }
}
