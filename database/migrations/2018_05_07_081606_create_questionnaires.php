<?php

use Illuminate\Support\Facades\Schema;
use Jenssegers\Mongodb\Schema\Blueprint;
// use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionnaires extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('questionnaires', function($collection)
        {
            //$collection->increments('questionnaire_id');
            $collection->integer('rest_id')->index();

            $collection->dateTime('stamp')->index();
            $collection->boolean('data.isCancelled')->index();
            $collection->boolean('data.isComplete')->index();
            //$collection->bigInteger('quest_id')->unique();
            $collection->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('questionnaires');

    }
}
