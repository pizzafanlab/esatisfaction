<?php

use Illuminate\Support\Facades\Schema;
use Jenssegers\Mongodb\Schema\Blueprint;
// use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      // protected $connection = 'mongodb';
      Schema::create('admins', function($collection)
      {
          $collection->increments('id');
          $collection->string('name');
          $collection->string('email')->unique();
          $collection->string('password');
          $collection->string('api_token',60)->unique();
          $collection->boolean('confirmed');
          $collection->boolean('active');
          $collection->rememberToken();
          $collection->timestamps();
      });
        // Schema::create('admins', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('name');
        //     $table->string('email')->unique();
        //     $table->string('password');
        //     $table->rememberToken();
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
