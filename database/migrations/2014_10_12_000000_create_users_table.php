<?php

use Illuminate\Support\Facades\Schema;
use Jenssegers\Mongodb\Schema\Blueprint;
// use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    //protected $connection = 'mongodb';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //protected $connection = 'mongodb';
      Schema::create('users', function($collection)
      {
          $collection->increments('user_id');
          $collection->string('name');
          $collection->string('surname');
          $collection->string('email')->unique();
          $collection->string('number');
          $collection->string('password');
          $collection->string('api_token',60)->unique();
          $collection->boolean('confirmed');
          $collection->boolean('active');
          $collection->boolean('isArea');
          $collection->boolean('isAdmin');
          $collection->boolean('Moderator');
          $collection->boolean('Νotifications');
          $collection->rememberToken();
          $collection->timestamps();
      });
        // Schema::create('users', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('name');
        //     $table->string('email')->unique();
        //     $table->string('password');
        //     $table->rememberToken();
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
