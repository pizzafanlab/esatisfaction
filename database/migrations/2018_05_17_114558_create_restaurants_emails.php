<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsEmails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('Restaurants_emails', function($collection)
      {
          $collection->increments('id');
          $collection->integer('rest_id')->unique();
          $collection->string('email')->unique();
          $collection->boolean('notify');
          $collection->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Restaurants_emails');
    }
}
