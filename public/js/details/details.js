jQuery(document).ready(function() {

  google.charts.load('current', {'packages':['corechart']});
  //google.charts.setOnLoadCallback(drawStuff);

  $('#generalStats').hide();
  $('#answerStats').hide();
  $('.loader').hide();

  function RangeChecker(dates,restaurants){
    var response = {};
    if (restaurants.id == 'all') {
      if((dates.endDate - dates.startDate)<=1000*60*60*24*31){
        response.success = true;
      }else {
        response.success = false;
        response.message = 'Το ημερολογιακό εύρος δεν πρέπει να ξεπερνά τον 1 μήνα για αναζήτηση σε όλο το δίκτυο.'
      }
    }
    else {
      if((dates.endDate - dates.startDate)<=1000*60*60*24*62){
        response.success = true;
      }else {
        response.success = false;
        response.message = 'Το ημερολογιακό εύρος δεν πρέπει να ξεπερνά τους 2 μήνες για αναζήτηση με κατάστημα.'
      }
    }
    return response;
  }
//
  function DatesChecker(){
    if ($("input[name='startDate']").val() && $("input[name='endDate']").val()) {
      var dates = {
        startDate: new Date($("input[name='startDate']").val()),
        endDate :  new Date($("input[name='endDate']").val())
      }
      var restaurants = {
        id : $("#restPicker").val()
      }
      var response = RangeChecker(dates,restaurants);
      if (response.success) return true;
      alert(response.message);
    }
  }

  function ApiCall(event){
    event.preventDefault();
    $('.loader').show();
    var $form = $( '#dateSearch' ),
    url = $form.attr( "action" );

    var posting = $.post( url, $form.serialize() );
   // console.log($form.serialize());
   FormsMatcher();
    posting.done(function( data ) {
     showData(data);
     $('.loader').hide();
    });
  }

  jQuery('#customdate').change(function(event) {
    customDatepicker(this.value,'startDate','endDate');
    if (DatesChecker()) ApiCall(event);
  });

  jQuery("input[name='endDate']").change(function(event) {

      if(DatesChecker()) {
        ApiCall(event);
      }
  });

  jQuery("input[name='maxSatisf']").change(function(event) {

      if(DatesChecker()) {
        ApiCall(event);
      }
  });

  jQuery("input[name='minSatisf']").change(function(event) {

      if(DatesChecker()) {
        ApiCall(event);
      }
  });

  jQuery('#restPicker').change(function(event) {
    if (DatesChecker()) ApiCall(event);
  });

  jQuery('#catPicker').change(function(event) {
    if (DatesChecker()) ApiCall(event);
  });

  $( function() {
     $( "#slider-range" ).slider({
       range: true,
       min: 1,
       max: 10,
       values: [ 1, 10 ],
       slide: function( event, ui ) {
         $( "#minSatisf" ).val(ui.values[ 0 ]);
         $( "#maxSatisf" ).val(ui.values[ 1 ]);
       },
       change: function(event, ui) {
         if(DatesChecker()) {
           ApiCall(event);
         }
       }
     });
     $( "#minSatisf" ).val($( "#slider-range" ).slider( "values", 0 ));
     $( "#maxSatisf" ).val($( "#slider-range" ).slider( "values", 1 ));

   } );

  function showData(data){

    function generalFiller(elem,stats){
      var i=0;
      var general = [];
      for (var key in stats) {
         general.push(stats[key]);
      }
      $('#'+elem+'>div').each(function(){
        $(this).children('span').text(general[i]);
        i++;
      });
    }
    var obj = data;
    var stats = obj['data']['stats'];
    var details = obj['data']['details']['orders'];

    generalFiller('general',stats);
    $('#generalStats').show();
    $('#answerStats').show();

      $("#detailsTable").dataTable().fnDestroy();
      $('#detailsTable').DataTable( {
       data: details,
           columns: [
               { data: 'id' },
               { data: 'created' },
               { data: 'submitted' },
               { data: 'satisf' },
               { data: 'type' }
           ]
       } );

  }

  $( "#export" ).click(function() {

    var $form = $( '#Export' ),
    url = $form.attr( "action" );

    var posting = $.post( url, $form.serialize() );
   // console.log($form.serialize());
    posting.done(function( data ) {
      var file = convertArrayOfObjectsToCSV(ArrayDataExport(data));
      downloadCSV({ filename: "erotimatologia.csv" }, file);
    });

  });

  $( "#exportPerStore" ).click(function() {

    var $form = $( '#ExportPerStore' ),
    url = $form.attr( "action" );

    var posting = $.post( url, $form.serialize() );
   // console.log($form.serialize());
    posting.done(function( data ) {
     //console.log(data);
      //ArrayRestExport(data);
    var file = convertArrayOfObjectsToCSV(ArrayRestExport(data.data));
     downloadCSV({ filename: "katastimata.csv" }, file);
    });

  });

});
