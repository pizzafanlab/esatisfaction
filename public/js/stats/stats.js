jQuery(document).ready(function() {

    google.charts.load('current', {'packages':['corechart']});
    //google.charts.setOnLoadCallback(drawStuff);

    $('#compareBlock').hide();
    $('#generalStats').hide();
    $('#answerStats').hide();
    $('#CompGenStats').hide();
    $('#CompAnsStats').hide();
    $('.loader').hide();

    var compareFlag = false;

    function ApiCall(event){

      event.preventDefault();
      $('.loader').show();
      var $form = $( '#dateSearch' ),
      url = $form.attr( "action" );

      var posting = $.post( url, $form.serialize() );

      posting.done(function( data ) {
        var compare = 'compStats';

        if (compare in data['data']) compareFlag=true;

        showData(data,compareFlag);
        $('.loader').hide();
      });
    }

    function RangeChecker(dates,restaurants){
      var response = {};
      if (dates.compare) {
        if (restaurants.id == 'all' && restaurants.compId == 'all') {
          if((dates.endDate - dates.startDate)<=1000*60*60*24*31 && (dates.compEndDate - dates.compStartDate)<=1000*60*60*24*31){
            response.success = true;
          }else {
            response.success = false;
            response.message = 'Το ημερολογιακό εύρος δεν πρέπει να ξεπερνά τον 1 μήνα για αναζήτηση σε όλο το δίκτυο.'
          }
        }
        else if (restaurants.id == 'all') {
          if((dates.endDate - dates.startDate)<=1000*60*60*24*31 && (dates.compEndDate - dates.compStartDate)<=1000*60*60*24*62){
            response.success = true;
          }else {
            response.success = false;
            response.message = 'Το ημερολογιακό εύρος δεν πρέπει να ξεπερνά τον 1 μήνα για αναζήτηση σε όλο το δίκτυο και τους 2 μήνες για αναζήτηση με κατάστημα.'
          }
        }
        else if (restaurants.compId == 'all') {
          if((dates.endDate - dates.startDate)<=1000*60*60*24*62 && (dates.compEndDate - dates.compStartDate)<=1000*60*60*24*31){
            response.success = true;
          }else {
            response.success = false;
            response.message = 'Το ημερολογιακό εύρος δεν πρέπει να ξεπερνά τον 1 μήνα για αναζήτηση σε όλο το δίκτυο και τους 2 μήνες για αναζήτηση με κατάστημα.'
          }
        }
        else {
          if((dates.endDate - dates.startDate)<=1000*60*60*24*62 && (dates.compEndDate - dates.compStartDate)<=1000*60*60*24*62){
            response.success = true;
          }else {
            response.success = false;
            response.message = 'Το ημερολογιακό εύρος δεν πρέπει να ξεπερνά τους 2 μήνες για αναζήτηση με κατάστημα.'
          }
        }
      }
      else {
        if (restaurants.id == 'all') {
          if((dates.endDate - dates.startDate)<=1000*60*60*24*31){
            response.success = true;
          }else {
            response.success = false;
            response.message = 'Το ημερολογιακό εύρος δεν πρέπει να ξεπερνά τον 1 μήνα για αναζήτηση σε όλο το δίκτυο.'
          }
        }
        else {
          if((dates.endDate - dates.startDate)<=1000*60*60*24*62){
            response.success = true;
          }else {
            response.success = false;
            response.message = 'Το ημερολογιακό εύρος δεν πρέπει να ξεπερνά τους 2 μήνες για αναζήτηση με κατάστημα.'
          }
        }
      }
      return response;
    }

    function DatesChecker(){
      if (compareFlag) {
        if ($('#CompareVisibility').prop('checked')) {
          if($("input[name='startDate']").val() && $("input[name='endDate']").val() && $("input[name='CompstartDate']").val() && $("input[name='CompendDate']").val()) {
            var dates = {
              compare: true,
              startDate: new Date($("input[name='startDate']").val()),
              endDate :  new Date($("input[name='endDate']").val()),
              compStartDate: new Date($("input[name='CompstartDate']").val()),
              compEndDate: new Date($("input[name='CompendDate']").val())
            }
            var restaurants = {
              id : $("#restPicker").val(),
              compId: $("#ComprestPicker").val()
            }
            var response = RangeChecker(dates,restaurants);
            if (response.success) return true;
            alert(response.message);
          }
        }
      }
      else {
        if ($('#CompareVisibility').prop('checked')) {
          if($("input[name='startDate']").val() && $("input[name='endDate']").val() && $("input[name='CompstartDate']").val() && $("input[name='CompendDate']").val()) {
            var dates = {
              compare: true,
              startDate: new Date($("input[name='startDate']").val()),
              endDate :  new Date($("input[name='endDate']").val()),
              compStartDate: new Date($("input[name='CompstartDate']").val()),
              compEndDate: new Date($("input[name='CompendDate']").val())
            }
            var restaurants = {
              id : $("#restPicker").val(),
              compId: $("#ComprestPicker").val()
            }
            var response = RangeChecker(dates,restaurants);
            if (response.success) return true;
            alert(response.message);
          }
        }
        else {
          if ($("input[name='startDate']").val() && $("input[name='endDate']").val()) {
            var dates = {
              compare: false,
              startDate: new Date($("input[name='startDate']").val()),
              endDate :  new Date($("input[name='endDate']").val())
            }
            var restaurants = {
              id : $("#restPicker").val()
            }
            var response = RangeChecker(dates,restaurants);
            if (response.success) return true;
            alert(response.message);
          }
        }
      }

    }

    jQuery('#CompareVisibility').change(function() {
    if ($(this).prop('checked')) {
      $('input[name=compare]').val('true');
      $('#compareBlock').show();
    }
    else {
      $('input[name=compare]').val('false');
      $('#compareBlock').hide();
    }
    });

    jQuery('#customdate').change(function(event) {
      customDatepicker(this.value,'startDate','endDate');
      if (DatesChecker()) ApiCall(event);
    });

    jQuery('#customCompdate').change(function(event) {
    customDatepicker(this.value,'CompstartDate','CompendDate');
    if (DatesChecker()) ApiCall(event);
    });

    jQuery("input[name='endDate']").change(function(event) {
      if (DatesChecker()) ApiCall(event);
    });

    jQuery("input[name='CompendDate']").change(function(event) {
      if (DatesChecker()) ApiCall(event);
    });

    jQuery('#restPicker').change(function(event) {
      if (DatesChecker()) ApiCall(event);
    });

    jQuery('#catPicker').change(function(event) {
      if (DatesChecker()) ApiCall(event);
    });


    jQuery('#CompareVisibility').change(function(event) {
      if (compareFlag) {
        window.location.reload();
      }
    });
});

function showData(data,compareflag){
//  console.log(data);
  function EmptyDivs(){
    $('#general').empty();
    $('#deliveryQuests').empty();
    $('#takeQuests').empty();
    $('#delIndlist').empty();
    $('#takeIndlist').empty();
    $('#AvgdelInd').empty();
    $('#AvgtakeInd').empty();
    $('#genSatisfaction').empty();
    $('#NPS').empty();
  }

  function generalFiller(elem,stats){
    var general = [];
    for (var key in stats) {
       general.push(stats[key]);
    }
    var questions = ['Ολα τα ερωτηματολόγια: ','Συμπληρωμένα: ','Μη Συμπληρωμένα: ','Ποσοστό συμπλήρωσης: ','Συμπληρωμένα Delivery: ','Συμπληρωμένα Take: ',
    'Γενική ικανοποίηση Delivery: ','Γενική ικανοποίηση Takeaway: '];

      for (var i = 0; i < questions.length; i++) {
        var row = $('<div></div>').attr('class','col-4');
        row.text(questions[i]+general[i]);
        $('#'+elem).append(row);
      }
  }

  function AvgDifference(array1,array2) {
    result = [];
    if (array1.length > 0 && array2.length > 0) {
      array1.forEach(function(element1) {
        array2.forEach(function(element2) {
          if(element1.questlabel == element2.questlabel)
            {
              var diff = parseFloat(element2.avg)-parseFloat(element1.avg);
              var item = {questlabel:element1.questlabel,avg:diff};
              result.push(item);
            }
          });
        });
    }
    return result;
  }

  function CompAverages(array1,array2) {
    result = [];
    if (array1.length > 0 && array2.length > 0) {
      array1.forEach(function(element1) {
        array2.forEach(function(element2) {
          if(element1.questlabel == element2.questlabel)
            {
              var value1 = parseFloat(element1.avg);
              var value2 = parseFloat(element2.avg);
              var item = {questlabel:element1.questlabel,compA:value1,compB:value2};
              result.push(item);
            }
          });
        });
    }
    return result;
  }

  if (!compareflag) {
    $('#generalStats').show();
    $('#answerStats').show();
    var obj = data;
    var dates = obj['data']['stats']['dates'];
    var stats = obj['data']['stats']['general'];
    var delQuests = obj['data']['stats']['deliveryQuestStats'];
    var takeQuests = obj['data']['stats']['takeQuestStats'];
    var delIndQuests = obj['data']['stats']['deliveryIndStats'];
    var takeIndQuests = obj['data']['stats']['takeIndStats'];
    var avgDelInd = obj['data']['stats']['DelAvgIndicators'];
    var avgTakeInd = obj['data']['stats']['TakeAvgIndicators'];
    var genSatisf = obj['data']['stats']['perDayScores'];
    EmptyDivs();
    $('#genDate').text('Από '+dates.date1+' έως '+dates.date2);
    generalFiller('general',stats);


    var counter = 1;
    var delCounter = QuestionsCreator(delQuests,'deliveryQuests',counter,'delivery',dates);
    var takeCounter = QuestionsCreator(takeQuests,'takeQuests',delCounter,'take',dates);
    var delIndCounter = ListCreator(delIndQuests,'delIndlist',takeCounter,'delivery',dates);
    var takeIndCounter = ListCreator(takeIndQuests,'takeIndlist',delIndCounter,'take',dates);
    var avgdelCounter = chartCreator(avgDelInd,'AvgdelInd',takeIndCounter,'delivery',dates,'bar');
    var avgtakeCounter = chartCreator(avgTakeInd,'AvgtakeInd',avgdelCounter,'take',dates,'bar');
    var satisfCounter = chartCreator(genSatisf,'genSatisfaction',avgtakeCounter,'Γενική Ικανοποίηση ανα ημέρα delivery & take',dates,'function');
    var npsCounter = chartCreator(genSatisf,'NPS',satisfCounter,'NPS ανα ημέρα delivery & take',dates,'function2');
  }
  else
   {
      EmptyDivs();
      $('#generalStats').hide();
      $('#answerStats').hide();
      $('#CompGenStats').show();
      $('#CompAnsStats').show();

      var obj = data;
      var dates = obj['data']['stats']['dates'];
      var compdates = obj['data']['compStats']['dates'];
      var stats = obj['data']['stats']['general'];
      var compstats = obj['data']['compStats']['general'];
      var delQuests = obj['data']['stats']['deliveryQuestStats'];
      var compdelQuests = obj['data']['compStats']['deliveryQuestStats'];
      var takeQuests = obj['data']['stats']['takeQuestStats'];
      var comptakeQuests = obj['data']['compStats']['takeQuestStats'];
      var avgDel = obj['data']['stats']['DelAvgIndicators'];
      var compavgDel = obj['data']['compStats']['DelAvgIndicators'];
      var avgTake = obj['data']['stats']['TakeAvgIndicators'];
      var compavgTake = obj['data']['compStats']['TakeAvgIndicators'];

      var deldiff = AvgDifference(compavgDel,avgDel);
      var takediff = AvgDifference(compavgTake,avgTake);
      var delAverages = CompAverages(avgDel,compavgDel);
      var TakeAverages = CompAverages(avgTake,compavgTake);

      $('#cgenDate').text('Από '+dates.date1+' έως '+dates.date2+' σε σύγκριση με '+compdates.date1+' έως '+compdates.date2);
      $('#cgeneral1').empty();
      $('#cgeneral2').empty();
      $('#CompAnsStats').empty();
      generalFiller('cgeneral1',stats);
      generalFiller('cgeneral2',compstats);
      var header = $("<div></div>").attr('class','row');
      header.html('<div class="col-sm"><h3>Συγκριτικά απαντήσεων</h3></div>');
      var delrow = $("<div></div>").attr('class','row');
      delrow.html('<div class="col-sm" id="delQuests"><h4>Delivery '+dates.date1+' έως '+dates.date2+'</h4></div><div class="col-sm" id="compdelQuests"><h4>Delivery '+compdates.date1+' έως '+compdates.date2+'</h4></div>');
      var takerow = $("<div></div>").attr('class','row');
      takerow.html('<div class="col-sm" id="tQuests"><h4>Take '+dates.date1+' έως '+dates.date2+'</h4></div><div class="col-sm" id="comptakeQuests"><h4>Take '+compdates.date1+' έως '+compdates.date2+'</h4></div>');
      $('#CompAnsStats').append(header,delrow,takerow);

      var counter = 1;
      var delCounter =       QuestionsCreator(delQuests,'delQuests',counter,'delivery',dates);
      var compdelCounter =   QuestionsCreator(compdelQuests,'compdelQuests',delCounter,'delivery',compdates);
      var takeCounter =      QuestionsCreator(takeQuests,'tQuests',compdelCounter,'take',dates);
      var comptakeCounter =  QuestionsCreator(comptakeQuests,'comptakeQuests',takeCounter,'take',compdates);

      if (deldiff.length > 0 && takediff.length > 0) {
        var avgdiff = $("<div></div>").attr('class','row');
        avgdiff.html('<div class="col-sm" id="deldiff"><h4>Διαφορές Delivery</h4></div><div class="col-sm" id="takediff"><h4>Διαφορές Take</h4></div>');
        var averages = $("<div></div>").attr('class','row');
        averages.html('<div class="col-sm" id="delavgs"><h4>Μέσοι Όροι Delivery</h4></div><div class="col-sm" id="takeavgs"><h4>Μέσοι Όροι Take</h4></div>');
        $('#CompAnsStats').append(avgdiff,averages);

        var deldiffcounter = chartCreator(deldiff,'deldiff',comptakeCounter,'delivery',dates,'bar');
        var takediffcounter = chartCreator(takediff,'takediff',deldiffcounter,'take',dates,'bar');
        var delAvgscounter = chartCreator(delAverages,'delavgs',takediffcounter,'delivery',dates,'bar2');
        var TakeAvgscounter = chartCreator(TakeAverages,'takeavgs',delAvgscounter,'take',dates,'bar2');
      }

    }


  $(".screenshot").click(function(){
    var id = this.id;
    var id = id.replace("screenshot", "");
    domtoimage.toJpeg(document.getElementById('chart'+id), { quality: 0.95 })
      .then(function (dataUrl) {
          var link = document.createElement('a');
          link.download = 'Screenshot';
          link.href = dataUrl;
          link.click();
      });
  });
}

function QuestionsCreator(data,element,counter,type,dates){
     // console.log(data);
  data.forEach(function(item) {
    var quest = $("<p></p>").text(item.questlabel);
    var avg = $("<p></p>").text('Μέσος όρος ερώτησης: '+item.avg);
    var to6 = 'Απο 1 έως 6: '+item.value['1to6'];
    var to8 = 'Απο 7 έως 8: '+item.value['7to8'];
    var to10 = 'Απο 9 έως 10: '+item.value['9to10'];
    var row = $("<div></div>").attr('class','row');
    row.html('<div class="col-4">'+to6+'</div><div class="col-4">'+to8+'</div><div class="col-4">'+to10+'</div>');
    var screenshot = $("<div></div>").attr('class','screenshot btn btn-light btn-sm');
    screenshot.attr('id','screenshot'+counter);
    screenshot.text('Screenshot');
    var chart = $("<div></div>").attr('class','charts');
    chart.attr('id','chart'+counter);
    $('#'+element).append(quest,avg,row,screenshot,chart);
    $('#'+element).append("<hr>");
    var itemsum = item.value['1to6']+item.value['7to8']+item.value['9to10'];
    if (itemsum == 0) itemsum++;
    var values = {to6:(item.value['1to6']/itemsum)*100,
                  to8:(item.value['7to8']/itemsum)*100,
                  to10:(item.value['9to10']/itemsum)*100};
    var options = myoptions(type);
    var customOptions = jQuery.extend({}, options);
    customOptions['title'] = 'Από '+dates.date1+' μέχρι'+dates.date2+' '+type+' '+item.questlabel;
    drawStuff(values,counter,customOptions,'column');
    counter++;
    });
    return counter;
}

function ListCreator(data,element,counter,type,dates){
  data.forEach(function(item) {
    var value = item.value['1to6']+item.value['7to8']+item.value['9to10'];
    var quest = $("<li></li>").text(item.questlabel+' '+value);
    quest.attr('class','list-group-item')
    $('#'+element).append(quest);
    });
    return counter;
}
