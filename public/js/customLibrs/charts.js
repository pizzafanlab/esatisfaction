function myoptions(type) {
  var deloptions = {
    width: 500,
    legend: { position: 'none' },
    chart: { title: 'Ποσοστά ικανοποίησης delivery',
             subtitle: '' }
  };
  var takeoptions = {
    width: 500,
    legend: { position: 'none' },
    chart: { title: 'Ποσοστά ικανοποίησης take',
             subtitle: '' }
  };
  var baroptions = {
    width: 500,
    legend: { position: 'none' },
    chart: { title: 'Μέσοι όροι',
             subtitle: '' },
    title: 'Μέσοι όροι'
  };

  var FunctionOptions = {
       curveType: 'function',
       legend: { position: 'bottom' },
       height:400,
       title: 'Στατιστικά'
     };
     switch (type) {
       case 'delivery':
       return deloptions;
         break;
      case 'take':
        return takeoptions;
        break;
      case 'bar':
        return baroptions;
        break;
      case 'bar2':
        return baroptions;
        break;
      case 'function':
        return FunctionOptions;
        break;
        case 'function2':
          return FunctionOptions;
          break;
     }
}

function chartCreator(data,element,counter,type,dates,chartType) {
  counter++;
  var screenshot = $("<div></div>").attr('class','screenshot btn btn-light btn-sm');
  screenshot.attr('id','screenshot'+counter);
  screenshot.text('Screenshot');
  var chart = $("<div></div>").attr('class','charts');
  chart.attr('id','chart'+counter);
  $('#'+element).append(screenshot,chart);
  var options = myoptions(chartType);
  var customOptions = jQuery.extend({}, options);
  customOptions['title'] += ' από '+dates.date1+' μέχρι '+dates.date2+' '+type+' ';
  drawStuff(data,counter,customOptions,chartType);
  return counter;
}

function drawStuff(values,chart,options,type) {
    switch (type) {
        case 'column':
        var chart = new google.visualization.ColumnChart(document.getElementById('chart'+chart));
        var data = new google.visualization.arrayToDataTable([
          ['Κατηγορίες', 'Ποσοστό'],
           ['[1-6]',values.to6],
           ['[7-8]',values.to8],
           ['[9-10]',values.to10]
        ]);
        break;

        case 'bar':
        var chart = new google.visualization.BarChart(document.getElementById('chart'+chart));
        var data = [];
        var Header= ['Ερώτηση', 'Μέσος όρος'];
        data.push(Header);
        values.forEach(function(item) {
          data.push([item.questlabel,parseFloat(item.avg)]);
        });
        var data = new google.visualization.arrayToDataTable(data);
        break;

        case 'bar2':
        var chart = new google.visualization.BarChart(document.getElementById('chart'+chart));
        var data = [];
        var Header= ['Ερώτηση', 'Τιμή Α','Τιμή Β'];
        data.push(Header);
        values.forEach(function(item) {
          data.push([item.questlabel,parseFloat(item.compA),parseFloat(item.compB)]);
        });
        var data = new google.visualization.arrayToDataTable(data);
        break;

        case 'function':
        var chart = new google.visualization.LineChart(document.getElementById('chart'+chart));
        var data = [];
         var Header= ['Ημέρα', 'Delivery', 'Take'];
         data.push(Header);
         values.forEach(function(item) {
          data.push([item.day,parseFloat(item.delScore),parseFloat(item.takeScore)]);
        });
        var data = new google.visualization.arrayToDataTable(data);
        break;

        case 'function2':
        var chart = new google.visualization.LineChart(document.getElementById('chart'+chart));
        var data = [];
         var Header= ['Ημέρα', 'Delivery', 'Take'];
         data.push(Header);
         values.forEach(function(item) {
          data.push([item.day,parseFloat(item.delNps),parseFloat(item.takeNps)]);
        });
        var data = new google.visualization.arrayToDataTable(data);
        break;
    }
  chart.draw(data,options);
}
