
function FormsMatcher(){
  var startDate = $( '#dateSearch input[name=startDate]').val();
  var endDate = $( '#dateSearch input[name=endDate]').val();
  var minSatisf = $( '#dateSearch input[name=minSatisf]').val();
  var maxSatisf = $( '#dateSearch input[name=maxSatisf]').val();
  $( '#Export input[name=startDate]').val(startDate);
  $( '#Export input[name=endDate]').val(endDate);
  $( '#Export input[name=minSatisf]').val(minSatisf);
  $( '#Export input[name=maxSatisf]').val(maxSatisf);
  $( '#ExportPerStore input[name=startDate]').val(startDate);
  $( '#ExportPerStore input[name=endDate]').val(endDate);
}

function downloadCSV(args,csv) {
  var data, filename, link;
  // var csv = convertArrayOfObjectsToCSV({
  //     data: stockData
  // });
  if (csv == null) return;

  filename = args.filename || 'export.csv';
  var csv = "\ufeff"+ csv;
  if (!csv.match(/^data:text\/csv/i)) {
      csv = 'data:text/csv;charset=utf-8,' + csv;
  }
  data = encodeURI(csv);

  link = document.createElement('a');
  link.setAttribute('href', data);
  link.setAttribute('download', filename);
  link.click();
}

function convertArrayOfObjectsToCSV(args) {
      var result, ctr, keys, columnDelimiter, lineDelimiter, data;

      data = args || null;
      if (data == null || !data.length) {
          return null;
      }

      columnDelimiter = args.columnDelimiter || ',';
      lineDelimiter = args.lineDelimiter || '\n';

      keys = Object.keys(data[0]);

      result = '';
      result += keys.join(columnDelimiter);
      result += lineDelimiter;

      data.forEach(function(item) {
          ctr = 0;
          keys.forEach(function(key) {
              if (ctr > 0) result += columnDelimiter;

              result += item[key];
              ctr++;
          });
          result += lineDelimiter;
      });
    //  console.log(result);
      return result;
  }

function ArrayDataExport(data){
var result = [];
data.forEach(function(item) {
    var obj = {
      Order_ID: item.data.order_id,
      Order_Date: item.stamp,
      Completed: item.data.stampCompleted,
      Overall_score: item.data.questions.generalScore,
      Order_type: item.data.order_type,
      Εστιατόριο: item.data.intro.restaurant,
      //indicators: item.data.questions.indicators,
      //comment: item.data.questions.comment.comment
    };
    item.data.questions.general.forEach(function(item2) {
      obj[item2.questlabel.substring(0,15)] = item2.value;
      });

    item.data.questions.indicators.forEach(function(item3) {
      obj[item3.questlabel] = item3.value;
      });

      obj['product'] = '';
      item.data.questions.products.forEach(function(item3) {
        obj['product'] += item3.prod_name +' '+ item3.value+'/5 ';
        if (item3.comment == null) {
          obj['product'] += 'Σχόλιο: -'
        }
        else {
          obj['product'] += 'Σχόλιο: '+item3.comment;
        }
        });

    if (item.data.questions.comment.comment == null){
      obj['Σχόλιο'] = '-';
    }else {
      obj['Σχόλιο'] = item.data.questions.comment.comment;
    }
    result.push(obj);
});
return result;
}

function ArrayRestExport(data){
var result = [];
data.forEach(function(item) {
  //console.log(item);
  var obj = {};
  for (var property in item) {
      if (item.hasOwnProperty(property)) {
        obj[property] = item[property];
      }
  }
    result.push(obj);
});
return result;
}
