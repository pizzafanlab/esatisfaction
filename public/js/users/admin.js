jQuery(document).ready(function() {

  $( function() {
      $( "#tabs" ).tabs({
        "activate": function(event, ui) {
          var table = $.fn.dataTable.fnTables(true);
          if ( table.length > 0 ) {
              $(table).dataTable().fnAdjustColumnSizing();
          }
      }
      }

      );
    } );



$('#UsersMessagebox1').hide();
$('#UsersMessagebox2').hide();
$('#UsersMessagebox3').hide();
$('#AdminMessagebox').hide();
$('#AdminMessagebox2').hide();
$('#EmailRestMessagebox').hide();
$('#QuestMessagebox').hide();

$('#flagform').hide();

$('#QuestCategory').change(function(){

  if ($('#QuestCategory').val() == 'general') {
    $('#flagform').show();
  }
  else {
    $('#flagform').hide();
  }
});


$('.changeQuestPicker').change(function(){

  if (this.id == 'DelStatus') {
    $('#changeQuestlabel').val($( "#DelStatus option:selected" ).text());
    $('#changeQuestval').val($( "#DelStatus option:selected" ).val());
  }
  else {
    $('#changeQuestlabel').val($( "#TakeStatus option:selected" ).text());
    $('#changeQuestval').val($( "#TakeStatus option:selected" ).val());
  }

});

$('.changeQuestEdit').change(function(){

  if (this.id == 'DelEdit') {
    $('#EditQuestlabel').val($( "#DelEdit option:selected" ).text());
    $('#EditQuestval').val($( "#DelEdit option:selected" ).val());
  }
  else {
    $('#EditQuestlabel').val($( "#TakeEdit option:selected" ).text());
    $('#EditQuestval').val($( "#TakeEdit option:selected" ).val());
  }

});

RefreshTables();
});

function showData(data){
  $("#usersTable").dataTable().fnDestroy();
  $('#usersTable').DataTable( {
    "sScrollY": "200px",
        "bScrollCollapse": true,
        "bPaginate": false,
        "bJQueryUI": true,
        "aoColumnDefs": [
            { "sWidth": "10%", "aTargets": [ -1 ] }
        ],
   data: data,
       columns: [
           { data: 'name' },
           { data: 'surname' },
           { data: 'email' },
           { data: 'area' },
           { data: 'number' },
           { data: 'Restaurants' },
           { data: 'notify' },
           { data: 'active' },
           { data: 'confirmed' },
           { data: 'emails' },
       ]
   } );
}

function showAdminData(data){
  $("#adminsTable").dataTable().fnDestroy();
  $('#adminsTable').DataTable( {
    "sScrollY": "200px",
        "bScrollCollapse": true,
        "bPaginate": false,
        "bJQueryUI": true,
        "aoColumnDefs": [
            { "sWidth": "10%", "aTargets": [ -1 ] }
        ],
   data: data,
       columns: [
           { data: 'name' },
           { data: 'surname' },
           { data: 'email' },
           { data: 'notify' },
           { data: 'number' },
           { data: 'active' },
           { data: 'confirmed' },
           { data: 'emails' },
       ]
   } );
}

function showModsData(data){
  $("#modsTable").dataTable().fnDestroy();
  $('#modsTable').DataTable( {
    "sScrollY": "200px",
        "bScrollCollapse": true,
        "bPaginate": false,
        "bJQueryUI": true,
        "aoColumnDefs": [
            { "sWidth": "10%", "aTargets": [ -1 ] }
        ],
   data: data,
       columns: [
           { data: 'name' },
           { data: 'surname' },
           { data: 'email' },
           { data: 'notify' },
           { data: 'number' },
           { data: 'active' },
           { data: 'confirmed' },
           { data: 'emails' },
       ]
   } );
}

function showEmailsData(data){
  $("#RestEmailsTable").dataTable().fnDestroy();
  $('#RestEmailsTable').DataTable( {
    "sScrollY": "200px",
        "bScrollCollapse": true,
        "bPaginate": false,
        "bJQueryUI": true,
        "aoColumnDefs": [
            { "sWidth": "10%", "aTargets": [ -1 ] }
        ],
   data: data,
       columns: [
           { data: 'id' },
           { data: 'email' },
           { data: 'notify' },
       ]
   } );
}

function showDelQuestData(data){
  $("#DelQuestsTable").dataTable().fnDestroy();
  $('#DelQuestsTable').DataTable( {
    "sScrollY": "200px",
        "bScrollCollapse": true,
        "bPaginate": false,
        "bJQueryUI": true,
        "aoColumnDefs": [
            { "sWidth": "10%", "aTargets": [ -1 ] }
        ],
   data: data,
       columns: [
           { data: 'category' },
           { data: 'questlabel' },
           { data: 'flag' },
           { data: 'status' },
       ]
   } );
}

function showTakeQuestData(data){
  $("#TakeQuestsTable").dataTable().fnDestroy();
  $('#TakeQuestsTable').DataTable( {
    "sScrollY": "200px",
        "bScrollCollapse": true,
        "bPaginate": false,
        "bJQueryUI": true,
        "aoColumnDefs": [
            { "sWidth": "10%", "aTargets": [ -1 ] }
        ],
   data: data,
       columns: [
           { data: 'category' },
           { data: 'questlabel' },
           { data: 'flag' },
           { data: 'status' },
       ]
   } );
}

function RefreshTables(){
  UsersRetrieve();
  AdminsRetrieve();
  ModsRetrieve();
  RestaurantPicker();
  RestEmailsRetrieve();
  QuestsRetrieve()
}

function UsersRetrieve(){
  var $form = $( '#RetrieveUsers' ),
  url = '/api/admin/RetrieveUsers';
  // url = $form.attr( "action" );
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    showData(data.user_data);
  });
}

function AdminsRetrieve(){
  var $form = $( '#RetrieveAdmins' ),
  url = '/api/admin/RetrieveAdmins';
  // url = $form.attr( "action" );
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    showAdminData(data.user_data);
  });
}

function ModsRetrieve(){
  var $form = $( '#RetrieveAdmins' ),
  url = '/api/admin/RetrieveMods';
  // url = $form.attr( "action" );
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    showModsData(data.user_data);
  });
}

function RestEmailsRetrieve(){
  var $form = $( '#RetrieveRestEmails' ),
  url = '/api/admin/RetrieveRestEmails';
  // url = $form.attr( "action" );
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    showEmailsData(data.emails_data);
  });
}

function ConfirmUser(event) {
  event.preventDefault();
  var $form = $( '#ConfirmUser' ),
  url = '/api/admin/ConfirmUser';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#UsersMessagebox1').show();
    $('#UsersMessagebox1').text(data.message);
    RefreshTables();
  });
}

function EnableUser(event) {
  event.preventDefault();
  var $form = $( '#ChangeStatus' ),
  url = '/api/admin/EnableUser';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#UsersMessagebox1').show();
    $('#UsersMessagebox1').text(data.message);
    RefreshTables();
  });
}

function DisableUser(event) {
  event.preventDefault();
  var $form = $( '#ChangeStatus' ),
  url = '/api/admin/DisableUser';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#UsersMessagebox1').show();
    $('#UsersMessagebox1').text(data.message);
    RefreshTables();
  });
}

function MakeAdmin(event){
  event.preventDefault();
  var $form = $( '#MakeAdmin' ),
  url = '/api/admin/MakeAdmin';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#UsersMessagebox1').show();
    $('#UsersMessagebox1').text(data.message);
    RefreshTables();
  });
}

function ChangeUserNotify(event){
  event.preventDefault();
  var $form = $( '#ChangeUserNotify' ),
  url = '/api/admin/ChangeUserNotify';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#UsersMessagebox3').show();
    $('#UsersMessagebox3').text(data.message);
    RefreshTables();
  });
}


function AddSecEmail(event){
  event.preventDefault();
  var $form = $( '#ChangeEmail' ),
  url = '/api/admin/AddUserEmails';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#UsersMessagebox2').show();
    $('#UsersMessagebox2').text(data.message);
    RefreshTables();
  });
}

function RemoveSecEmail(event){
  event.preventDefault();
  var $form = $( '#ChangeEmail' ),
  url = '/api/admin/RemoveUserEmails';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#UsersMessagebox2').show();
    $('#UsersMessagebox2').text(data.message);
    RefreshTables();
  });
}

function AddRestaurant(event){
  event.preventDefault();
  var $form = $( '#ChangeRestaurant' ),
  url = '/api/admin/AddUserRestaurants';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#UsersMessagebox2').show();
    $('#UsersMessagebox2').text(data.message);
  RefreshTables();
  });
}
function RemoveRestaurant(event){
  event.preventDefault();
  var $form = $( '#ChangeRestaurant' ),
  url = '/api/admin/DeleteUserRestaurants';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#UsersMessagebox2').show();
    $('#UsersMessagebox2').text(data.message);
    RefreshTables();
  });
}

function EnableArea(event){
  event.preventDefault();
  var $form = $( '#ChangeArea' ),
  url = '/api/admin/EnableArea';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#UsersMessagebox2').show();
    $('#UsersMessagebox2').text(data.message);
    RefreshTables();
  });
}

function DisableArea(event){
  event.preventDefault();
  var $form = $( '#ChangeArea' ),
  url = '/api/admin/DisableArea';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#UsersMessagebox2').show();
    $('#UsersMessagebox2').text(data.message);
    RefreshTables();
  });
}

function MakeMod(event){
  event.preventDefault();
  var $form = $( '#MakeModerator' ),
  url = '/api/admin/EnableMod';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#UsersMessagebox3').show();
    $('#UsersMessagebox3').text(data.message);
    RefreshTables();
  });
}

function AddAdminSecEmail(event){
  event.preventDefault();
  var $form = $( '#ChangeAdminEmail' ),
  url = '/api/admin/AddUserEmails';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#AdminMessagebox').show();
    $('#AdminMessagebox').text(data.message);
    RefreshTables();
  });
}

function RemoveAdminSecEmail(event){
  event.preventDefault();
  var $form = $( '#ChangeAdminEmail' ),
  url = '/api/admin/RemoveUserEmails';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#AdminMessagebox').show();
    $('#AdminMessagebox').text(data.message);
    RefreshTables();
  });
}
function EnableAdmin(event) {
  event.preventDefault();
  var $form = $( '#ChangeAdminStatus' ),
  url = '/api/admin/EnableUser';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#AdminMessagebox').show();
    $('#AdminMessagebox').text(data.message);
    RefreshTables();
  });
}

function DisableAdmin(event) {
  event.preventDefault();
  var $form = $( '#ChangeAdminStatus' ),
  url = '/api/admin/DisableUser';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#AdminMessagebox').show();
    $('#AdminMessagebox').text(data.message);
    RefreshTables();
  });
}

function ChangeAdminNotify(event){
  event.preventDefault();
  var $form = $( '#ChangeAdminNotify' ),
  url = '/api/admin/ChangeUserNotify';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#AdminMessagebox').show();
    $('#AdminMessagebox').text(data.message);
    RefreshTables();
  });
}

function AddModSecEmail(event){
  event.preventDefault();
  var $form = $( '#ChangeModEmail' ),
  url = '/api/admin/AddUserEmails';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#AdminMessagebox2').show();
    $('#AdminMessagebox2').text(data.message);
    RefreshTables();
  });
}

function RemoveModSecEmail(event){
  event.preventDefault();
  var $form = $( '#ChangeModEmail' ),
  url = '/api/admin/RemoveUserEmails';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#AdminMessagebox2').show();
    $('#AdminMessagebox2').text(data.message);
    RefreshTables();
  });
}
function EnableMod(event) {
  event.preventDefault();
  var $form = $( '#ChangeModStatus' ),
  url = '/api/admin/EnableUser';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#AdminMessagebox2').show();
    $('#AdminMessagebox2').text(data.message);
    RefreshTables();
  });
}

function DisableMod(event) {
  event.preventDefault();
  var $form = $( '#ChangeModStatus' ),
  url = '/api/admin/DisableUser';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#AdminMessagebox2').show();
    $('#AdminMessagebox2').text(data.message);
    RefreshTables();
  });
}

function ChangeModNotify(event){
  event.preventDefault();
  var $form = $( '#ChangeModNotify' ),
  url = '/api/admin/ChangeUserNotify';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#AdminMessagebox2').show();
    $('#AdminMessagebox2').text(data.message);
    RefreshTables();
  });
}

//
function AddRestEmail(event){
  event.preventDefault();
  var $form = $( '#AddRestEmail' ),
  url = '/api/admin/CreateRestEmail';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#EmailRestMessagebox').show();
    $('#EmailRestMessagebox').text(data.message);
    RefreshTables();
  });
}

function UpdateRestEmail(event){
  event.preventDefault();
  var $form = $( '#UpdateRestEmail' ),
  url = '/api/admin/UpdateRestEmail';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#EmailRestMessagebox').show();
    $('#EmailRestMessagebox').text(data.message);
    RefreshTables();
  });
}

function ChangeRestNotify(event){
  event.preventDefault();
  var $form = $( '#ChamgeRestNotify' ),
  url = '/api/admin/ChangeRestNotify';
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#EmailRestMessagebox').show();
    $('#EmailRestMessagebox').text(data.message);
    RefreshTables();
  });
}

//Questions
function QuestsRetrieve(){
  var $form = $( '#RetrieveQuests' ),
  url = '/api/admin/RetrieveQuestions';
  // url = $form.attr( "action" );
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    showDelQuestData(data.del_data);
    showTakeQuestData(data.take_data);

    data.del_data.forEach(function(element) {
      var option = $("<option></option>").attr('value',element.id);
         option.text(element.status+'-'+element.questlabel);
         $(".delquestpicker").append(option);
    });

    data.take_data.forEach(function(element) {
      var option = $("<option></option>").attr('value',element.id);
         option.text(element.status+'-'+element.questlabel);
         $(".takequestpicker").append(option);
    });
  });
}

function AddQuest(){
  event.preventDefault();
  var $form = $( '#AddQuest' ),
  url = '/api/admin/AddQuestion';
  $('#QuestMessagebox').hide();
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#QuestMessagebox').show();
    $('#QuestMessagebox').text(data);
    RefreshTables();
  });
}

function DisableQuest(){
  event.preventDefault();
  var $form = $( '#SatusQuest' ),
  url = '/api/admin/DeleteQuestion';
  $('#QuestMessagebox').hide();
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#QuestMessagebox').show();
    $('#QuestMessagebox').text(data);
    RefreshTables();
  });
}

function ActivateQuest(){
  event.preventDefault();
  var $form = $( '#SatusQuest' ),
  url = '/api/admin/EnableQuestion';
  $('#QuestMessagebox').hide();
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#QuestMessagebox').show();
    $('#QuestMessagebox').text(data);
    RefreshTables();
  });
}

function EditQuest(){
  event.preventDefault();
  var $form = $( '#EditQuest' ),
  url = '/api/admin/EditQuestion';
  $('#QuestMessagebox').hide();
  var posting = $.post( url, $form.serialize() );
  posting.done(function( data ) {
    $('#QuestMessagebox').show();
    $('#QuestMessagebox').text(data);
    RefreshTables();
  });
}

function RestaurantPicker() {
  var $form = $( '#RetrieveUsers' ),
  url = '/api/admin/Restaurants';
  var posting = $.post( url, $form.serialize() );
  $(".restPicker").html('');
  posting.done(function( data ) {
    data.restaurants.forEach(function(element) {
      var option = $("<option></option>").attr('value',element.id);
         option.text(element.name);
         $(".restPicker").append(option);
    });
  });
}
