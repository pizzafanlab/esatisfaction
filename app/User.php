<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Jenssegers\Mongodb\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $connection = 'mongodb';
    protected $collection = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','surname','password','number','api_token','confirmed',
        'active','isArea','isAdmin','Restaurants','other_emails','Νotifications',
        'Moderator'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','api_token',
    ];

    public function Enable($email){

      $user = User::where('email','=',$email)->first();
      $result = new \stdClass;
      if ($user->confirmed) {
        if (!$user->active) {
          $user->active = true;
          if (!$user->Νotifications) {
            $user->Νotifications = true;
          }
          $user->save();
          $result->success = true;
        }else {
          $result->success = false;
          $result->message = 'Ο χρήστης είναι ήδη ενεργοποιημένος';
        }
      }
      else {
        $result->success = false;
        $result->message = 'Ο χρήστης δεν έχει επιβεβαιωθεί';
      }
      return $result;
    }

    public function Disable($email){

      $user = User::where('email','=',$email)->first();
      $result = new \stdClass;
      if ($user->confirmed) {
        if ($user->active) {
          $user->active = false;
          $user->Νotifications = false;
          $user->save();
          $result->success = true;
        }else {
          $result->success = false;
          $result->message = 'Ο χρήστης είναι ήδη απενεργοποιημένος';
        }
      }
      else {
        $result->success = false;
        $result->message = 'Ο χρήστης δεν έχει επιβεβαιωθεί';
      }
        return $result;
    }

    public function Confirm($email){
      $user = User::where('email','=',$email)->first();
      if (!$user->confirmed) {
        $user->confirmed = true;
        $user->save();
        return true;
      }else {
        return false;
      }
    }

    public function AddEmails($email,$other_email)
    {
      $user = User::where('email','=',$email)->first();
      $existed_emails = $user->other_emails;
      if (in_array($other_email,$existed_emails)||$other_email==$user->email) {
        return false;
      }
      else {
        array_push($existed_emails,$other_email);
        $user->other_emails = $existed_emails;
        $user->save();
        return true;
      }
    }

    public function RemoveEmails($email,$other_email)
    {
      $user = User::where('email','=',$email)->first();
      $existed_emails = $user->other_emails;
      $new_emails = array();
      if (in_array($other_email,$existed_emails)) {
        foreach ($existed_emails as $key => $value) {
          if (!($other_email==$value)) {
            array_push($new_emails,$value);
          }
        }
        $user->other_emails = $new_emails;
        $user->save();
        return true;
      }
      else {
        return false;
      }
    }
    // public function Unconfirm($email){
    //   $user = User::where('email','=',$email)->first();
    //   if ($user->confirmed) {
    //     $user->confirmed = false;
    //     $user->save();
    //     return true;
    //   }else {
    //     return false;
    //   }
    // }

    public function AddRestaurants($email,$id)
    {
      $user = User::where('email','=',$email)->first();
      $Restaurants = array();
      $Restaurants = $user->Restaurants;

        if (!in_array($id,$Restaurants)) {
          array_push($Restaurants,$id);
          $user->Restaurants = $Restaurants;
          $user->save();
          return true;
        }
        else {
          return false;
        }
    }

    public function DeleteRestaurants($email,$id)
    {
      $user = User::where('email','=',$email)->first();
      $Restaurants = array();
      $Restaurants = $user->Restaurants;

      if (($key = array_search($id, $Restaurants)) !== false) {
            unset($Restaurants[$key]);
            array_values($Restaurants);
            $user->Restaurants = $Restaurants;
            $user->save();
            return true;
        }
        else {
            return false;
        }
    }

    public function EnableArea($email)
    {
      $user = User::where('email','=',$email)->first();
      if ($user->isArea) {
        return false;
      }
      else {
        $user->isArea = true;
        $user->save();
        return true;
      }
    }

    public function DisableArea($email)
    {
      $user = User::where('email','=',$email)->first();
      $result = new \stdClass;
      if (!$user->isArea) {
        $result->success = false;
        $result->message = 'Ο Χρήστης δεν είναι Area';
      }
      else {

        if (count($user->Restaurants)>1) {
          $result->success = false;
          $result->message = 'Ο Area για να απενεργοποιηθεί πρέπει να έχει μοναδικό εστιατόριο';
        }
        else {
          $user->isArea = false;
          $user->save();
          $result->success = true;
          $result->message = 'Ο Area απενεργοποιήθηκε';
        }

      }
      return $result;
    }

    public function EnableMod($email)
    {
      $user = User::where('email','=',$email)->first();
      if ($user->Moderator) {
        return false;
      }
      else {
        $empty = array();
        $user->isArea = false;
        $user->Restaurants = $empty;
        $user->Moderator = true;
        $user->save();
        return true;
      }
    }

    public function MakeAdmin($email)
    {
      $user = User::where('email','=',$email)->first();
      $result = new \stdClass;
      if (!$user->isAdmin) {
        if ($user->active && $user->confirmed) {
          $empty = array();
          $user->isArea = false;
          $user->Restaurants = $empty;
          $user->isAdmin = true;
          $user->save();
          $result->success = true;
          $result->message = 'Ο Χρήστης άλλαξε σε Διαχειριστής';
        }
        else {
          $result->success = false;
          $result->message = 'Ο χρήστης δεν είναι ενεργοποιημένος ή/και επιβεβαιωμένος';
        }
      }
      else {
        $result->success = false;
        $result->message = 'Ο χρήστης είναι ήδη Διαχειριστής';
      }
      return $result;
    }

    public function ChangeNotify($email)
    {
      $user = User::where('email','=',$email)->first();

      if ($user->Νotifications) {
        $user->Νotifications = false;
        $user->save();
        return false;
      }
      else {
        $user->Νotifications = true;
        $user->save();
        return true;
      }
    }


}
