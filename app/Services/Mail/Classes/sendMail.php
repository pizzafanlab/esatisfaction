<?php
namespace App\Services\Mail\Classes;
use Mail;
use App\Services\Questionnaires\Models\Questionnaire;
use App\Services\Mail\Models\mailLog;
use DateTime;

class sendMail
{

  public function send($id,$hash,$address)
  {
    // $mail_address =  decrypt($address);
    // $link = 'https://satisfaction.pizzafan.gr/Quest?id='.$id.'&hash='.$hash;
    $link = \Config::get('constant.server.name').'/Quest?id='.$id.'&hash='.$hash;
    $subject = 'Ερωτηματολόγιο Pizzafan';
    $sender = 'no-reply@pizzafan-offers.gr';
    $sender_name =  \Config::get('constant.mail.sender_name');

    // $link2 = $_SERVER['SERVER_NAME'].'/unsubscribe'.'/Quest?id='.$id.'&hash='.$hash;
    // $link2 = 'https://satisfaction.pizzafan.gr//unsubscribe/Quest?id='.$id.'&hash='.$hash;
    $link2 = \Config::get('constant.server.name').'/unsubscribe/Quest?id='.$id.'&hash='.$hash;
    $data = array('link'=>$link,'subject'=>$subject,'email'=>$address,
                  'sender'=>$sender,'sender_name'=>$sender_name,'link2'=>$link2);
     Mail::send('email.mail', $data, function($message)use ($data) {
          $message->to($data['email'])
                  ->subject($data['subject']);
          $message->from($data['sender'],$data['sender_name']);
         });
     try {
       $log = mailLog::create(['email' => encrypt($address), 'quest_id' => $id, 'stamp' => new DateTime()]);
     } catch (\Exception $e) {
       $exception = $e->getMessage();
       Log::info($exception);
     }
   }

   public function sendSubmitted($id,$hash,$address)
   {
     // $link = 'https://satisfaction.pizzafan.gr/Quest?id='.$id.'&hash='.$hash;


     // $link = \Config::get('constant.server.name').'/Quest?id='.$id.'&hash='.$hash;
     // $subject = 'Ερωτηματολόγιο για την παραγγελία #'.$id;
     // $sender = 'no-reply@pizzafan-offers.gr';
     // $sender_name =  \Config::get('constant.mail.sender_name');
     //
     //
     // $data = array('link'=>$link,'subject'=>$subject,'email'=>$address,'sender'=>$sender,'sender_name'=>$sender_name);
     // Mail::send('email.submitted', $data, function($message)use ($data) {
     //   $message->to($data['email'])
     //           ->subject($data['subject']);
     //   $message->from($data['sender'],$data['sender_name']);
     //  });

     $link = \Config::get('constant.server.name').'/Quest?id='.$id.'&hash='.$hash;
     $subject = 'Ερωτηματολόγιο για την παραγγελία #'.$id;
     $sender = 'no-reply@pizzafan-offers.gr';
     $sender_name =  \Config::get('constant.mail.sender_name');
     $result = Questionnaire::where('data.quest_id','=',intval($id))->first();
     $quest = json_decode(json_encode($result));
     $date = $result->stamp->setTimezone('Europe/Athens');
     $date = $date->format("Y-m-d H:i");
     $data = array('link'=>$link,'subject'=>$subject,'email'=>$address,'sender'=>$sender,'sender_name'=>$sender_name,
                  'quest' => $quest,'date' => $date);

    // dd(print_r($data));
     Mail::send('filledQuest', $data, function($message)use ($data) {
       $message->to($data['email'])
               ->subject($data['subject']);
       $message->from($data['sender'],$data['sender_name']);
      });
   }

}

 ?>
