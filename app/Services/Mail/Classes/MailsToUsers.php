<?php
namespace App\Services\Mail\Classes;
use  App\Services\Mail\Models\modelMail;
use  App\Services\Mail\Models\Restaurant;
use App\Services\Questionnaires\Models\Questionnaire;
use App\User;


class MailsToUsers{

  public function sendMails($quest_id)
  {
    $quest_id = intval($quest_id);

     $mail = modelMail::where('quest_id','=',$quest_id)->first();
     if (isset($mail->sentUsers)) {
       if (!$mail->sentUsers) {
         $mailobj = json_decode(json_encode($mail));
         $mail->sentUsers = true;
         $mail->save();
        // // $send = app()->make('sendMail')->send($mailobj->quest_id,$mailobj->hash,$mailobj->Storeaddress);
        $quest = Questionnaire::where('data.quest_id','=',$quest_id)->first();


        $restaurant = $quest->rest_id;

        $mailsList =  $this->MailsList($restaurant);

        foreach ($mailsList as $email) {
         $send = app()->make('sendMail')->sendSubmitted($mailobj->quest_id,$mailobj->hash,$email);
        }
       }
     }
     else {
       $mailobj = json_decode(json_encode($mail));
      // // $send = app()->make('sendMail')->send($mailobj->quest_id,$mailobj->hash,$mailobj->Storeaddress);
      $quest = Questionnaire::where('data.quest_id','=',$quest_id)->first();


      $restaurant = $quest->rest_id;

      $mailsList =  $this->MailsList($restaurant);

      foreach ($mailsList as $email) {
       $send = app()->make('sendMail')->sendSubmitted($mailobj->quest_id,$mailobj->hash,$email);
      }
     }

  }

  public function MailsList($restaurant)
  {
    $list = array();
    $restaurant = strval($restaurant);
    $users = User::where("Restaurants","=",$restaurant)
                  ->where("active","=",true)
                  ->where("Νotifications","=",true)
                  ->get();

    foreach ($users as $user) {
      $user_email = $user->email;
      if (!in_array($user_email,$list)) {
        array_push($list,$user_email);
      }

      $other_emails = $user->other_emails;

      foreach ($other_emails as $email) {
        if (!in_array($email,$list)) {
          array_push($list,$email);
        }
      }
    }

    $admins = User::where("active","=",true)
                  ->where("isAdmin","=",true)
                  ->where("Νotifications","=",true)
                  ->get();

    foreach ($admins as $admin) {
      $admin_email = $admin->email;
      if (!in_array($admin_email,$list)) {
        array_push($list,$admin_email);
      }

      $other_emails = $admin->other_emails;
      foreach ($other_emails as $email) {
        if (!in_array($email,$list)) {
          array_push($list,$email);
        }
      }
    }

    $mods = User::where("active","=",true)
                  ->where("Moderator","=",true)
                  ->where("Νotifications","=",true)
                  ->get();

    foreach ($mods as $mod) {
      $mod_email = $mod->email;
      if (!in_array($mod_email,$list)) {
        array_push($list,$mod_email);
        }
    }


    $rest = Restaurant::where("rest_id","=",intval($restaurant))
                      ->first();

    if (!empty($rest)) {
      if ($rest['notify']) {
        $rest_email = $rest['email'];
        if (!in_array($rest_email,$list)) {
          array_push($list,$rest_email);
        }
      }
    }

    return $list;
  }

}


 ?>
