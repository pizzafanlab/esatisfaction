<?php
namespace App\Services\Mail\Classes;
use  App\Services\Mail\Models\modelMail;
use App\Services\Mail\Models\mailLog;
use App\Services\Questionnaires\Models\Questionnaire;
use App\Services\Orders\Models\Customer;
use Illuminate\Support\Facades\Log;
use DateTime;
use DateTimeZone;

class checkMail
{
  public function check()
  {
    $mails = modelMail::where('sent','=',false)
                      ->where('cancelled','=',false)
                      ->where('stamp','>',new DateTime('-2 days'))
                      ->get();

    $toUpdate  = array();

    foreach ($mails as $mail) {
      if ($mail->failed || $this->isUnsubscribed($mail->quest_id)) continue;

      if ($this->timeCheck($mail->stamp)) {
        try {
            $email = decrypt($mail->address);
            $send = app()->make('sendMail')->send($mail->quest_id,$mail->hash,$email);
            array_push($toUpdate,$mail->quest_id);
            // try {
            //   $log = mailLog::create(['email' => $mail->address, 'quest_id' => $mail->quest_id, 'stamp' => new DateTime()]);
            // } catch (\Exception $e) {
            //   $exception = $e->getMessage();
            //   Log::info($exception);
            // }

        } catch (DecryptException  $c ) {
          $exception = $c->getMessage();
          Log::info($exception);
        }
         catch (\Exception $e ) {
           $exception = $e->getMessage();
           Log::info($exception);
           try {
             $mail->failed = true;
             $mail->save();
             continue;
           } catch (\Exception $e) {
             $exception = $e->getMessage();
             Log::info($exception);
             continue;
           }

        }
      }
    }

      try {
        modelMail::whereIn('quest_id',$toUpdate)->update(['sent' => true]);
      } catch (\Exception $e) {
        $exception = $e->getMessage();
        Log::info($exception);
        \Artisan::call('down');
      }

  }

  public function timeCheck($stamp)
  {
    $curr_date = $this->getTime();
    // $mail_date = new DateTime($stamp);
    $mail_date = $stamp->setTimezone('Europe/Athens');
    $interval = $mail_date->diff($curr_date);
    $daysToH = $interval->d *24;
    $hoursToMin = ($interval->h + $daysToH)* 60;
    $mins = $interval->i;
    $totaldiff = $hoursToMin + $mins;
    $interval = \Config::get('constant.mail.mail_interval');
    // Minutes for sending mail
    if($totaldiff>$interval){
      return true;
    }
    else{
      return false;
    }

  }

  public function getTime()
  {
    date_default_timezone_set('Europe/Athens');
     setlocale(LC_TIME, 'el_GR.UTF-8');
      $current = date("Y-m-d H:i");
      $curr_date = new DateTime($current);
      return $curr_date;
  }

  public function isUnsubscribed($questID)
  {
      try {
        $quest = Questionnaire::where('data.quest_id','=',$questID)->first(['data.customer_id']);
        $customer_id = $quest->data['customer_id'];
        $customer = Customer::where('customer_id','=',$customer_id)->first(['unsubscribe']);
        if ($customer->unsubscribe) {
          return true;
        }
        else {
          return false;
        }
      } catch (\Exception $e) {
        return true;
      }

  }
}


// db.mails.find({
//     stamp: {
//         $lt: new ISODate("2018-06-23T23:59:59Z")
//     },sent:false,cancelled:false}).count()
//
//     db.mails.find({
//         stamp: {
//             $gte: new ISODate("2018-06-23T00:59:59Z"),
//             $lt: new ISODate("2018-06-23T23:59:59Z")
//         }}).count()
//
//         db.mails.find({stamp: {$gte: new ISODate("2018-06-23T02:59:59Z")},sent:false,cancelled:false}).count()
//
//
//         db.mails.updateMany({sent:false,cancelled:false},{$set:{failed:true}})

 ?>
