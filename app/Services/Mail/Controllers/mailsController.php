<?php
namespace App\Services\Mail\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use  App\Services\Mail\Models\modelMail;

class mailsController extends Controller
{
    public function checkMails()
    {
      $check = app()->make('checkMail')->check();
      return 'All pending mails sent successfully';
    }


    public function sendUserMails(Request $request)
    {
      $send = app()->make('MailsToUsers')->sendMails($request->input('quest_id'));
    }

    public function CancelledOrder(Request $request)
    {
      $req = $request->json()->all();
      $inputObj = json_decode(json_encode($req));
      $mail = modelMail::where('quest_id','=',$inputObj->data->order_id)->first();
      if (!empty($mail) && !$mail->cancelled && !$mail->sent) {
        $mail->cancelled = true;
        $mail->save();
        return response()->json([
            'cancelled' => true,
            'message' => 'Order Cancelled'
        ], 201);
      }
      elseif (empty($mail)) {
        return response()->json([
            'cancelled' => false,
            'message' => 'Order not Found'
        ], 200);
      }
      elseif ($mail->cancelled) {
        return response()->json([
            'cancelled' => false,
            'message' => 'Order already cancelled'
        ], 200);
      }
      else {
        return response()->json([
            'cancelled' => false,
            'message' => 'Questionnaire already sent'
        ], 200);
      }
    }

    public function SendTest(Request $request)
    {
      $email = $request->input('email');
      $send = app()->make('sendMail')->send(155555,15555555,$email);
      // $sendsubm = app()->make('sendMail')->sendSubmitted(403574493,15555555,$email);

        return 'mail sent to '.$email;
    }
}

 ?>
