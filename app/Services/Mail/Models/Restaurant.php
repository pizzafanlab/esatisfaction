<?php
namespace App\Services\Mail\Models;
use Illuminate\Support\Facades\Validator;
use Moloquent\Eloquent\Model as Eloquent;

class Restaurant extends Eloquent
{
  protected $collection = 'Restaurants_emails';
  protected $connection = 'mongodb';

  public function Create($id,$email)
  {
    $result = new \stdClass;
    $check = $this->RestaurantCheck($id);
    if ($check->success) {
      $check2 = $this->MailValidator($email);
      if ($check2->success) {
          $check3 = $this->EmailCheck($email);
          if ($check3->success) {
            $this->rest_id = $id;
            $this->email = $email;
            $this->notify = true;
            $this->save();
            $result->success = true;
            $result->message = 'Το κατάστημα προστέθηκε';
          }
          else {
            $result->success = false;
            $result->message = $check3->message;
          }
      }
      else {
        $result->success = false;
        $result->message = $check2->message;
      }
    }
    else {
      $result->success = false;
      $result->message = $check->message;
    }
    return $result;
  }

  public function UpdateEmail($id,$email)
  {
    $result = new \stdClass;
    $check = $this->RestaurantCheck($id);
    if ($check->success) {
      $result->success = false;
      $result->message = 'Το κατάστημα δεν έχει προστεθεί';
    }
    else {
      $check2 = $this->MailValidator($email);
       if ($check2->success) {
           $check3 = $this->EmailCheck($email);
           if ($check3->success) {
            $rest= Restaurant::where('rest_id','=',$id)->first();
             $rest->email = $email;
             // $this->notify = true;
             $rest->save();
             $result->success = true;
             $result->message = 'Η αλλαγή email πραγματοποιήθηκε';
           }
           else {
             $result->success = false;
             $result->message = $check3->message;
           }
          }
        else {
          $result->success = false;
          $result->message = $check2->message;
        }
      }
    return $result;
  }

  public function ChangeNotify($id)
  {
      $result = new \stdClass;
      $check = Restaurant::where('rest_id','=',$id)->first();
      if (empty($check)) {
        $result->success = false;
        $result->message = 'Το κατάστημα δεν έχει προστεθεί';
      }
      else {
          if ($check->notify) {
            $check->notify = false;
            $result->success = true;
            $result->message = 'Οι ειδοποιήσεις απενεργοποιήθηκαν';

          }
          else {
            $check->notify = true;
            $result->success = true;
            $result->message = 'Οι ειδοποιήσεις ενεργοποιήθηκαν';
          }
            $check->save();

      }
      return $result;
  }


  public function RestaurantCheck($id)
  {
    $result = new \stdClass;
    $check = Restaurant::where('rest_id','=',$id)->first();
    if (empty($check)) {
      $result->success = true;
    }
    else {
      $result->success = false;
      $result->message = 'Το κατάστημα υπάρχει ήδη';
    }
    return $result;
  }

  public function EmailCheck($email)
  {
    $result = new \stdClass;
    $check = Restaurant::where('email','=',$email)->first();
    if (empty($check)) {
        $result->success = true;
    }
    else {
      $result->success = false;
      $result->message = 'Το email χρησιμοποιείται απο άλλο Κατάστημα';
    }
    return $result;
  }

  public function MailValidator($email)
  {
    $result = new \stdClass;
    $data = array('email'=>$email);
    $validator = Validator::make($data, [
      'email' => 'required|string|email|max:255',
    ]);

    if ($validator->fails()) {
      $result->success = false;
      $result->message = 'Η δομή του email δεν είναι σωστή';
    }
    else {
      $result->success = true;
    }
    return $result;
  }

}

 ?>
