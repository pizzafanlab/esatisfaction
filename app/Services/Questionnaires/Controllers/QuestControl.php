<?php
namespace App\Services\Questionnaires\Controllers;

use App\Services\Questionnaires\Models\Questionnaire;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use DateTime;

class QuestControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getQuests()
    {
        // return Questionnaire::all();
    }

    public function retrieve(Request $request)
    {
      $date1 = $request->input('startDate').' 00:01';
      $date2 = $request->input('endDate').' 23:59';
      $minSatisf = intval($request->input('minSatisf'));
      $maxSatisf = intval($request->input('maxSatisf'));

      $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                ->where('data.isCancelled', '=',false)
                                                                ->where('data.isComplete', '=',true)
                                                                ->where('data.DBquests','=',true)
                                                                ->where('data.questions.generalScore', '>=',$minSatisf)
                                                                ->where('data.questions.generalScore', '<=',$maxSatisf)
                                                                ->get();

      return response()->json($Quests, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
