<?php
namespace App\Services\Questionnaires\Models;
use Moloquent\Eloquent\Model as Eloquent;

class Questionnaire extends Eloquent {

  protected $collection = 'questionnaires';
  protected $connection = 'mongodb';
  protected $dates = ['stamp'];
  // public function Order()
  //  {
  //      return $this->belongsTo('Order');
  //  }
}
?>
