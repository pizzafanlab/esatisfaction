<?php
namespace App\Services\Questionnaires\Classes;

use App\Services\Questionnaires\Models\Questionnaire;
use Illuminate\Support\Facades\Hash;
use DateTime;


class QuestStorePush
{

  public function getTemplate($temp)
  {
    $quest = new Questionnaire;
    $quest->stamp = new DateTime($this->date());
    $template = json_decode($temp);
    $quest->rest_id = $template->data->rest_id;
    $quest->data = $template->data;
    $quest->data->quest_id = $template->data->order_id;
    $quest->data->isComplete = false;
    $hash = Hash::make($template->data->customer_id);
    $quest->data->hash = $hash;
    $quest->save();
    $mail = app()->make('storeMail')->store($template->data->order_id);
  }

  public function date()
  {
    date_default_timezone_set('Europe/Athens');
     setlocale(LC_TIME, 'el_GR.UTF-8');
     return date("Y-m-d H:i");
  }

}
 ?>
