<?php
namespace App\Services\Questionnaires\Classes;
use App\Services\Questionnaires\Models\Questionnaire;
use App\Services\Mail\Models\modelMail;
use App\Services\QuestionnaireControl\Models\Question;

class FiledQuest
{
  public function update($result,$requests,$quest)
  {

    $prodinputs = $requests['products'];
    $prodcomments = $requests['productsComments'];

    if (!empty($prodinputs)) {
      foreach ($prodinputs as $key => $var) {
          $prodinputs[$key] = (int)$var;
      }

      foreach( $quest->data->questions->products as $index => $product ) {

        $product->value = $prodinputs[$index];
        $product->comment = $prodcomments[$index];

      }
    }

    $geninputs = $requests['generals'];

    foreach ($geninputs as $key => $var) {
        $geninputs[$key] = (int)$var;
    }

    //prin tin allagi ton erotiseon
    // foreach( $quest->data->questions->general as $index => $general ) {
    //
    //     $general->value = $geninputs[$index];
    //
    //
    // }


    foreach( $quest->data->questions->general as $general ) {
      if (array_key_exists($general->_id,$geninputs)) {
        $general->value = $geninputs[$general->_id];
        if($general->flag == 'satisf') $quest->data->questions->generalScore = $geninputs[$general->_id];
      }
    }

    //prin tin allagi ton erotiseon
    // foreach( $quest->data->questions->general as $general ) {
    //
    //   if ($general->flag == 'satisf') {
    //     $quest->data->questions->generalScore = $general->value;
    //     break;
    //   }
    //   else {
    //     $quest->data->questions->generalScore = 0;
    //   }
    //
    // }

    $indinputs = $requests['indicators'];

    foreach ($indinputs as $key => $var) {
        $indinputs[$key] = (int)$var;
    }

    //prin tin allagi ton erotiseon
    // foreach( $quest->data->questions->indicators as $index => $indicator ) {
    //   $indicator->value = $indinputs[$index];
    // }

    foreach( $quest->data->questions->indicators as $indicator ) {
      if (array_key_exists($indicator->_id,$indinputs)) {
        $indicator->value = $indinputs[$indicator->_id];
      }
    }

    if($quest->data->order_type == 'take'){
      $quest->data->questions->method->comment = $requests['comment'];
    }
    else {
      $quest->data->questions->method->value = intval($requests['select']);
      $quest->data->questions->method->comment = $requests['comment'];
    }

    $quest->data->questions->comment->comment = $requests['generalcomment'];

   $result->data = $quest->data;

   $quest->data->isComplete = true;
   $quest->data->stampCompleted = $this->AppendCompleteDate();
   $result->save();

   $input = $quest->data->quest_id;
   $post_data = array(
    'quest_id' => $input
    );
   $curl = curl_init();
   $url = \Config::get('constant.server.name').'/api/services/sendUserMails';

   curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt ($curl, CURLOPT_POST, TRUE);
    curl_setopt ($curl, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($curl, CURLOPT_TIMEOUT, 2);

   curl_exec($curl);

   curl_close($curl);
   $send = app()->make('MailsToUsers')->sendMails($quest->data->quest_id);
  }

  public function updateold($result,$requests,$quest)
  {

    $prodinputs = $requests['products'];
    $prodcomments = $requests['productsComments'];

    if (!empty($prodinputs)) {
      foreach ($prodinputs as $key => $var) {
          $prodinputs[$key] = (int)$var;
      }

      foreach( $quest->data->questions->products as $index => $product ) {

        $product->value = $prodinputs[$index];
        $product->comment = $prodcomments[$index];

      }
    }

    $geninputs = $requests['generals'];

    foreach ($geninputs as $key => $var) {
        $geninputs[$key] = (int)$var;
    }

    foreach( $quest->data->questions->general as $index => $general ) {

        $temp = Question::where('category','=','general')
                ->where('method','=',$quest->data->order_type)
                ->where('questlabel','=',$general->questlabel)->first();

        $general->_id = $temp->_id;
        $general->inputType = $temp->inputType;
        $general->value = $geninputs[$index];
        $general->category = $temp->category;
        $general->flag = $temp->flag;
        $general->method = $temp->method;
        $general->status = $temp->status;
    }

    foreach( $quest->data->questions->general as $general ) {

      if ($general->flag == 'satisf') {
        $quest->data->questions->generalScore = $general->value;
        break;
      }
      else {
        $quest->data->questions->generalScore = 0;
      }

    }

    $indinputs = $requests['indicators'];

    foreach ($indinputs as $key => $var) {
        $indinputs[$key] = (int)$var;
    }

    foreach( $quest->data->questions->indicators as $index => $indicator ) {
      $temp = Question::where('category','=','indicators')
              ->where('method','=',$quest->data->order_type)
              ->where('questlabel','=',$indicator->questlabel)->first();

      $indicator->_id = $temp->_id;
      $indicator->inputType = $temp->inputType;
      $indicator->value = $indinputs[$index];
      $indicator->category = $temp->category;
      $indicator->method = $temp->method;
      $indicator->status = $temp->status;
    }

    if($quest->data->order_type == 'take'){
      $quest->data->questions->method->comment = $requests['comment'];
    }
    else {
      $quest->data->questions->method->value = intval($requests['select']);
      $quest->data->questions->method->comment = $requests['comment'];
    }

    $quest->data->questions->comment->comment = $requests['generalcomment'];

    $quest->data->DBquests = true;
   $quest->data->isComplete = true;
   $quest->data->stampCompleted = $this->AppendCompleteDate();
   $result->data = $quest->data;
   // dd(print_r($quest));
   $result->save();

   $input = $quest->data->quest_id;
   $post_data = array(
    'quest_id' => $input
    );
   $curl = curl_init();
   $url = \Config::get('constant.server.name').'/api/services/sendUserMails';

   curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt ($curl, CURLOPT_POST, TRUE);
    curl_setopt ($curl, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($curl, CURLOPT_TIMEOUT, 2);

   curl_exec($curl);

   curl_close($curl);
   $send = app()->make('MailsToUsers')->sendMails($quest->data->quest_id);
  }

  public function AppendCompleteDate()
  {
    date_default_timezone_set('Europe/Athens');
     setlocale(LC_TIME, 'el_GR.UTF-8');
      $current = date("Y-m-d H:i");
      return $current;
  }

}

 ?>
