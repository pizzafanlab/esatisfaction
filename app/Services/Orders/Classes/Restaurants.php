<?php
namespace App\Services\Orders\Classes;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class Restaurants
{
  public function UpdateRestaurants()
  {
    $file_path = '../app/Services/Orders/JsonData/restaurants.json';
    $api_url='http://api.pizzafan.gr:9001/api/v1/restaurants/';

    // if(file_exists($file_path)){
    //
    //   if ($this->dateChecker($file_path)) {
    //
    //     return $this->JsonCreate($file_path);
    //   }
    //   else {
    //     return 'To products json einai kainourgio';
    //   }
    //
    // }
    // else {
      return $this->JsonCreate($file_path,$api_url);
    // }

  }

  // public function dateChecker($path)
  // {
  //   date_default_timezone_set('Europe/Athens');
  //    setlocale(LC_TIME, 'el_GR.UTF-8');
  //     $current = date("Y-m-d H:i");
  //     $curr_date = new DateTime($current);
  //
  //     $file = file_get_contents($path);
  //     $restObj = json_decode($file);
  //     $json_date = new DateTime($prodObj->stamp);
  //
  //     $interval = $json_date->diff($curr_date);
  //     $daysToH = $interval->d *24;
  //     $hoursToMin = ($interval->h + $daysToH)* 60;
  //     $mins = $interval->i;
  //     $totaldiff = $hoursToMin + $mins;
  //
  //     // Minutes for file caching
  //     if($totaldiff>2){
  //
  //       return true;
  //     }
  //     else{
  //       return false;
  //     }
  // }

  public function JsonCreate($path,$url)
  {
    date_default_timezone_set('Europe/Athens');
    setlocale(LC_TIME, 'el_GR.UTF-8');
    $current = date("Y-m-d H:i");

      try {
        $client = new Client();
          $res = $client->get($url,[
            'max'             => 5,
        ]);
          $restObj = json_decode($res->getBody());
          $restObj->stamp = $current;

          $fh = fopen($path, 'w')
            or die("Error opening output file");
            fwrite($fh,json_encode($restObj,JSON_UNESCAPED_UNICODE));
            fclose($fh);
        } catch (\Exception $ex) {
        }
    // return 'Json file created or updated';
  }

}
 ?>
