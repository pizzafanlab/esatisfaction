<?php
namespace App\Services\Orders\Classes;

use App\Services\Orders\Models\Order;
use App\Services\Orders\Models\Customer;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use DateTime;
use Validator;

class OrderStore
{
  public function store($request)
  {
    $product = app()->make('Products')->UpdateProducts();
    $restaurant = app()->make('Restaurants')->UpdateRestaurants();
    $prodfile = file_get_contents('../app/Services/Orders/JsonData/products.json');
    $restrsfile = file_get_contents('../app/Services/Orders/JsonData/restaurants.json');
    $exclfile= file_get_contents('../app/Services/Orders/JsonData/exlCategories.json');
    $excluded = json_decode($exclfile);
    $prodObj = json_decode($prodfile);
    $restrObj = json_decode($restrsfile);

    $req = $request->json()->all();
    $inputObj = json_decode(json_encode($req));

    $check = Order::where('order_id','=',$inputObj->data->ord_id)->first();

    $validator = Validator::make(
        ['email' => $inputObj->data->customer_data->customer_email],
        ['email' => 'email']
    );

    if ($validator->fails())
    {
      return response()->json([
          'created' => false,
          'message' => 'Email not valid'
      ], 400);
    }

    if (empty($check)) {
        $order = new Order;
        $order->stamp = new DateTime($this->AppendOrderDate());
        $order->order_id = $inputObj->data->ord_id;
        // $order->customer_data = $inputObj->data->customer_data;

        $order->customer_id = $inputObj->data->customer_data->customer_id;
        $mail = $inputObj->data->customer_data->customer_email;
        $order->customer_email = encrypt($mail);
        // $order->customer_email = $inputObj->data->customer_data->customer_email;
        $products = array();
        foreach($inputObj->data->products as $item1)
        {
          if (!in_array($item1->type_id,$excluded->data->excl_cats)) {
            foreach($prodObj->data as $item2)
            {
                if($item1->product_id == $item2->product_id)
                {
                  $prod = new \stdClass();
                  $prod->name=$item2->title->el;
                  $prod->quantity = $item1->quantity;
                  $prod->size_id = $item1->size_id;
                  $prod->prod_id = $item1->product_id;
                  $prod->cat_id = $item1->type_id;
                  //$prod->price = $item1->price;
                  // $prod->coupon = $item1->coupon;
                    array_push($products,$prod);
                }
            }
          }
          elseif ($item1->type_id == 55 && $item1->price>2) {
                $prod = new \stdClass();
                $prod->name= $item1->coupon;
                $prod->quantity = $item1->quantity;
                $prod->size_id = $item1->size_id;
                $prod->prod_id = $item1->product_id;
                $prod->cat_id = $item1->type_id;
                array_push($products,$prod);
          }

        }
        $order->products = $products;

        foreach($restrObj->data as $item3)
        {
            if($item3->restaurant_id == $inputObj->data->restaurant_id)
            {
              $address =  new \stdClass();
              $address->restaurant_id = $item3->restaurant_id;
              $address->restaurant_name = $item3->name;
              $order->address = $address;
              // $order->address = $inputObj->data->order_type;
              // $order->address->restaurant_name = $item3->name;

            }
        }

        if ($inputObj->data->order_type == 'delivery') {
          $order->order_type = 'delivery';
        }
        else {
          $order->order_type = 'take';
        }
        // $order->order_type =$inputObj->data->order_type;

        $customer = new Customer;
        $customer_id = $inputObj->data->customer_data->customer_id;

        if ($customer->MainCheck($customer_id,$order->stamp)) {
          $order->save();
           $this->pushOrderQuestContr($order->order_id);
          return response()->json([
              'created' => true,
              'message' => 'Content created'
          ], 201);
        }
        else {
          return response()->json([
              'created' => false,
              'message' => 'Questionnaire not created'
          ], 400);
        }
    }
    else {
      return response()->json([
          'created' => false,
          'message' => 'Order already submitted'
      ], 400);
    }
  }

  public function AppendOrderDate()
  {
    date_default_timezone_set('Europe/Athens');
     setlocale(LC_TIME, 'el_GR.UTF-8');
      $current = date("Y-m-d H:i");
      return $current;
  }

  public function pushOrderQuestContr($id)
  {
   $push = app()->make('OrderPusher')->push($id);
  }

}
 ?>
