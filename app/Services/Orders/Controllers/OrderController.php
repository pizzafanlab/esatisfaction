<?php

namespace App\Services\Orders\Controllers;

use App\Services\Orders\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function store(Request $request)
    {
        return $push = app()->make('OrderStore')->store($request);

    }

    public function index()
    {
        // return Order::all();
    }

    public function show(Request $request)
    {
        $order = Order::where('order_id','=',intval($request->input('order_id')))->get();
        return $order;

    }
}
?>
