<?php

namespace App\Services\Orders\Controllers;

use App\Services\Questionnaires\Models\Questionnaire;
use App\Services\Orders\Models\Order;
use App\Services\Orders\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public function Unsubscribe(Request $request)
    {
      $result = Questionnaire::where('data.quest_id','=',intval($request->input('id')))->first();
      $hash = $request->input('hash');
      $quest = json_decode(json_encode($result));

      if($hash == $quest->data->hash){
        $customer_id = $quest->data->customer_id;
        $customer = new Customer;
        if ($customer->CustomerExists($customer_id)) {
          $customer->Unsubscribed($customer_id);
          return 'Unsubscribed';
        }
        else {
          return 'No client in Database';
        }
     }
    }

}
?>
