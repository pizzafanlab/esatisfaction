<?php
namespace App\Services\Orders\Models;
use Moloquent\Eloquent\Model as Eloquent;
use DateTime;


class Customer extends Eloquent {

protected $collection = 'Customers';
protected $connection = 'mongodb';
 protected $dates = ['stamp'];

  public function CustomerExists($id)
  {
    $status = Customer::where('customer_id','=',$id)->first();
    if($status){
      return true;
    }
    else {
      return false;
    }
  }

  public function NewCustomer($id)
  {
    $this->customer_id = $id;
    $this->orders = array();
    $this->notify = true;
    $this->unsubscribe = false;
    $this->save();
    return true;
  }

  public function DisableNotify($id){
    $customer = Customer::where('customer_id','=',$id)->first();
    $customer->notify = false;
    $customer->save();
    return true;
  }

  public function Unsubscribed($id){
    $customer = Customer::where('customer_id','=',$id)->first();
    $customer->notify = false;
    $customer->unsubscribe = true;
    $customer->save();
    return true;
  }

  public function EnableNotify($id){
    $customer = Customer::where('customer_id','=',$id)->first();
    $customer->notify = true;
    $customer->save();
    return true;
  }

  public function Subscribe($id){
    $customer = Customer::where('customer_id','=',$id)->first();
    $customer->notify = true;
    $customer->unsubscribe = false;
    $customer->save();
    return true;
  }

  public function InsertOrder($id,$stamp)
  {
    $customer = Customer::where('customer_id','=',$id)->first();
    $orders = $customer->orders;
    array_push($orders,$stamp);
    $customer->orders = $orders;
    $customer->save();
    return true;
  }

  public function Frequency_check($id,$stamp)
  {
    $customer = Customer::where('customer_id','=',$id)->first();
    $orders = $customer->orders;
    if (count($orders)<3) {
      if ($this->InsertOrder($id,$stamp))return true;
    }
    else {
      $newestDate = strtotime($orders[0]);
      $oldestDate = strtotime($orders[0]);
      foreach($orders as $order){
          if(strtotime($order) > $newestDate){
              $newestDate = strtotime($order);
          }
          if(strtotime($order) < $oldestDate){
              $oldestDate = strtotime($order);
          }
      }
      $newestDate = date("Y-m-d",$newestDate);
      $oldestDate = date("Y-m-d",$oldestDate);
      if ($this->dateChecker($newestDate) > 30 ) {
        $customer->orders=$this->Orders_Trimmer($orders,$stamp);
        $customer->save();
        return true;
      }
      elseif ($this->dateChecker($oldestDate) > 30 ) {
       $customer->orders=$this->Orders_Trimmer($orders,$stamp);
       $customer->save();
       return true;
      }
      else {
        return false;
      }
    }
  }

  public function dateChecker($date)
  {
    date_default_timezone_set('Europe/Athens');
     setlocale(LC_TIME, 'el_GR.UTF-8');
      $current = date("Y-m-d");
      $curr_date = new DateTime($current);
      $ord_date = new DateTime($date);
      $interval = $ord_date->diff($curr_date);
      return $interval->days;
    }

  public function Orders_Trimmer($orders,$stamp)
  {
      $oldestDate = strtotime($orders[0]);
      $deleted = 0;
      foreach($orders as $key => $value){
          if(strtotime($value) < $oldestDate){
              $oldestDate = strtotime($value);
              $deleted = $key;
          }
      }
      unset($orders[$deleted]);
      $orders[$deleted] = $stamp;
    return $orders;
  }

  public function MainCheck($id,$date)
  {
    $stamp = $date->format("Y-m-d H:i");
    if ($this->CustomerExists($id)) {
      $customer = Customer::where('customer_id','=',$id)->first();
      if ($customer->notify && !$customer->unsubscribe) {
        return $this->Frequency_check($id,$stamp);
      }
      else {
        return false;
      }
      //return $this->Frequency_check($id,$stamp);
    }
    else {
      if ($this->NewCustomer($id)) {
        return $this->Frequency_check($id,$stamp);
      }
      else {
        return false;
      }
    }
  }

  public function AppendOrderDate()
  {
    date_default_timezone_set('Europe/Athens');
     setlocale(LC_TIME, 'el_GR.UTF-8');
      $current = date("Y-m-d H:i");
      return $current;
  }

}
?>
