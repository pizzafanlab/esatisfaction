<?php
namespace App\Services\Statistics\Classes;
use App\Services\Questionnaires\Models\Questionnaire;
use DateTime;

class GetQuestData{

  public function generalData($date1,$date2,$Quests,$all)
  {
    $stats = new \stdClass();
    $general= new \stdClass();
    $delScoreSum=$takeScoreSum=$delcounter=$takecounter= 0;
    $delGenQuests = $takeGenQuests = $delIndQuests = $takeIndQuests = array();

    foreach ($Quests as $quest) {

        if ($quest->data['order_type'] == 'delivery') {
          $delScoreSum+=$quest->data['questions']['generalScore'];
          $delGenQuests = $this->QuestionsData($quest->data['questions']['general'],$delGenQuests);
          $delIndQuests = $this->QuestionsData($quest->data['questions']['indicators'],$delIndQuests);
          $delcounter++;
        }
        else {
          $takeScoreSum+=$quest->data['questions']['generalScore'];
          $takeGenQuests = $this->QuestionsData($quest->data['questions']['general'],$takeGenQuests);
          $takeIndQuests = $this->QuestionsData($quest->data['questions']['indicators'],$takeIndQuests);
          $takecounter++;
        }
    }

    $stats->deliveryQuestStats = $delGenQuests;
    $stats->takeQuestStats = $takeGenQuests;
    $stats->deliveryIndStats = $delIndQuests;
    $stats->takeIndStats = $takeIndQuests;
    $general->all=$all;
    $general->completed =$Quests->count();
    $general->uncompleted = $general->all - $general->completed;

    if($general->all == 0){
      $general->completeRate = 0;
    }
    else {
      $general->completeRate = round($general->completed/$general->all,2)*100;
    }

    $general->completedDelivery = $delcounter;
    $general->completedTake = $takecounter;



    if($general->completedDelivery==0 || $general->completedTake==0 ){
      if ($general->completedDelivery!=0) {
        $general->delScore = round($delScoreSum/$general->completedDelivery,2);
        $general->takeScore = 0;
      }
      elseif ($general->completedTake!=0) {
        $general->delScore = 0;
        $general->takeScore = round($takeScoreSum/$general->completedTake,2);
      }
      else {
        $general->delScore = 0;
        $general->takeScore = 0;
      }
    }

    else {
      $general->delScore = round($delScoreSum/$general->completedDelivery,2);
      $general->takeScore = round($takeScoreSum/$general->completedTake,2);
    }

    // $stats->perDayScores = $this->perDayData($date1,$date2,$restId,$Quests);
    $stats->perDayScores = $this->perDayData($date1,$date2,$Quests);
    $stats->DelAvgIndicators= $this->AverageIndQuests($stats->deliveryIndStats,$general->completedDelivery);
    $stats->TakeAvgIndicators= $this->AverageIndQuests($stats->takeIndStats,$general->completedTake);
    $stats->general =$general;
    $date1 = new DateTime($date1);
    $date1 = $date1->format('d-m-y');
    $date2 = new DateTime($date2);
    $date2 = $date2->format('d-m-y');
    $dates  = array('date1' => $date1,'date2'=> $date2);
    $stats->dates= $dates;
    return $stats;
  }

  public function Aggregated($date1,$date2,$Quests,$all)
  {
    $stats = new \stdClass();
    $general= new \stdClass();
    $delScoreSum=$takeScoreSum=$delcounter=$takecounter= 0;


    foreach ($Quests as $quest) {

        if ($quest->data['order_type'] == 'delivery') {
          $delScoreSum+=$quest->data['questions']['generalScore'];
          $delcounter++;
          }
        else {
          $takeScoreSum+=$quest->data['questions']['generalScore'];
          $takecounter++;
        }

    }

    $general->all=$all;
    $general->completed =$Quests->count();
    $general->uncompleted = $general->all - $general->completed;

    if($general->all == 0){
      $general->completeRate = 0;
    }
    else {
      $general->completeRate = round($general->completed/$general->all,2)*100;
    }

    $general->completedDelivery = $delcounter;
    $general->completedTake = $takecounter;



    if($general->completedDelivery==0 || $general->completedTake==0 ){
      if ($general->completedDelivery!=0) {
        $general->delScore = round($delScoreSum/$general->completedDelivery,2);
        $general->takeScore = 0;
      }
      elseif ($general->completedTake!=0) {
        $general->delScore = 0;
        $general->takeScore = round($takeScoreSum/$general->completedTake,2);
      }
      else {
        $general->delScore = 0;
        $general->takeScore = 0;
      }
    }

    else {
      $general->delScore = round($delScoreSum/$general->completedDelivery,2);
      $general->takeScore = round($takeScoreSum/$general->completedTake,2);
    }

    $stats->general =$general;
    $date1 = new DateTime($date1);
    $date1 = $date1->format('d-m-y');
    $date2 = new DateTime($date2);
    $date2 = $date2->format('d-m-y');
    $dates  = array('date1' => $date1,'date2'=> $date2);
    $stats->dates= $dates;
    return $stats;
  }

  // public function perDayData($date1,$date2,$restId,$Questionnaires)
    public function perDayData($date1,$date2,$Questionnaires)
  {
    $datetime1 = strtotime($date1);
    $datetime2 = strtotime($date2);
    $datediff = $datetime2 - $datetime1;

    $days= round($datediff / (60 * 60 * 24));
    $step = $date1;
    $perDayData= array();


    for ($i=1; $i <=$days ; $i++) {

      $obj = new \stdClass();
      $delsum=$takesum=$delperDay=$takeperDay=0;
      $delsum1to6=$delsum7to8=$delsum9to10=0;
      $takesum1to6=$takesum7to8=$takesum9to10=0;

      $day = date('d', strtotime($step));
      $from = date('Y-m-d', strtotime($step)).' 00:00';
      $to = date('Y-m-d', strtotime($step)).' 23:59';
      $obj->day=$day;

        $Quests = $Questionnaires->where('stamp', '>=',new DateTime($from))->where('stamp', '<=',new DateTime($to));

      foreach ($Quests as $quest) {

            if ($quest->data['order_type'] == 'delivery') {
              $delsum+=$quest->data['questions']['generalScore'];
              foreach ($quest->data['questions']['general'] as $genQuest) {
                if ($genQuest['flag'] == 'promo') {
                  if ($genQuest['value']>= 1) {
                    if ($genQuest['value']<= 6) {
                      $delsum1to6++;
                    }
                    elseif ($genQuest['value']<= 8) {
                      $delsum7to8++;
                    }
                    else {
                      $delsum9to10++;
                    }
                  }
                }
              }
              $delperDay++;
            }
          else {
            $takesum+=$quest->data['questions']['generalScore'];
            foreach ($quest->data['questions']['general'] as $genQuest) {
              if ($genQuest['flag'] == 'promo') {
                if ($genQuest['value']>= 1) {

                    if ($genQuest['value']<= 6) {
                      $takesum1to6++;
                    }
                    elseif ($genQuest['value']<= 8) {
                      $takesum7to8++;
                    }
                    else {
                      $takesum9to10++;
                    }
                }
              }
            }
            $takeperDay++;
          }

      }
      // $delperDay = $Quests->where('data.order_type','=','delivery')->count();
      // $takeperDay = $Quests->where('data.order_type','=','take')->count();

      if ($delperDay>0) {
        $obj->delScore = round($delsum/$delperDay,2);
        $obj->delNps= (round($delsum9to10/$delperDay,2) - round($delsum1to6/$delperDay,2))*100;
      }
      else {
        $obj->delScore = 0;
        $obj->delNps= 0;
      }

      if ($takeperDay>0) {
      $obj->takeScore = round($takesum/$takeperDay,2);
      $obj->takeNps= (round($takesum9to10/$takeperDay,2) - round($takesum1to6/$takeperDay,2))*100;
      }
      else {
        $obj->takeScore = 0;
        $obj->takeNps= 0;
      }

      $step = date('Y-m-d H:i', strtotime($step . ' +1 day'));
      array_push($perDayData,$obj);
    }
    return $perDayData;
  }

  public function QuestionsData($questions,$array)
  {
    if (empty($array)) {

      $constructed = $this->constructArray($questions,$array);
      return $this->QuestionsData($questions,$constructed);

      }
    else {
      foreach ($questions as $quest) {
        $arrayQuests = array();
        foreach ($array as $item) {
          array_push($arrayQuests,$item->id);
        }
        if (in_array($quest['_id'], $arrayQuests)){
          foreach ($array as $item) {
            if ($quest['_id']==$item->id && $quest['value'] >=1 ) {
                $item->sum += $quest['value'];
                if ($quest['value']<=6) {
                  $item->value["1to6"]++;
                }
                elseif ($quest['value']<=8) {
                  $item->value['7to8']++;
                }
                else{
                  $item->value['9to10']++;
                }
                $counter = $item->value["1to6"]+$item->value['7to8']+$item->value['9to10'];
                $item->avg = round($item->sum/$counter,2);
             }
           }
        }
        else {
          $range = ['1to6'=>0,'7to8'=>0,'9to10'=>0];
          $item = new \stdClass();
          $item->id = $quest['_id'];
          $item->value = $range;
          $item->questlabel = $quest['questlabel'];
          if ($quest['value'] >=1 ){
            $item->sum =  $item->avg = $quest['value'];

            if ($quest['value']<=6) {
              $item->value["1to6"]=1;
            }
            elseif ($quest['value']<=8) {
              $item->value['7to8']=1;
            }
            else{
              $item->value['9to10']=1;
            }
            array_push($array,$item);
          }
        }
       }
       return $array;
    }
  }

  public function constructArray($questions,$data)
  {
       $range = ['1to6'=>0,'7to8'=>0,'9to10'=>0];
          foreach ($questions as $quest) {
            $item = new \stdClass();
            $item->id = $quest['_id'];
            $item->questlabel = $quest['questlabel'];
            $item->value = $range;
            $item->sum = $item->avg = 0;
            array_push($data,$item);
          }
          return $data;
  }

  public function AverageIndQuests($array,$counter)
  {
    $average = array();
    foreach ($array as $item) {
      $data = new \stdClass();
      $data->questlabel = $item->questlabel;
      if ($counter == 0) {
          $data->avg = 0;
      }
      else {
        $data->avg = round($item->sum/$counter,2);
      }

      array_push($average,$data);
    }
    return $average;
  }

  public function detailsData($min,$max,$Quests)
  {
    $details = new \stdClass();
    $order_data = array();
    foreach ($Quests as $quest) {

        $score = $quest->data['questions']['generalScore'];
        if ($score >= $min && $score <= $max) {
          $data = new \stdClass();
          $data->id = $quest->data['order_id'];
          $date = $quest->stamp->setTimezone('Europe/Athens');
          $data->created = $date->format("Y-m-d H:i");
          $data->submitted = $quest->data['stampCompleted'];
          $data->satisf = $quest->data['questions']['generalScore'];
          $data->type = $quest->data['order_type'];
          array_push($order_data,$data);
        }

    }
    $details->orders = $order_data;
    return $details;
  }
  public function commentsData($min,$max,$Quests)
  {
    $comments = new \stdClass();
    $comments_data = array();
    foreach ($Quests as $quest) {
      $score = $quest->data['questions']['generalScore'];
      if ($score >= $min && $score <= $max) {
        $data = new \stdClass();
        $data->id = $quest->data['order_id'];
        // $date = new DateTime($quest->stamp);
        $data->email = decrypt($quest->data['customer_email']);
        $date = $quest->stamp->setTimezone('Europe/Athens');
        $data->created = $date->format("Y-m-d H:i");
        $data->satisf = $quest->data['questions']['generalScore'];
        $data->comment = $quest->data['questions']['comment']['comment'];
        $data->type = $quest->data['order_type'];
        $data->rest = $quest->data['intro']['restaurant'];
        $method = new \stdClass();
        if ($data->type == 'take') {
          $method->quest = $quest->data['questions']['method']['questlabel'];
          $method->value = $quest->data['questions']['method']['comment'];
        }
        elseif ($data->type = 'delivery') {
          $method->quest = $quest->data['questions']['method']['questlabel'];
          $method->value = $quest->data['questions']['method']['value'];
          $method->quest2 = $quest->data['questions']['method']['commlabel'];
          $method->value2 = $quest->data['questions']['method']['comment'];
        }
        $data->method = $method;
        $products = array();

        foreach ($quest->data['questions']['products'] as $item) {
          $product = new \stdClass();
          $product->label = $item['prod_name'];
          $product->value = $item['value'];
          $product->comment = $item['comment'];
          array_push($products,$product);
        }

        $data->products = $products;
        array_push($comments_data,$data);
      }
    }
    $comments = $comments_data;
    return $comments;
  }

  public function commentsDataUser($min,$max,$Quests)
  {
    $comments = new \stdClass();
    $comments_data = array();
    foreach ($Quests as $quest) {
      $score = $quest->data['questions']['generalScore'];
      if ($score >= $min && $score <= $max) {
        $data = new \stdClass();
        $data->id = $quest->data['order_id'];
        // $date = new DateTime($quest->stamp);
        $date = $quest->stamp->setTimezone('Europe/Athens');
        $data->created = $date->format("Y-m-d H:i");
        $data->satisf = $quest->data['questions']['generalScore'];
        $data->comment = $quest->data['questions']['comment']['comment'];
        $data->type = $quest->data['order_type'];
        $data->rest = $quest->data['intro']['restaurant'];
        $method = new \stdClass();
        if ($data->type == 'take') {
          $method->quest = $quest->data['questions']['method']['questlabel'];
          $method->value = $quest->data['questions']['method']['comment'];
        }
        elseif ($data->type = 'delivery') {
          $method->quest = $quest->data['questions']['method']['questlabel'];
          $method->value = $quest->data['questions']['method']['value'];
          $method->quest2 = $quest->data['questions']['method']['commlabel'];
          $method->value2 = $quest->data['questions']['method']['comment'];
        }
        $data->method = $method;
        $products = array();

        foreach ($quest->data['questions']['products'] as $item) {
          $product = new \stdClass();
          $product->label = $item['prod_name'];
          $product->value = $item['value'];
          $product->comment = $item['comment'];
          array_push($products,$product);
        }

        $data->products = $products;
        array_push($comments_data,$data);
      }
    }
    $comments = $comments_data;
    return $comments;
  }
}
 ?>
