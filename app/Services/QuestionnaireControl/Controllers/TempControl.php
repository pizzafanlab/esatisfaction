<?php
namespace App\Services\QuestionnaireControl\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
// use App\Services\QuestionnaireControl\Models\Template;
use App\Services\QuestionnaireControl\Models\Question;
use App\Services\Questionnaires\Models\Questionnaire;

class TempControl extends Controller
{
      // public function index()
      // {
      //   $prodfile = file_get_contents('../app/Services/QuestionnaireControl/JsonData/template1.json');
      //   return $prodfile;
      // }

    public function Retrieve(Request $request)
    {
      $response = new \stdClass;
      $delquest = Question::where('method','=','delivery')->get();
      $takequest =Question::where('method','=','take')->get();
      $del_data = $take_data = array();


        if (empty($delquest) || empty($takequest)) {
          if (empty($delquest)) {
            $response->del_data = array();
          }
          elseif (empty($takequest)) {
            $response->take_data = array();
          }
          else{
            $response->del_data = array();
            $response->take_data = array();
          }
        }
        else {
          foreach ($delquest as $quest) {
            $obj = new \stdClass;
            $obj->id = $quest->_id;
            $obj->category = $quest->category;
            $obj->questlabel = $quest->questlabel;

            if ($quest->flag == 'satisf') {
              $obj->flag = 'Ικανοποίηση';
            }
            elseif($quest->flag == 'promo'){
              $obj->flag = 'Promotion';
            }
            else {
              $obj->flag = '';
            }

            if ($quest->status) {
              $obj->status = 'Ενεργή';
            }
            else {
              $obj->status = 'Μη Ενεργή';
            }

            array_push($del_data,$obj);
          }
          foreach ($takequest as $quest) {
            $obj = new \stdClass;
            $obj->id = $quest->_id;
            $obj->category = $quest->category;
            $obj->questlabel = $quest->questlabel;

            if ($quest->flag == 'satisf') {
              $obj->flag = 'Ικανοποίηση';
            }
            elseif($quest->flag == 'promo'){
              $obj->flag = 'Promotion';
            }
            else {
              $obj->flag = '';
            }

            if ($quest->status) {
              $obj->status = 'Ενεργή';
            }
            else {
              $obj->status = 'Μη Ενεργή';
            }

            array_push($take_data,$obj);
          }
          $response->del_data = $del_data;
          $response->take_data = $take_data;
        }

      return response()->json($response, 200);
    }

    public function AddQuestion(Request $request)
    {
      $data = array();
      $data['questlabel'] = $request->input('text');
      $data['inputType'] = $request->input('Input');
      $data['value'] = $request->input('value');
      $data['category'] = $request->input('category');
      $data['method'] = $request->input('method');
      if ($data['category']=='general') {
        $data['flag'] = $request->input('flag');
      }
      $question = new Question;
      $errors = $question->Add($data);
      if (empty($errors)) {
        array_push($errors,'Η ερώτηση προστέθηκε');
      }
      return $errors;
    }

    public function DeleteQuestion(Request $request)
    {
      $data = array();
      $data['questID'] = $request->input('questID');
      $question = new Question;
      $errors = $question->DeleteQuest($data);
      if (empty($errors)) {
        array_push($errors,'Η ερώτηση απενεργοποιήθηκε');
      }
      return $errors;
    }

    public function EnableQuestion(Request $request)
    {
      $data = array();
      $data['questID'] = $request->input('questID');
      $question = new Question;
      $errors = $question->EnableQuest($data);
      if (empty($errors)) {
        array_push($errors,'Η ερώτηση ενεργοποιήθηκε');
      }
      return $errors;
    }

    public function EditQuestion(Request $request)
    {
      $data = array();
      $data['questID'] = $request->input('questID');
      $data['questText'] = $request->input('questText');
      $question = new Question;
      $errors = $question->EditQuest($data);
      if (empty($errors)) {
        array_push($errors,'Η ερώτηση άλλαξε');
      }
      return $errors;
    }

    public function Migrator()
    {
      $quests = Questionnaire::where('data.isComplete','=',true)
                              ->where('data.DBquests','=',false)
                              ->limit(30)->get();
      if (!empty($quests)) {
        foreach ($quests as $quest) {

          // if (!isset($quest->data['DBquests']) || !$quest->data['DBquests']) {
            $obj = json_decode(json_encode($quest));
            foreach( $obj->data->questions->general as $general ) {

              $temp = Question::where('category','=','general')
                      ->where('method','=',$obj->data->order_type)
                      ->where('questlabel','=',$general->questlabel)->first();

              $general->_id = $temp->_id;
              $general->inputType = $temp->inputType;
              $general->category = $temp->category;
              $general->flag = $temp->flag;
              $general->method = $temp->method;
              $general->status = $temp->status;

            }


            foreach( $obj->data->questions->indicators as $indicator ) {

              $temp = Question::where('category','=','indicators')
                      ->where('method','=',$obj->data->order_type)
                      ->where('questlabel','=',$indicator->questlabel)->first();

              $indicator->_id = $temp->_id;
              $indicator->inputType = $temp->inputType;
              $indicator->category = $temp->category;
              // $general->flag = $temp->flag;
              $indicator->method = $temp->method;
              $indicator->status = $temp->status;
            }
            $obj->data->DBquests = true;
            $quest->data = $obj->data;
            $quest->save();
          // }


        }
      }
      else{
        return 'Nothing to migrate';
      }

    }
}

 ?>
