<?php
namespace App\Services\QuestionnaireControl\Models;
use Moloquent\Eloquent\Model as Eloquent;

class Question extends Eloquent {

  protected $collection = 'Questions';
  protected $connection = 'mongodb';

  public function Add($data)
  {
    $errors=array();
    if ($data['category'] == 'general') {
      if ($data['method'] == 'delivery') {
        if ($data['flag']=='satisf' || $data['flag']=='promo') {
          if ($data['flag']=='satisf') {
            if ($this->where('flag','=','satisf')
                          ->where('method','=','delivery')
                          ->where('status','=',true)->count()>0) {
              array_push($errors,'Υπάρχει ήδη ερώτηση Γενικής Ικανοποίησης');
            }
            else {
              $this->saveQuest($data);
            }
          }
          else {
            if ($this->where('flag','=','promo')
                          ->where('method','=','delivery')
                          ->where('status','=',true)->count()>0) {
              array_push($errors,'Υπάρχει ήδη ερώτηση Promotion');
            }
            else {
              $this->saveQuest($data);
            }
          }
        }
        else {
          $this->saveQuest($data);
        }
      }
      else {
        if ($data['flag']=='satisf' || $data['flag']=='promo') {
          if ($data['flag']=='satisf') {
            if ($this->where('flag','=','satisf')
                          ->where('method','=','take')
                          ->where('status','=',true)->count()>0) {
              array_push($errors,'Υπάρχει ήδη ερώτηση Γενικής Ικανοποίησης');
            }
            else {
              $this->saveQuest($data);
            }
          }
          else {
            if ($this->where('flag','=','promo')
                          ->where('method','=','take')
                          ->where('status','=',true)->count()>0) {
              array_push($errors,'Υπάρχει ήδη ερώτηση Promotion');
            }
            else {
              $this->saveQuest($data);
            }
          }
        }
        else {
          $this->saveQuest($data);
        }
      }
    }
    elseif ($data['category'] == 'indicators') {
        $this->saveQuest($data);
    }

    return $errors;
  }

  public function saveQuest($data){
    $this->questlabel = $data['questlabel'];
    $this->inputType = $data['inputType'];
    $this->value = $data['value'];
    $this->category = $data['category'];
    if ($data['category']=='general') {
          $this->flag = $data['flag'];
    }
    $this->method = $data['method'];
    $this->status = true;
    $this->save();
  }

  public function DeleteQuest($data){
    $errors=array();
    $question = $this->where('_id','=',$data['questID'])->first();
    if ($question->category == 'general') {
        if ($question->flag == 'satisf' || $question->flag == 'promo') {
          array_push($errors,'Δεν μπορεί να διαγραφεί ερώτηση Ικανοποίησης ή Promotion');
        }
        else {
          $question->status = false;
          $question->save();
        }
    }
    elseif ($question->category == 'indicators') {
      if ($question->method == 'delivery') {
        if ($this->where('category','=','indicators')
        ->where('method','=','delivery')->count()>1) {
          $question->status = false;
          $question->save();
        }
        else {
          array_push($errors,'Οι δείκτες Delivery πρέπει να περιλαμβάνουν τουλάχιστον 1 ερώτηση');
        }
      }
      else {
        if ($this->where('category','=','indicators')
        ->where('method','=','take')->count()>1) {
          $question->status = false;
          $question->save();
        }
        else {
          array_push($errors,'Οι δείκτες Take πρέπει να περιλαμβάνουν τουλάχιστον 1 ερώτηση');
        }
      }
    }
    return $errors;
  }

  public function EnableQuest($data)
  {
    $errors=array();
    $question = $this->where('_id','=',$data['questID'])->first();
    if (!$question->status) {
      if ($question->category == 'general') {
        if ($question->flag == 'satisf' || $question->flag == 'promo') {
          if ($question->flag == 'satisf') {
            $check = $this->where('category','=','general')
                          ->where('flag','=','satisf')
                          ->where('method','=',$question->method)->count();
            if ($check > 0) {
              array_push($errors,'Υπάρχει ήδη ερώτηση Ικανοποίησης');
            }
            else {
              $question->status = true;
              $question->save();
            }
          }
          else {
            $check = $this->where('category','=','general')
                          ->where('flag','=','promo')
                          ->where('method','=',$question->method)->count();
            if ($check > 0) {
              array_push($errors,'Υπάρχει ήδη ερώτηση Promotion');
            }
            else {
              $question->status = true;
              $question->save();
            }
          }

        }
        else {
          $question->status = true;
          $question->save();
        }
      }
      else {
        $question->status = true;
        $question->save();
      }
    }else {
      array_push($errors,'Η ερώτηση είναι ήδη ενεργοποιημένη');
    }


    return $errors;
  }

  public function EditQuest($data)
  {
    $errors=array();
    $question = $this->where('_id','=',$data['questID'])->first();
    if (!$data['questText'] == null) {
      $question->questlabel = $data['questText'];
      $question->save();
    }else {
      array_push($errors,'Το όνομα της ερώτησης δε μπορεί να είναι κενό');
    }
    return $errors;
  }

}
?>
