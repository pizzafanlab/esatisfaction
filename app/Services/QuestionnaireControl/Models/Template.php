<?php
namespace App\Services\QuestionnaireControl\Models;
use Moloquent\Eloquent\Model as Eloquent;

class Template extends Eloquent {

  protected $collection = 'templates';
  protected $connection = 'mongodb';

  protected $dates = ['stamp'];
}
?>
