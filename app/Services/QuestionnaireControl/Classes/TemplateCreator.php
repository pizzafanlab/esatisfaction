<?php
namespace App\Services\QuestionnaireControl\Classes;

use App\Services\QuestionnaireControl\Models\Template;
use App\Services\Orders\Models\Order;
use App\Services\QuestionnaireControl\Models\Question;

class TemplateCreator{

    public function create($id)
    {
      $order = new Order;
      $order = Order::where('order_id','=',$id)->first();

      $data = new \stdClass();
      $intro = new \stdClass();
      $data->order_id = $order->order_id;
      //$data->stamp = $order->stamp;
      $data->customer_id= $order->customer_id;
      $data->customer_email= $order->customer_email;
      $data->order_type = $order->order_type;
      $data->rest_id = $order->address['restaurant_id'];
      $data->products = $order->products;


      $intro->title = 'Το ερωτηματολόγιο αφορά τη παραγγελία σας απο το κατάστημα';
      $intro->restaurant = $order->address['restaurant_name'];
      $data->intro =$intro;

      $questions = new \stdClass();

      $questions->products = $this->ProductsQuestions($order->products);

      $questions->method = $this->methodQuestions($data->order_type);

      $questions->general = $this->GeneralQuestions($data->order_type);

      $questions->indicators = $this->indicatorsQuestions($data->order_type);

      $questions->comment = $this->comment();

      $questions->generalScore = 0;

      $data->questions = $questions;
      $data->DBquests = true;
      $data->isCancelled = false;

      $temp = new Template;
      $temp->data = $data;

      $this->QuestPusher($temp);
    }

  public function QuestPusher($temp)
  {
    $template = $temp->toJson();
    $push = app()->make('QuestStorePush')->getTemplate($template);
  }

  public function GeneralQuestions($order_type)
  {
    if ($order_type=='delivery'){
      $questions = Question::where('category','=','general')
                    ->where('method','=','delivery')
                    ->where('status','=',true)->get();
      foreach ($questions as $quest) {
        unset($quest->updated_at);
        unset($quest->created_at);
      }
      // $temp = file_get_contents('../app/Services/QuestionnaireControl/JsonData/tempDel1.json');
      // $tempObj = json_decode($temp);
      // return $tempObj->data->questions->general;
      return $questions;

    }
    else
    {
      $questions = Question::where('category','=','general')
                    ->where('method','=','take')
                    ->where('status','=',true)->get();
      foreach ($questions as $quest) {
        unset($quest->updated_at);
        unset($quest->created_at);
      }
      return $questions;
      // $temp = file_get_contents('../app/Services/QuestionnaireControl/JsonData/tempTake1.json');
      // $tempObj = json_decode($temp);
      // return $tempObj->data->questions->general;
    }

  }

  public function ProductsQuestions($order)
  {
    $products = array();
    foreach ($order as $prod) {
      $quest= new \stdClass();
      $quest->questlabel = 'Πώς θα αξιολογούσατε το προιόν:';
      $quest->prod_name = $prod['name'];
      $quest->value = 0;
      $quest->commlabel = 'Σχόλιο προϊόντος '.$prod['name'];
      $quest->comment = '';
      array_push($products,$quest);
     }
     return $products;
  }

  public function methodQuestions($order_type)
  {
    if ($order_type=='delivery'){
      $temp = file_get_contents('../app/Services/QuestionnaireControl/JsonData/tempDel1.json');
      $tempObj = json_decode($temp);
      return $tempObj->data->questions->method;

    }else
    {
      $temp = file_get_contents('../app/Services/QuestionnaireControl/JsonData/tempTake1.json');
      $tempObj = json_decode($temp);
      return $tempObj->data->questions->method;
    }
  }

  public function indicatorsQuestions($order_type)
  {
    if ($order_type=='delivery'){

      $questions = Question::where('category','=','indicators')
                    ->where('method','=','delivery')
                    ->where('status','=',true)->get();
      foreach ($questions as $quest) {
        unset($quest->updated_at);
        unset($quest->created_at);
      }
      return $questions;
      // $temp = file_get_contents('../app/Services/QuestionnaireControl/JsonData/tempDel1.json');
      // $tempObj = json_decode($temp);
      // return $tempObj->data->questions->indicators;
    }else
    {
      $questions = Question::where('category','=','indicators')
                    ->where('method','=','take')
                    ->where('status','=',true)->get();
      foreach ($questions as $quest) {
        unset($quest->updated_at);
        unset($quest->created_at);
      }
      return $questions;
      // $temp = file_get_contents('../app/Services/QuestionnaireControl/JsonData/tempTake1.json');
      // $tempObj = json_decode($temp);
      // return $tempObj->data->questions->indicators;
    }

  }


  public function comment()
  {
      $quest = new \stdClass();
      $quest->commentlabel = 'Αν θέλετε, αφήστε μας το σχόλιο σας, όπως εσείς θέλετε';
      $quest->comment = '';
      return $quest;
  }

}
 ?>
