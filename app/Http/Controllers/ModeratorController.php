<?php

namespace App\Http\Controllers;
use App\Services\Questionnaires\Models\Questionnaire;
use Illuminate\Http\Request;
use Auth;
use App\User;

class ModeratorController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth:admin');
        $this->middleware('auth');
    }

    public function stats()
    {
      $api_token = auth()->user()->api_token;
      $confirmed= auth()->user()->confirmed;
      $active = auth()->user()->active;
      $mod = auth()->user()->Moderator;
      if ($active && $confirmed) {
        if ($mod) {
          return view('moderator.stats',['api_token'=>$api_token]);
        }
        else {
          return view('moderator.Forbidden');
        }
      }
      else {
        return view('moderator.Inactive');
      }

    }

    public function details()
    {
      $api_token = auth()->user()->api_token;
      $confirmed= auth()->user()->confirmed;
      $active = auth()->user()->active;
      $mod = auth()->user()->Moderator;
      if ($active && $confirmed) {
        if ($mod) {
          return view('moderator.details',['api_token'=>$api_token]);
        }
        else {
          return view('moderator.Forbidden');
        }
      }
      else {
        return view('moderator.Inactive');
      }
    }


    public function comments()
    {
      $api_token = auth()->user()->api_token;
      $confirmed= auth()->user()->confirmed;
      $active = auth()->user()->active;
      $mod = auth()->user()->Moderator;
      if ($active && $confirmed) {
        if ($mod) {
          return view('moderator.comments',['api_token'=>$api_token]);
        }
        else {
          return view('moderator.Forbidden');
        }
      }
      else {
        return view('moderator.Inactive');
      }

     }


    public function date()
    {
      date_default_timezone_set('Europe/Athens');
       setlocale(LC_TIME, 'el_GR.UTF-8');
       return date("Y-m-d");
    }
}
