<?php

namespace App\Http\Controllers;
use App\Services\Questionnaires\Models\Questionnaire;
use Illuminate\Http\Request;
use App\User;
use Auth;
use DateTime;

class AdminApiStatsController extends Controller
{

    public function aggregated(Request $request)
    {
        $start = microtime(true);

        $response = new \stdClass;
        $response->data = array();

        $user = User::where('api_token','=',$request->input('api_token'))->first();

        if ($user->isAdmin || $user->Moderator) {
          if ($request->input('compare')=='false') {
            $date1 = $request->input('startDate').' 00:01';
            $date2 = $request->input('endDate').' 23:59';

            if ($request->input('restaurant')=='all' && $request->input('category')=='all') {
              // $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
              $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('data.isComplete', '=',true)
                                                                        ->where('data.DBquests','=',true)
                                                                        ->get();

              $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->count();

            }
            elseif($request->input('category')=='all' && is_numeric($request->input('restaurant'))) {
              $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('data.isComplete', '=',true)
                                                                        ->where('data.DBquests','=',true)
                                                                        ->where('rest_id','=',intval($request->input('restaurant')))
                                                                        ->get();

              $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('rest_id','=',intval($request->input('restaurant')))
                                                                        ->count();
            }
            elseif($request->input('restaurant')=='all' && is_numeric($request->input('category'))) {
              $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('data.isComplete', '=',true)
                                                                        ->where('data.DBquests','=',true)
                                                                        ->where('data.products', 'elemMatch', array('cat_id' => intval($request->input('category'))))
                                                                        ->get();

              $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->count();
            }
            elseif(is_numeric($request->input('restaurant')) && is_numeric($request->input('category'))) {
              $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('data.isComplete', '=',true)
                                                                        ->where('data.DBquests','=',true)
                                                                        ->where('rest_id','=',intval($request->input('restaurant')))
                                                                        ->where('data.products', 'elemMatch', array('cat_id' => intval($request->input('category'))))
                                                                        ->get();

              $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('rest_id','=',intval($request->input('restaurant')))
                                                                        ->count();
            }
            else {
              $Quests = array();
              $all = 0;
            }

            $stats = app()->make('GetQuestData')->generalData($date1,$date2,$Quests,$all);
            $flagCompare = false;
            $response->data['stats']=$stats;
          }
          else {

            $date1 = $request->input('startDate').' 00:01';
            $date2 = $request->input('endDate').' 23:59';
            $date3 = $request->input('CompstartDate').' 00:01';
            $date4 = $request->input('CompendDate').' 23:59';

            if ($request->input('restaurant')=='all' && $request->input('category')=='all') {
              // $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
              $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('data.isComplete', '=',true)
                                                                        ->where('data.DBquests','=',true)
                                                                        ->get();

              $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->count();

               // $Quests = Questionnaire::where('data.DBquests','=',true)->get();
            }
            elseif($request->input('category')=='all' && is_numeric($request->input('restaurant'))) {
              $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('data.isComplete', '=',true)
                                                                        ->where('data.DBquests','=',true)
                                                                        ->where('rest_id','=',intval($request->input('restaurant')))
                                                                        ->get();

              $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('rest_id','=',intval($request->input('restaurant')))
                                                                        ->count();
            }
            elseif($request->input('restaurant')=='all' && is_numeric($request->input('category'))) {
              $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('data.isComplete', '=',true)
                                                                        ->where('data.DBquests','=',true)
                                                                        ->where('data.products', 'elemMatch', array('cat_id' => intval($request->input('category'))))
                                                                        ->get();

              $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->count();
            }
            elseif(is_numeric($request->input('restaurant')) && is_numeric($request->input('category'))) {
              $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('data.isComplete', '=',true)
                                                                        ->where('data.DBquests','=',true)
                                                                        ->where('rest_id','=',intval($request->input('restaurant')))
                                                                        ->where('data.products', 'elemMatch', array('cat_id' => intval($request->input('category'))))
                                                                        ->get();

              $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('rest_id','=',intval($request->input('restaurant')))
                                                                        ->count();
            }
            else {
              $Quests = $all = array();
            }


            if ($request->input('CompRestaurant')=='all' && $request->input('compcategory')=='all') {
              $comQuests = Questionnaire::where('stamp', '>=', new DateTime($date3))->where('stamp', '<=',new DateTime($date4))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('data.isComplete', '=',true)
                                                                        ->where('data.DBquests','=',true)
                                                                        // ->where('rest_id','=',intval($request->input('restaurant')))
                                                                        ->get();

              $compall =  Questionnaire::where('stamp', '>=', new DateTime($date3))->where('stamp', '<=',new DateTime($date4))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->count();
            }
            elseif($request->input('compcategory')=='all' && is_numeric($request->input('CompRestaurant'))) {
              $comQuests = Questionnaire::where('stamp', '>=', new DateTime($date3))->where('stamp', '<=',new DateTime($date4))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('data.isComplete', '=',true)
                                                                        ->where('data.DBquests','=',true)
                                                                        ->where('rest_id','=',intval($request->input('CompRestaurant')))
                                                                        ->get();

              $compall  = Questionnaire::where('stamp', '>=', new DateTime($date3))->where('stamp', '<=',new DateTime($date4))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('rest_id','=',intval($request->input('CompRestaurant')))
                                                                        ->count();
            }
            elseif($request->input('CompRestaurant')=='all' && is_numeric($request->input('compcategory'))) {
              $comQuests = Questionnaire::where('stamp', '>=', new DateTime($date3))->where('stamp', '<=',new DateTime($date4))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('data.DBquests','=',true)
                                                                        ->where('data.isComplete', '=',true)
                                                                        ->where('data.products', 'elemMatch', array('cat_id' => intval($request->input('compcategory'))))
                                                                        ->get();

              $compall = Questionnaire::where('stamp', '>=', new DateTime($date3))->where('stamp', '<=',new DateTime($date4))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->count();
            }
            elseif(is_numeric($request->input('CompRestaurant')) && is_numeric($request->input('compcategory'))) {
              $comQuests = Questionnaire::where('stamp', '>=', new DateTime($date3))->where('stamp', '<=',new DateTime($date4))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('data.isComplete', '=',true)
                                                                        ->where('data.DBquests','=',true)
                                                                        ->where('rest_id','=',intval($request->input('CompRestaurant')))
                                                                        ->where('data.products', 'elemMatch', array('cat_id' => intval($request->input('compcategory'))))
                                                                        ->get();

              $compall = Questionnaire::where('stamp', '>=', new DateTime($date3))->where('stamp', '<=',new DateTime($date4))
                                                                        ->where('data.isCancelled', '=',false)
                                                                        ->where('rest_id','=',intval($request->input('CompRestaurant')))
                                                                        ->count();
            }
            else {
              $comQuests = array();
              $compall= 0;
            }
            // if ($request->input('restaurant')=='all') {
            //   $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
            //                                                             ->where('data.isCancelled', '=',false)
            //                                                             // ->where('rest_id','=',intval($request->input('restaurant')))
            //                                                             ->get();
            // }
            // else {
            //   $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
            //                                                             ->where('data.isCancelled', '=',false)
            //                                                             ->where('rest_id','=',intval($request->input('restaurant')))
            //                                                             ->get();
            // }
            // if ($request->input('CompRestaurant')=='all') {
            //   $comQuests = Questionnaire::where('stamp', '>=', new DateTime($date3))->where('stamp', '<=',new DateTime($date4))
            //                                                             ->where('data.isCancelled', '=',false)
            //                                                             // ->where('rest_id','=',intval($request->input('restaurant')))
            //                                                             ->get();
            // }
            // else {
            //   $comQuests = Questionnaire::where('stamp', '>=', new DateTime($date3))->where('stamp', '<=',new DateTime($date4))
            //                                                             ->where('data.isCancelled', '=',false)
            //                                                             ->where('rest_id','=',intval($request->input('CompRestaurant')))
            //                                                             ->get();
            // }

            $stats = app()->make('GetQuestData')->generalData($date1,$date2,$Quests,$all);
            $compStats = app()->make('GetQuestData')->generalData($date3,$date4,$comQuests,$compall);
            $flagCompare = true;
            $response->data['stats']=$stats;
            $response->data['compStats']=$compStats;
          }
        }
        else {
          $response->message = 'Δεν έχετε πρόσβαση';
        }

        $end = microtime(true) - $start;
        $response->time = $end;

        return response()->json($response, 200);
    }

    public function detailedData(Request $request)
    {
      $start = microtime(true);
      $response = new \stdClass;
      $response->data = array();

      $user = User::where('api_token','=',$request->input('api_token'))->first();


      if ($user->isAdmin || $user->Moderator) {
        $date1 = $request->input('startDate').' 00:01';
        $date2 = $request->input('endDate').' 23:59';
        $minSatisf = $request->input('minSatisf');
        $maxSatisf = $request->input('maxSatisf');

        if ($request->input('restaurant')=='all' && $request->input('category')=='all') {
          $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->where('data.isComplete', '=',true)
                                                                    ->where('data.DBquests','=',true)
                                                                    // ->where('rest_id','=',intval($request->input('restaurant')))
                                                                    ->get();

          $all =  Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->count();
        }
        elseif($request->input('category')=='all' && is_numeric($request->input('restaurant'))) {
          $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->where('data.isComplete', '=',true)
                                                                    ->where('data.DBquests','=',true)
                                                                    ->where('rest_id','=',intval($request->input('restaurant')))
                                                                    ->get();

          $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->where('rest_id','=',intval($request->input('restaurant')))
                                                                    ->count();
        }
        elseif($request->input('restaurant')=='all' && is_numeric($request->input('category'))) {
          $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->where('data.isComplete', '=',true)
                                                                    ->where('data.DBquests','=',true)
                                                                    ->where('data.products', 'elemMatch', array('cat_id' => intval($request->input('category'))))
                                                                    ->get();

          $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->count();
        }
        elseif(is_numeric($request->input('restaurant')) && is_numeric($request->input('category'))) {
          $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->where('data.isComplete', '=',true)
                                                                    ->where('data.DBquests','=',true)
                                                                    ->where('rest_id','=',intval($request->input('restaurant')))
                                                                    ->where('data.products', 'elemMatch', array('cat_id' => intval($request->input('category'))))
                                                                    ->get();

          $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->where('rest_id','=',intval($request->input('restaurant')))
                                                                    ->count();
        }
        else {
          $Quests = array();
          $all = 0;
        }

        // if ($request->input('restaurant')=='all') {
        //   $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
        //                                                             ->where('data.isCancelled', '=',false)
        //                                                             // ->where('rest_id','=',intval($request->input('restaurant')))
        //                                                             ->get();
        // }
        // else {
        //   $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
        //                                                             ->where('data.isCancelled', '=',false)
        //                                                             ->where('rest_id','=',intval($request->input('restaurant')))
        //                                                             ->get();
        // }


        $stats = app()->make('GetQuestData')->Aggregated($date1,$date2,$Quests,$all);
        $details = app()->make('GetQuestData')->detailsData($minSatisf,$maxSatisf,$Quests);

        $response->data['stats']=$stats->general;
        $response->data['details']=$details;

      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }

      $end = microtime(true) - $start;
      $response->time = $end;
      return response()->json($response, 200);
    }

    public function Comments(Request $request)
    {
      $start = microtime(true);
      $response = new \stdClass;
      $response->data = array();

      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin || $user->Moderator) {
        $date1 = $request->input('startDate').' 00:01';
        $date2 = $request->input('endDate').' 23:59';
        $minSatisf = $request->input('minSatisf');
        $maxSatisf = $request->input('maxSatisf');

        if ($request->input('restaurant')=='all' && $request->input('category')=='all') {
          $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->where('data.isComplete', '=',true)
                                                                    ->where('data.DBquests','=',true)
                                                                    ->get();
          $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->count();
        }
        elseif($request->input('category')=='all' && is_numeric($request->input('restaurant'))) {
          $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->where('data.isComplete', '=',true)
                                                                    ->where('data.DBquests','=',true)
                                                                    ->where('rest_id','=',intval($request->input('restaurant')))
                                                                    ->get();

            $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                      ->where('data.isCancelled', '=',false)
                                                                      ->where('rest_id','=',intval($request->input('restaurant')))
                                                                      ->count();
        }
        elseif($request->input('restaurant')=='all' && is_numeric($request->input('category'))) {
          $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->where('data.isComplete', '=',true)
                                                                    ->where('data.DBquests','=',true)
                                                                    ->where('data.products', 'elemMatch', array('cat_id' => intval($request->input('category'))))
                                                                    ->get();
          $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->count();
        }
        elseif(is_numeric($request->input('restaurant')) && is_numeric($request->input('category'))) {
          $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->where('data.isComplete', '=',true)
                                                                    ->where('data.DBquests','=',true)
                                                                    ->where('rest_id','=',intval($request->input('restaurant')))
                                                                    ->where('data.products', 'elemMatch', array('cat_id' => intval($request->input('category'))))
                                                                    ->get();

          $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->where('rest_id','=',intval($request->input('restaurant')))
                                                                    ->count();
        }
        else {
          $Quests = array();
          $all = 0;
        }

        // if ($request->input('restaurant')=='all') {
        //   $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
        //                                                             ->where('data.isCancelled', '=',false)
        //                                                             // ->where('rest_id','=',intval($request->input('restaurant')))
        //                                                             ->get();
        // }
        // else {
        //   $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
        //                                                             ->where('data.isCancelled', '=',false)
        //                                                             ->where('rest_id','=',intval($request->input('restaurant')))
        //                                                             ->get();
        // }

        $stats = app()->make('GetQuestData')->Aggregated($date1,$date2,$Quests,$all);
        $comments = app()->make('GetQuestData')->commentsData($minSatisf,$maxSatisf,$Quests);
        $response->data['stats']=$stats->general;
        $response->data['comments']=$comments;
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }

      $end = microtime(true) - $start;
      $response->time = $end;
      return response()->json($response, 200);
    }

    public function RestaurantsStats(Request $request)
    {
      $start = microtime(true);
      $response = new \stdClass;
      $response->data = array();

      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $date1 = $request->input('startDate').' 00:01';
        $date2 = $request->input('endDate').' 23:59';
        // $minSatisf = $request->input('minSatisf');
        // $maxSatisf = $request->input('maxSatisf');
        // $restaurants = app()->make('GetAdvancedData')->RestaurantPicker();
        $restaurants = $this->RestaurantPicker();
        $RestStats = array();
        $tempDel = file_get_contents('../app/Services/QuestionnaireControl/JsonData/tempDel1.json');
        $DelObj = json_decode($tempDel);
        $DelProps = array();
        foreach ($DelObj->data->questions->indicators as $indicator) {
          $DelProps[$indicator->questlabel]= $indicator->value;
        }
        $tempTake = file_get_contents('../app/Services/QuestionnaireControl/JsonData/tempTake1.json');
        $TakeObj = json_decode($tempTake);
        $TakeProps = array();
        foreach ($TakeObj->data->questions->indicators as $indicator) {
          $TakeProps[$indicator->questlabel]= $indicator->value;
        }

        foreach ($restaurants as $restObj) {
          $obj = new \stdClass;
          $obj->rest_name = $restObj->name;
          $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->where('data.isComplete', '=',true)
                                                                    ->where('data.DBquests','=',true)
                                                                    ->where('rest_id','=',$restObj->id)
                                                                    ->get();

          $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                    ->where('data.isCancelled', '=',false)
                                                                    ->where('rest_id','=',$restObj->id)
                                                                    ->count();
          $stats = app()->make('GetQuestData')->generalData($date1,$date2,$Quests,$all);
          $obj->Συμπληρωμένα = $stats->general->completed;
          $obj->Delivery_Ικανοποίηση = $stats->general->delScore;
          $obj->Take_Ικανοποίηση = $stats->general->takeScore;
          if (empty($stats->DelAvgIndicators)) {
           foreach ($DelProps as $key => $value) {
             $property = $key .' Delivery';
             $obj->$property = 0;
           }
          }
          else {
            foreach ($stats->DelAvgIndicators as $quest) {

              $property = $quest->questlabel .' Delivery';

              $obj->$property = $quest->avg;
              // $questions[$quest->questlabel]=$quest->avg;
            }
          }

          if (empty($stats->TakeAvgIndicators)) {
            foreach ($TakeProps as $key => $value) {
              $property = $key .' Take';
              $obj->$property = 0;
            }
          }
          else {
            foreach ($stats->TakeAvgIndicators as $quest) {

              $property = $quest->questlabel .' Take';

              $obj->$property = $quest->avg;
              // $questions[$quest->questlabel]=$quest->avg;
            }
          }

          array_push($RestStats,$obj);
        }
        $response->data = $RestStats;
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }
      $end = microtime(true) - $start;
      $response->time = $end;
      return response()->json($response, 200);
    }

    public function RestaurantPicker()
    {
      $restrs = array();
      $file = file_get_contents('../app/Services/Orders/JsonData/restaurants.json');
      $rest_obj = json_decode($file);
      foreach ($rest_obj->data as $rest) {
        $obj = new \stdClass;
        $obj->id = $rest->restaurant_id;
        $obj->name = $rest->name;
        array_push($restrs,$obj);
      }
      usort($restrs, function($a, $b)
      {
          return strcmp($a->name, $b->name);
      });
      return $restrs;
    }

    public function CategoryPicker()
    {
      $exclfile= file_get_contents('../app/Services/Orders/JsonData/exlCategories.json');
      $excluded = json_decode($exclfile);
      $cats = $check = array();
      $file = file_get_contents('../app/Services/Orders/JsonData/products.json');
      $obj = json_decode($file);
      foreach ($obj->data as $cat) {
        if (!in_array($cat->type->type_id,$excluded->data->excl_cats)) {
          if (!in_array($cat->type->type_id,$check)) {
            $cat_obj = new \stdClass;
            $cat_obj->id = $cat->type->type_id;
            $cat_obj->name =  $cat->type->title->el;
            array_push($cats,$cat_obj);
            array_push($check,$cat_obj->id);
          }
        }
      }
      usort($cats, function($a, $b)
      {
          return strcmp($a->name, $b->name);
      });
      return $cats;
    }


    // public function Advanced(Request $request)
    // {
    //   $advanced = app()->make('GetAdvancedData')->generalData($request);
    //   $response = new \stdClass;
    //   $response->data = array();
    //   $response->data['advanced']=$advanced;
    //   return response()->json($response, 200);
    //  }
    //
    //  public function Canceled()
    //  {
    //    $date1 =$date2= $this->date();
    //    $response = new \stdClass;
    //    $response->data = array();
    //    $cancelData = app()->make('CancelQuests')->cancelledData($date1.' 00:00',$date2.' 23:59');
    //    // return view('admin.cancel',['cancelData'=>$cancelData,'date1' => $date1,'date2' => $date2]);
    //    $response->data['cancelled']=$cancelData;
    //    return response()->json($response, 200);
    //  }
    //
    //  public function searchCanceled(Request $request)
    //  {
    //    $response = new \stdClass;
    //    $response->data = array();
    //
    //    if ($request->input('flag')== 'cancel') {
    //      $date1 = $request->input('startDate');
    //      $date2 = $request->input('endDate');
    //      $questId= intval($request->input('cancelId'));
    //      $cancel = app()->make('CancelQuests')->cancelQuest($questId);
    //      $cancelData = app()->make('CancelQuests')->cancelledData($date1.' 00:00',$date2.' 23:59');
    //      // return view('admin.cancel',['cancelData'=>$cancelData,'date1' => $date1,'date2' => $date2]);
    //      $response->data['cancelled']=$cancelData;
    //    }
    //    elseif ($request->input('flag')== 'search') {
    //      $date1 = $request->input('startDate');
    //      $date2 = $request->input('endDate');
    //      $cancelData = app()->make('CancelQuests')->cancelledData($date1.' 00:00',$date2.' 23:59');
    //      // return view('admin.cancel',['cancelData'=>$cancelData,'date1' => $date1,'date2' => $date2]);
    //      $response->data['cancelled']=$cancelData;
    //    }
    //
    //    return response()->json($response, 200);
    //  }


     // public function AdminRestaurants(Request $request)
     // {
     //   $response = new \stdClass;
     //   $user = User::where('api_token','=',$request->input('api_token'))->first();
     //   $Restaurants = array();
     //   $Restaurants = $this->RestaurantPicker();
     //
     //     $response->restaurants = $Restaurants;
     //     return response()->json($response, 200);
     // }
     //
     // public function RestaurantPicker()
     // {
     //   $restrs = array();
     //   $file = file_get_contents('../app/Services/Orders/JsonData/restaurants.json');
     //   $obj = json_decode($file);
     //   foreach ($obj->data as $rest) {
     //     $restrs[$rest->restaurant_id] = $rest->name;
     //   }
     //   asort($restrs);
     //   return $restrs;
     // }

}
