<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use DateTime;

class ApiDataCreate extends Controller
{
  public function UpdateProducts()
  {
    $file_path = '../storage/ApiData/products.json';

    if(file_exists($file_path)){

      if ($this->dateChecker($file_path)) {

        return $this->JsonCreate($file_path);
      }
      else {
        return 'To products json einai kainourgio';
      }

    }
    else {
      return $this->JsonCreate($file_path);
    }

  }

  public function dateChecker($path)
  {
    date_default_timezone_set('Europe/Athens');
     setlocale(LC_TIME, 'el_GR.UTF-8');
      $current = date("Y-m-d H:i");
      $curr_date = new DateTime($current);

      $file = file_get_contents($path);
      $prodObj = json_decode($file);
      $json_date = new DateTime($prodObj->stamp);

      $interval = $json_date->diff($curr_date);
      $daysToH = $interval->d *24;
      $hoursToMin = ($interval->h + $daysToH)* 60;
      $mins = $interval->i;
      $totaldiff = $hoursToMin + $mins;

      // Minutes for file caching
      if($totaldiff>30){

        return true;
      }
      else{
        return false;
      }
  }

  public function JsonCreate($path)
  {
    date_default_timezone_set('Europe/Athens');
    setlocale(LC_TIME, 'el_GR.UTF-8');
    $current = date("Y-m-d H:i");

    $client = new Client();
    $res = $client->get('http://api.pizzafan.gr:9001/api/v1/products/');
    $prodObj = json_decode($res->getBody());
    $prodObj->stamp = $current;

    $fh = fopen($path, 'w')
      or die("Error opening output file");
      fwrite($fh,json_encode($prodObj,JSON_UNESCAPED_UNICODE));
      fclose($fh);
    return 'Json file created or updated';
  }
}
