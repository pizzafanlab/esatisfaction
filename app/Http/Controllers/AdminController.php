<?php

namespace App\Http\Controllers;
use App\Services\Questionnaires\Models\Questionnaire;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth:admin');
        $this->middleware('auth');
    }

    public function stats()
    {

      $api_token = auth()->user()->api_token;
      $active = auth()->user()->active;
      $confirmed = auth()->user()->confirmed;
      $admin = auth()->user()->isAdmin;
      if ($active && $confirmed) {
        if ($admin) {
          return view('admin.stats',['api_token'=>$api_token]);
        }
        else {
          return view('user.Forbidden');
        }

      }
      else {
        return view('user.Inactive');
      }
    }

    public function details()
    {
      $api_token = auth()->user()->api_token;
      $active = auth()->user()->active;
      $confirmed = auth()->user()->confirmed;
      $admin = auth()->user()->isAdmin;
      if ($active && $confirmed) {
        if ($admin) {
          return view('admin.details',['api_token'=>$api_token]);
        }
        else {
          return view('user.Forbidden');
        }

      }
      else {
        return view('user.Inactive');
      }
      // $date1 =$date2= $this->date();
      // $restId = 'all';
      // $minSatisf = 0;
      // $maxSatisf = 10;
      // $Quests = Questionnaire::where('data.stamp', '>=', $date1.' 00:00')->where('data.stamp', '<=',$date2.' 23:59')
      //                                                           ->where('data.isCancelled', '=',false)->get();
      // $stats = app()->make('GetQuestData')->generalData($date1.' 00:00',$date2.' 23:59',$restId,$Quests);
      // $details = app()->make('GetQuestData')->detailsData($minSatisf,$maxSatisf,$Quests);
      // return view('admin.details',['stats' => $stats,'details'=>$details,'date1' => $date1,'date2' => $date2,
      // 'min'=>$minSatisf,'max'=>$maxSatisf]);
    }

    public function comments()
    {
      $api_token = auth()->user()->api_token;
      $active = auth()->user()->active;
      $confirmed = auth()->user()->confirmed;
      $admin = auth()->user()->isAdmin;
      if ($active && $confirmed) {
        if ($admin) {
          return view('admin.comments',['api_token'=>$api_token]);
        }
        else {
          return view('user.Forbidden');
        }

      }
      else {
        return view('user.Inactive');
      }
    //   $restId = 'all';
    //   $date1 =$date2= $this->date();
    //   $minSatisf = 0;
    //   $maxSatisf = 10;
    //   $Quests = Questionnaire::where('data.stamp', '>=', $date1.' 00:00')->where('data.stamp', '<=',$date2.' 23:59')
    //                                                             ->where('data.isCancelled', '=',false)->get();
    //   $stats = app()->make('GetQuestData')->generalData($date1.' 00:00',$date2.' 23:59',$restId,$Quests);
    //   $comments = app()->make('GetQuestData')->commentsData($minSatisf,$maxSatisf,$Quests);
    //   return view('admin.comments',['stats' => $stats,'comments' => $comments,'date1' => $date1,'date2' => $date2,'min'=>$minSatisf,'max'=>$maxSatisf]);

     }

    public function settings()
    {
      $api_token = auth()->user()->api_token;
      $active = auth()->user()->active;
      $confirmed = auth()->user()->confirmed;
      $admin = auth()->user()->isAdmin;
      if ($active && $confirmed) {
        if ($admin) {
          return view('admin.settings',['api_token'=>$api_token]);
        }
        else {
          return view('user.Forbidden');
        }

      }
      else {
        return view('user.Inactive');
      }
    }

    public function date()
    {
      date_default_timezone_set('Europe/Athens');
       setlocale(LC_TIME, 'el_GR.UTF-8');
       return date("Y-m-d");
    }
    // public function advSearch()
    // {
    //   $result = app()->make('GetAdvancedData')->searchData();
    //   return view('admin.advancedSearch',$result);
    // }
    //
    // public function PostadvSearch(Request $request)
    // {
    //   $result = app()->make('GetAdvancedData')->generalData($request);
    //   if (empty($result['errors'])) {
    //   return view('admin.advanced',$result);
    //   }
    //   else {
    //   return view('admin.advancedSearch',$result);
    //   }
    //  }
    //
    //
    // public function mails()
    // {
    //   // $date1 =$date2= $this->date();
    //   // $Mailstats = app()->make('GetMailData')->getMails($date1.' 00:00',$date2.' 23:59');
    //   // return view('admin.mails',['Mailstats'=>$Mailstats,'date1' => $date1,'date2' => $date2]);
    //   $tempObj = app()->make('GetMailData')->getMails();
    //   return view('admin.mails',['tempObj'=>$tempObj]);
    // }
    //
    // public function storemails(Request $request)
    // {
    //   // $date1 = $request->input('startDate');
    //   // $date2 = $request->input('endDate');
    //   // $Mailstats = app()->make('GetMailData')->getMails($date1.' 00:00',$date2.' 23:59');
    //   // return view('admin.mails',['Mailstats'=>$Mailstats,'date1' => $date1,'date2' => $date2]);
    //   $tempObj = app()->make('GetMailData')->storeMails($request);
    //   return view('admin.mails',['tempObj'=>$tempObj]);
    // }
    //
    // public function getCanceledQuests()
    // {
    //   $date1 =$date2= $this->date();
    //   $cancelData = app()->make('CancelQuests')->cancelledData($date1.' 00:00',$date2.' 23:59');
    //   return view('admin.cancel',['cancelData'=>$cancelData,'date1' => $date1,'date2' => $date2]);
    // }
    //
    // public function CancelQuest(Request $request)
    // {
    //   if ($request->input('flag')== 'cancel') {
    //     $date1 = $request->input('startDate');
    //     $date2 = $request->input('endDate');
    //     $questId= intval($request->input('cancelId'));
    //     $cancel = app()->make('CancelQuests')->cancelQuest($questId);
    //     $cancelData = app()->make('CancelQuests')->cancelledData($date1.' 00:00',$date2.' 23:59');
    //     return view('admin.cancel',['cancelData'=>$cancelData,'date1' => $date1,'date2' => $date2]);
    //   }
    //   elseif ($request->input('flag')== 'search') {
    //     $date1 = $request->input('startDate');
    //     $date2 = $request->input('endDate');
    //     $cancelData = app()->make('CancelQuests')->cancelledData($date1.' 00:00',$date2.' 23:59');
    //     return view('admin.cancel',['cancelData'=>$cancelData,'date1' => $date1,'date2' => $date2]);
    //   }
    //
    // }
    //
    // public function Quests()
    // {
    //   $tempObj = app()->make('Questions')->getQuests();
    //   return view('admin.quests',['tempObj'=>$tempObj]);
    // }
    // public function addQuests(Request $request)
    // {
    //   $tempObj = app()->make('Questions')->addQuests($request);
    //   return view('admin.quests',['tempObj'=>$tempObj]);
    // }




}
