<?php

namespace App\Http\Controllers;
use App\Services\Questionnaires\Models\Questionnaire;
use Illuminate\Http\Request;
use App\User;
use Auth;
use DateTime;

class UserApiStatsController extends Controller
{
    public function aggregated(Request $request)
    {

        $response = new \stdClass;
        $response->data = array();

        $user = User::where('api_token','=',$request->input('api_token'))->first();
        $user_restaurants = $user->Restaurants;

        if (in_array(intval($request->input('restaurant')),$user_restaurants)) {
          if ($request->input('compare')=='false') {
            $date1 = $request->input('startDate').' 00:01';
            $date2 = $request->input('endDate').' 23:59';
            $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                      ->where('data.isCancelled', '=',false)
                                                                      ->where('data.isComplete', '=',true)
                                                                      ->where('data.DBquests','=',true)
                                                                      ->where('rest_id','=',intval($request->input('restaurant')))
                                                                      ->get();
            $all =  Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                      ->where('data.isCancelled', '=',false)
                                                                      ->where('rest_id','=',intval($request->input('restaurant')))
                                                                      ->count();

            $stats = app()->make('GetQuestData')->generalData($date1,$date2,$Quests,$all);
            $flagCompare = false;
            $response->data['stats']=$stats;
          }
          else {

            $date1 = $request->input('startDate').' 00:01';
            $date2 = $request->input('endDate').' 23:59';
            $date3 = $request->input('CompstartDate').' 00:01';
            $date4 = $request->input('CompendDate').' 23:59';
            $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                      ->where('data.isCancelled', '=',false)
                                                                      ->where('data.isComplete', '=',true)
                                                                      ->where('data.DBquests','=',true)
                                                                      ->where('rest_id','=',intval($request->input('restaurant')))
                                                                      ->get();

            $all =  Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                      ->where('data.isCancelled', '=',false)
                                                                      ->where('rest_id','=',intval($request->input('restaurant')))
                                                                      ->count();

            $comQuests = Questionnaire::where('stamp', '>=', new DateTime($date3))->where('stamp', '<=',new DateTime($date4))
                                                                      ->where('data.isCancelled', '=',false)
                                                                      ->where('data.isComplete', '=',true)
                                                                      ->where('data.DBquests','=',true)
                                                                      ->get();

            $compall = Questionnaire::where('stamp', '>=', new DateTime($date3))->where('stamp', '<=',new DateTime($date4))
                                                                      ->where('data.isCancelled', '=',false)
                                                                      ->count();

            $stats = app()->make('GetQuestData')->generalData($date1,$date2,$Quests,$all);
            $compStats = app()->make('GetQuestData')->generalData($date3,$date4,$comQuests,$compall);
            $flagCompare = true;
            $response->data['stats']=$stats;
            $response->data['compStats']=$compStats;
          }
        }
        else {
          $response->message = 'Δεν έχετε πρόσβαση στο συγκεκριμένο κατάστημα';
        }
        return response()->json($response, 200);
    }

    public function detailedData(Request $request)
    {
      $response = new \stdClass;
      $response->data = array();

      $user = User::where('api_token','=',$request->input('api_token'))->first();
      $user_restaurants = $user->Restaurants;

      if (in_array(intval($request->input('restaurant')),$user_restaurants)) {
        $date1 = $request->input('startDate').' 00:01';
        $date2 = $request->input('endDate').' 23:59';
        $minSatisf = $request->input('minSatisf');
        $maxSatisf = $request->input('maxSatisf');

        $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                  ->where('data.isCancelled', '=',false)
                                                                  ->where('data.isComplete', '=',true)
                                                                  ->where('data.DBquests','=',true)
                                                                  ->where('rest_id','=',intval($request->input('restaurant')))
                                                                  ->get();

        $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                  ->where('data.isCancelled', '=',false)
                                                                  ->where('rest_id','=',intval($request->input('restaurant')))
                                                                  ->count();

        $stats = app()->make('GetQuestData')->Aggregated($date1,$date2,$Quests,$all);
        $details = app()->make('GetQuestData')->detailsData($minSatisf,$maxSatisf,$Quests);

        $response->data['stats']=$stats->general;
        $response->data['details']=$details;

      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση στο συγκεκριμένο κατάστημα';
      }
      return response()->json($response, 200);
    }

    public function Comments(Request $request)
    {
      $response = new \stdClass;
      $response->data = array();

      $user = User::where('api_token','=',$request->input('api_token'))->first();
      $user_restaurants = $user->Restaurants;

      if (in_array(intval($request->input('restaurant')),$user_restaurants)) {
        $date1 = $request->input('startDate').' 00:01';
        $date2 = $request->input('endDate').' 23:59';
        $minSatisf = $request->input('minSatisf');
        $maxSatisf = $request->input('maxSatisf');
        $Quests = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                  ->where('data.isCancelled', '=',false)
                                                                  ->where('data.isComplete', '=',true)
                                                                  ->where('data.DBquests','=',true)
                                                                  ->where('rest_id','=',intval($request->input('restaurant')))
                                                                  ->get();

        $all = Questionnaire::where('stamp', '>=', new DateTime($date1))->where('stamp', '<=',new DateTime($date2))
                                                                  ->where('data.isCancelled', '=',false)
                                                                  ->where('rest_id','=',intval($request->input('restaurant')))
                                                                  ->count();

        $stats = app()->make('GetQuestData')->Aggregated($date1,$date2,$Quests,$all);
        $comments = app()->make('GetQuestData')->commentsDataUser($minSatisf,$maxSatisf,$Quests);

        $response = new \stdClass;
        $response->data = array();
        $response->data['stats']=$stats->general;
        $response->data['comments']=$comments;

      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση στο συγκεκριμένο κατάστημα';
      }
      return response()->json($response, 200);

    }

    public function UserRestaurants(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();
      $user_restaurants = $user->Restaurants;
      $all_restaurants = $this->RestaurantPicker();
      $Restaurants = array();

      foreach ($user_restaurants as $user_rest) {
        if (isset($all_restaurants[$user_rest])) {
          $Restaurants[$user_rest] = $all_restaurants[$user_rest];
        }
      }
        $response->restaurants = $Restaurants;
        return response()->json($response, 200);
    }

    public function RestaurantPicker()
    {
      $restrs = array();
      $file = file_get_contents('../app/Services/Orders/JsonData/restaurants.json');
      $obj = json_decode($file);
      foreach ($obj->data as $rest) {
        $restrs[$rest->restaurant_id] = $rest->name;
      }
      asort($restrs);
      return $restrs;
    }

    public function date()
    {
      date_default_timezone_set('Europe/Athens');
       setlocale(LC_TIME, 'el_GR.UTF-8');
       return date("Y-m-d");
    }

    // public function RestaurantsStats(Request $request)
    // {
    //   $date1 = $request->input('startDate');
    //   $date2 = $request->input('endDate');
    //   // $minSatisf = $request->input('minSatisf');
    //   // $maxSatisf = $request->input('maxSatisf');
    //   $restId = 'all';
    //   $restaurants = app()->make('GetAdvancedData')->RestaurantPicker();
    //   $RestStats = array();
    //   $tempDel = file_get_contents('../app/Services/QuestionnaireControl/JsonData/tempDel1.json');
    //   $DelObj = json_decode($tempDel);
    //   $DelProps = array();
    //   foreach ($DelObj->data->questions->indicators as $indicator) {
    //     $DelProps[$indicator->questlabel]= $indicator->value;
    //   }
    //   $tempTake = file_get_contents('../app/Services/QuestionnaireControl/JsonData/tempTake1.json');
    //   $TakeObj = json_decode($tempTake);
    //   $TakeProps = array();
    //   foreach ($TakeObj->data->questions->indicators as $indicator) {
    //     $TakeProps[$indicator->questlabel]= $indicator->value;
    //   }
    //
    //   foreach ($restaurants as $key => $value) {
    //     $obj = new \stdClass;
    //     $obj->rest_name = $value;
    //     $Quests = Questionnaire::where('data.stamp', '>=', $date1.' 00:00')->where('data.stamp', '<=',$date2.' 23:59')
    //                                                               ->where('data.isCancelled', '=',false)
    //                                                               ->where('rest_id','=',$key)
    //                                                               ->get();
    //     $stats = app()->make('GetQuestData')->generalData($date1.' 00:00',$date2.' 23:59',$restId,$Quests);
    //     $obj->Συμπληρωμένα = $stats->general->completed;
    //     $obj->Delivery_Ικανοποίηση = $stats->general->delScore;
    //     $obj->Take_Ικανοποίηση = $stats->general->takeScore;
    //     if (empty($stats->DelAvgIndicators)) {
    //      foreach ($DelProps as $key => $value) {
    //        $property = $key .' Delivery';
    //        $obj->$property = 0;
    //      }
    //     }
    //     else {
    //       foreach ($stats->DelAvgIndicators as $quest) {
    //
    //         $property = $quest->questlabel .' Delivery';
    //
    //         $obj->$property = $quest->avg;
    //         // $questions[$quest->questlabel]=$quest->avg;
    //       }
    //     }
    //
    //     if (empty($stats->TakeAvgIndicators)) {
    //       foreach ($TakeProps as $key => $value) {
    //         $property = $key .' Take';
    //         $obj->$property = 0;
    //       }
    //     }
    //     else {
    //       foreach ($stats->TakeAvgIndicators as $quest) {
    //         $property = $quest->questlabel .' Take';
    //         $obj->$property = $quest->avg;
    //       }
    //     }
    //     array_push($RestStats,$obj);
    //   }
    //   $response = new \stdClass;
    //   $response->data = $RestStats;
    //   return response()->json($response, 200);
    // }
}
