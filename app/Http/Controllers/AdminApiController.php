<?php

namespace App\Http\Controllers;
//use App\Services\Questionnaires\Models\Questionnaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use  App\Services\Mail\Models\Restaurant;
use App\Services\QuestionnaireControl\Models\Question;
use App\User;


class AdminApiController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth:admin');
        $this->middleware('auth');
    }

//Calls for Users
    public function RetrieveUsers(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();
      $users = User::where('isAdmin','=',false)->
                      where('Moderator','=',false)->get();
      $data = array();

      if ($user->isAdmin) {
        if (empty($users)) {
        $response->user_data = array();
        }
        else {
          foreach ($users as $user) {
            $obj = new \stdClass;
            $obj->name = $user->name;
            $obj->surname = $user->surname;
            $obj->email = $user->email;
            $obj->number = $user->number;
            if ($user->active) {
              $obj->active = 'Ενεργός';
            }
            else{
              $obj->active = 'Μη Ενεργός';
            }

            if ($user->confirmed) {
              $obj->confirmed = 'Επιβεβαιωμένος';
            }
            else {
              $obj->confirmed = 'Μη Επιβεβαιωμένος';
            }

            if ($user->isArea) {
              $obj->area = 'Ναί';
            }
            else {
              $obj->area = 'Όχι';
            }

            $RestString = '';
             foreach ($user->Restaurants as $item) {
                foreach ($this->RestaurantPicker() as $rest_obj) {
                    if ($item == $rest_obj->id) {
                     $RestString .= $rest_obj->name.'<br>';
                    }
                }
             }
           $obj->Restaurants = $RestString;

           if ($user->Νotifications) {
            $obj->notify = 'Ναί';
           }
           else {
            $obj->notify = 'Όχι';
           }

           $emailString = '';
            foreach ($user->other_emails as $item) {
              $emailString .= $item.'<br>';
              }
            $obj->emails = $emailString;


            array_push($data,$obj);
          }
          $response->user_data = $data;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }

      return response()->json($response, 200);
    }

    public function RetrieveAdmins(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();
      $users = User::where('isAdmin','=',true)->get();
      $data = array();

      if ($user->isAdmin) {
        if (empty($users)) {
        $response->user_data = array();
        }
        else {
          foreach ($users as $user) {
            $obj = new \stdClass;
            $obj->name = $user->name;
            $obj->surname = $user->surname;
            $obj->email = $user->email;
            $obj->number = $user->number;
            if ($user->active) {
              $obj->active = 'Ενεργός';
            }
            else{
              $obj->active = 'Μη Ενεργός';
            }

            if ($user->confirmed) {
              $obj->confirmed = 'Επιβεβαιωμένος';
            }
            else {
              $obj->confirmed = 'Μη Επιβεβαιωμένος';
            }

            if ($user->Νotifications) {
             $obj->notify = 'Ναί';
            }
            else {
             $obj->notify = 'Όχι';
            }

           $emailString = '';
            foreach ($user->other_emails as $item) {
              $emailString .= $item.'<br>';
              }
            $obj->emails = $emailString;


            array_push($data,$obj);
          }
          $response->user_data = $data;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }

      return response()->json($response, 200);
    }

    public function RetrieveMods(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();
      $users = User::where('Moderator','=',true)->get();
      $data = array();

      if ($user->isAdmin) {
        if (empty($users)) {
        $response->user_data = array();
        }
        else {
          foreach ($users as $user) {
            $obj = new \stdClass;
            $obj->name = $user->name;
            $obj->surname = $user->surname;
            $obj->email = $user->email;
            $obj->number = $user->number;
            if ($user->active) {
              $obj->active = 'Ενεργός';
            }
            else{
              $obj->active = 'Μη Ενεργός';
            }

            if ($user->confirmed) {
              $obj->confirmed = 'Επιβεβαιωμένος';
            }
            else {
              $obj->confirmed = 'Μη Επιβεβαιωμένος';
            }

            if ($user->Νotifications) {
             $obj->notify = 'Ναί';
            }
            else {
             $obj->notify = 'Όχι';
            }

           $emailString = '';
            foreach ($user->other_emails as $item) {
              $emailString .= $item.'<br>';
              }
            $obj->emails = $emailString;


            array_push($data,$obj);
          }
          $response->user_data = $data;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }

      return response()->json($response, 200);
    }

    public function AddUserRestaurants(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();
      if ($user->isAdmin) {
        $check = $this->MailUserValidate($request);
        if ($check->success) {

          $check2 = $this->UserActiveAndConfirmed($request->input('email'));
          if ($check2->success) {
            $user =  new User;
            if ($user->AddRestaurants($request->input('email'),$request->input('restaurants'))) {
              $response->success = true;
              $response->message= 'Το εστιατόριο προστέθηκε';
              return response()->json($response, 200);
            }
            else {
              $response->success = false;
              $response->message= 'Τα εστιατόριο υπάρχει ήδη';
              return response()->json($response, 200);
            }
          }
          else {
            $response->success = false;
            $response->message= $check2->message;
          }

        }
        else {
          $response->success = false;
          $response->message= $check->message;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }

       return response()->json($response, 200);

    }

    public function DeleteUserRestaurants(Request $request)
    {

      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $check = $this->MailUserValidate($request);
        if ($check->success) {
          $check2 = $this->UserActiveAndConfirmed($request->input('email'));
          if ($check2->success) {
            $user =  new User;
            if ($user->DeleteRestaurants($request->input('email'),$request->input('restaurants'))) {
              $response->success = true;
              $response->message= 'Το εστιατόριο αφαιρέθηκε';
              return response()->json($response, 200);
            }
            else {
              $response->success = false;
              $response->message= 'Τα εστιατόριο δεν υπάρχει';
              return response()->json($response, 200);
            }
          }
          else {
            $response->success = false;
            $response->message= $check2->message;
          }
        }
        else {
          $response->success = false;
          $response->message= $check->message;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }

       return response()->json($response, 200);

    }

    public function EnableUser(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $check = $this->MailUserValidate($request);
        if ($check->success) {
          $user =  new User;
          $status = $user->Enable($request->input('email'));
          if ($status->success) {
            $response->success = true;
            $response->message= 'Ο χρήστης ενεργοποιήθηκε';
          }
          else {
            $response->success = false;
            $response->message= $status->message;
          }
        }
        else {
          $response->success = false;
          $response->message= $check->message;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }

       return response()->json($response, 200);

    }

    public function DisableUser(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $check = $this->MailUserValidate($request);
        if ($check->success) {
          $user =  new User;
          $status = $user->Disable($request->input('email'));
          if ($status->success) {
            $response->success = true;
            $response->message= 'Ο χρήστης απενεργοποιήθηκε';
          }
          else {
            $response->success = false;
            $response->message= $status->message;
          }
        }
        else {
          $response->success = false;
          $response->message= $check->message;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }

      return response()->json($response, 200);
    }

    public function ConfirmUser(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $check = $this->MailUserValidate($request);
        if ($check->success) {
          $user =  new User;
          if ($status = $user->Confirm($request->input('email'))) {
            $response->success = true;
            $response->message= 'Ο χρήστης Επιβεβαιώθηκε';
          }
          else {
            $response->success = false;
            $response->message= 'Ο χρήστης είναι ήδη Επιβεβαιωμένος';
          }
        }
        else {
          $response->success = false;
          $response->message= $check->message;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }

       return response()->json($response, 200);
    }


    public function AddUserEmails(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $check = $this->MailUserValidate($request);
        $email = $request->input('email');
        $other_email = $request->input('other_email');
        if ($check->success) {
          $check2 = $this->UserActiveAndConfirmed($email);

          if ($check2->success) {
            $user = new User;
            if ($user->AddEmails($email,$other_email)) {
                $response->success = true;
                $response->message= 'Το δευτερεύον e-mail προστέθηκε';
            }
            else {
              $response->success = false;
              $response->message= 'Το e-mail υπάρχει ήδη';
            }
          }
          else {
            $response->success = false;
            $response->message= $check2->message;
          }
        }
        else {
          $response->success = false;
          $response->message= $check->message;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }

      return response()->json($response, 200);
    }

    public function DeleteUserEmails(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $check = $this->MailUserValidate($request);
        $email = $request->input('email');
        $other_email = $request->input('other_email');
        if ($check->success) {
          $check2 = $this->UserActiveAndConfirmed($email);

          if ($check2->success) {
            $user = new User;
            if ($user->RemoveEmails($email,$other_email)) {
                $response->success = true;
                $response->message= 'Το δευτερεύον e-mail αφαιρέθηκε';
            }
            else {
              $response->success = false;
              $response->message= 'Το e-mail δεν υπάρχει';
            }
          }
          else {
            $response->success = false;
            $response->message= $check2->message;
          }
        }
        else {
          $response->success = false;
          $response->message= $check->message;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }

      return response()->json($response, 200);
    }

    public function EnableArea(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $check = $this->MailUserValidate($request);
        $email = $request->input('email');
        if ($check->success) {
          $check2 = $this->UserActiveAndConfirmed($email);
          if ($check2->success) {
            $user = new User;
            if ($user->EnableArea($email)) {
              $response->success = true;
              $response->message= 'Ο Area ενεργοποιήθηκε';
            }
            else {
              $response->success = false;
              $response->message= 'Ο Χρήστης είναι ήδη Area';
            }
          }
          else {
            $response->success = false;
            $response->message= $check2->message;
          }
        }
        else {
          $response->success = false;
          $response->message= $check->message;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }
      return response()->json($response, 200);
    }

    public function EnableMod(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $check = $this->MailUserValidate($request);
        $email = $request->input('email');
        if ($check->success) {
          $check2 = $this->UserActiveAndConfirmed($email);
          if ($check2->success) {
            $user = new User;
            if ($user->EnableMod($email)) {
              $response->success = true;
              $response->message= 'Ο Moderator ενεργοποιήθηκε';
            }
            else {
              $response->success = false;
              $response->message= 'Ο Χρήστης είναι ήδη Moderator';
            }
          }
          else {
            $response->success = false;
            $response->message= $check2->message;
          }
        }
        else {
          $response->success = false;
          $response->message= $check->message;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }
      return response()->json($response, 200);
    }

    public function DisableArea(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $check = $this->MailUserValidate($request);
        $email = $request->input('email');
        if ($check->success) {
          $check2 = $this->UserActiveAndConfirmed($email);
          if ($check2->success) {
            $user = new User;
            $check3 = $user->DisableArea($email);
            if ($check3->success) {
              $response->success = true;
              $response->message= $check3->message;
            }
            else {
              $response->success = false;
              $response->message= $check3->message;
            }
          }
          else {
            $response->success = false;
            $response->message= $check2->message;
          }
        }
        else {
          $response->success = false;
          $response->message= $check->message;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }


      return response()->json($response, 200);

    }

    public function MakeAdmin(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $check = $this->MailUserValidate($request);
        $email = $request->input('email');
        if ($check->success) {
          $check2 = $this->UserActiveAndConfirmed($email);
          if ($check2->success) {
            $user = new User;
            $check3 = $user->MakeAdmin($email);
            if ($check3->success) {
              $response->success = true;
              $response->message= $check3->message;
            }
            else {
              $response->success = false;
              $response->message= $check3->message;
            }
          }
          else {
            $response->success = false;
            $response->message= $check2->message;
          }
        }
        else {
          $response->success = false;
          $response->message= $check->message;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }


      return response()->json($response, 200);
    }

    public function ChangeUserNotify(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $check = $this->MailUserValidate($request);
        $email = $request->input('email');
        if ($check->success) {
          $check2 = $this->UserActiveAndConfirmed($email);
          if ($check2->success) {
            $user = new User;
            $check3 = $user->ChangeNotify($email);

            if ($check3) {
              $response->success = true;
              $response->message= 'Οι ειδοποιήσεις ενεργοποιήθηκαν';
            }
            else {
              $response->success = true;
              $response->message= 'Οι ειδοποιήσεις απενεργοποιήθηκαν';
            }
          }
          else {
            $response->success = false;
            $response->message= $check2->message;
          }
        }
        else {
          $response->success = false;
          $response->message= $check->message;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }


      return response()->json($response, 200);
    }


    public function MailUserValidate($request)
    {
      $check= new \stdClass;
      $validator = Validator::make($request->all(), [
        'email' => 'required|string|email|max:255',
      ]);
      if ($validator->fails()) {
        $check->success = false;
        $check->message= 'Η δομή του email δεν είναι σωστή';
        }
      else {
        $user =  new User;

        if ($user->where('email','=',$request->input('email'))->first()) {
          $check->success = true;
        }
        else {
          $check->success = false;
          $check->message= 'Δεν υπάρχει χρήστης με email '.$request->input('email');
        }

      }
      return $check;
    }

    public function UserActiveAndConfirmed($email)
    {
        $check= new \stdClass;

        $user = User::where('email','=',$email)->first();
        if (!$user->confirmed) {
          $check->success = false;
          $check->message= 'Ο χρήστης δεν έχει επιβεβαιωθεί';
        }
        elseif (!$user->active) {
          $check->success = false;
          $check->message= 'Ο χρήστης δεν έχει ενεργοποιηθεί';
        }
        else {
          $check->success = true;
        }

        return $check;

    }

    public function RetrieveRestEmails(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();
      $Rest_emails = Restaurant::all();
      $data = array();

      if ($user->isAdmin) {
        if (empty($Rest_emails)) {
        $response->emails_data = array();
        }
        else {
          foreach ($Rest_emails as $rest_email) {
            $obj = new \stdClass;

            foreach ($this->RestaurantPicker() as $rest_obj) {
                if ($rest_email->rest_id == $rest_obj->id) {
                  $obj->id = $rest_obj->name;
                  break;
                }
            }
            $obj->email = $rest_email->email;

            if ($rest_email->notify) {
              $obj->notify = 'Ενεργές';
            }
            else{
              $obj->notify = 'Μη Ενεργές';
            }
            array_push($data,$obj);
          }
          $response->emails_data = $data;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }

      return response()->json($response, 200);
    }

    public function CreateRestEmail(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $rest_id = intval($request->input('restaurant'));
        $Restaurant = new Restaurant;

         $create = $Restaurant->Create($rest_id,$request->input('email'));
        if ($create->success) {
          $response->success = true;
          $response->message= $create->message;
        }
        else {
          $response->success = false;
          $response->message= $create->message;
        }

      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }

      return response()->json($response, 200);
    }

    public function ChangeRestNotify(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $rest_id = intval($request->input('restaurant'));
        $Restaurant = new Restaurant;
        $change = $Restaurant->ChangeNotify($rest_id);
        if ($change->success) {
          $response->success = true;
          $response->message= $change->message;
        }
        else {
          $response->success = false;
          $response->message= $change->message;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }
      return response()->json($response, 200);
    }

    public function UpdateRestEmail(Request $request)
    {
      $response = new \stdClass;
      $user = User::where('api_token','=',$request->input('api_token'))->first();

      if ($user->isAdmin) {
        $rest_id = intval($request->input('restaurant'));
        $Restaurant = new Restaurant;
        $update = $Restaurant->UpdateEmail($rest_id,$request->input('email'));
        if ($update->success) {
          $response->success = true;
          $response->message= $update->message;
        }
        else {
          $response->success = false;
          $response->message= $update->message;
        }
      }
      else {
        $response->message = 'Δεν έχετε πρόσβαση';
      }
      return response()->json($response, 200);
    }



    public function Restaurants()
    {
      $response = new \stdClass;
      $response->restaurants = $this->RestaurantPicker();
      return response()->json($response, 200);
    }

    public function RestaurantPicker()
    {
      $restrs = array();
      $file = file_get_contents('../app/Services/Orders/JsonData/restaurants.json');
      $rest_obj = json_decode($file);
      foreach ($rest_obj->data as $rest) {
        $obj = new \stdClass;
        $obj->id = $rest->restaurant_id;
        $obj->name = $rest->name;
        array_push($restrs,$obj);
      }
      usort($restrs, function($a, $b)
      {
          return strcmp($a->name, $b->name);
      });
      return $restrs;
    }

}
