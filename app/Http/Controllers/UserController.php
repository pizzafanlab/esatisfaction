<?php

namespace App\Http\Controllers;
use App\Services\Questionnaires\Models\Questionnaire;
use Illuminate\Http\Request;
use Auth;
use App\User;

class UserController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth:admin');
        $this->middleware('auth');
    }

    public function stats()
    {
      $api_token = auth()->user()->api_token;
      $confirmed= auth()->user()->confirmed;
      $active = auth()->user()->active;
      if ($active && $confirmed) {
        return view('user.stats',['api_token'=>$api_token]);
      }
      else {
        return view('user.Inactive');
      }

    }

    public function details()
    {
      $api_token = auth()->user()->api_token;
      $confirmed= auth()->user()->confirmed;
      $active = auth()->user()->active;
      if ($active && $confirmed) {
        return view('user.details',['api_token'=>$api_token]);
      }
      else {
        return view('user.Inactive');
      }
    }


    public function comments()
    {
      $api_token = auth()->user()->api_token;
      $confirmed= auth()->user()->confirmed;
      $active = auth()->user()->active;
      if ($active && $confirmed) {
        return view('user.comments',['api_token'=>$api_token]);
      }
      else {
        return view('user.Inactive');
      }

     }

    // public function advSearch()
    // {
    //   $api_token = auth()->user()->api_token;
    //   $active = auth()->user()->confirmed;
    //   if ($active) {
    //     $result = app()->make('GetAdvancedData')->searchData();
    //     return view('user.advancedSearch',$result);;
    //   }
    //   else {
    //     return view('user.Inactive');
    //   }
    //
    // }
    //
    // public function PostadvSearch(Request $request)
    // {
    //
    //   $api_token = auth()->user()->api_token;
    //   $active = auth()->user()->confirmed;
    //   if ($active) {
    //     $result = app()->make('GetAdvancedData')->generalData($request);
    //     if (empty($result['errors'])) {
    //     return view('user.advanced',$result);
    //     }
    //     else {
    //     return view('user.advancedSearch',$result);
    //     }
    //   }
    //   else {
    //     return view('user.Inactive');
    //   }
    //
    //  }

    public function date()
    {
      date_default_timezone_set('Europe/Athens');
       setlocale(LC_TIME, 'el_GR.UTF-8');
       return date("Y-m-d");
    }
}
