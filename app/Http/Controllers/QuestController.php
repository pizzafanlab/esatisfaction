<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Questionnaires\Models\Questionnaire;
use App\Services\QuestionnaireControl\Models\Question;
use App\Mail\Welcome;

class QuestController extends Controller
{


    public function store(Request $request)
    {
       $result = new Questionnaire;
        //$result = Questionnaire::where('data.quest_id','=',$request->input('id'))->first();
        $result = Questionnaire::where('data.quest_id','=',intval($request->input('id')))->first();
         $hash = $request->input('hash');

        if (!$result) {
          return response()->json([
               'error' => 'Το ερωτηματολόγιο δεν βρέθηκε'
           ], 404);
          }

        else{
             $quest = json_decode(json_encode($result));

             if($hash == $quest->data->hash){

                if ($quest->data->isComplete) {
                  return response()->json([
                       'error' => 'Το ερωτηματολόγιο έχει ήδη συμπληρωθεί'
                   ], 200);
                }
                else{
                      $errors = array();
                      $requests = array();
                      $requests['products']=$request->input('prodSelect');
                      $requests['productsComments']=$request->input('prodtext');
                      $requests['indicators']=$request->input('indicatorsSelect');
                      $requests['generals']=$request->input('generalSelect');
                      if($quest->data->order_type == 'take'){
                         $requests['comment']=$request->input('takeComment');
                         $Satisf = Question::where('category','=','general')
                                           ->where('method','=','take')
                                           ->where('flag','=','satisf')
                                           ->where('status','=',true)->first();
                      }
                      else{
                        $requests['select']=$request->input('delSelect');
                        $requests['comment']=$request->input('delComment');
                        $Satisf = Question::where('category','=','general')
                                          ->where('method','=','delivery')
                                          ->where('flag','=','satisf')
                                          ->where('status','=',true)->first();
                      }
                        $requests['generalcomment']=$request->input('genComment');
                        if(array_key_exists(0,$requests['generals'])){
                          if ($requests['generals'][0] >=1 && $requests['generals'][0] <=10 ) {
                            $save = app()->make('FiledQuest')->updateold($result,$requests,$quest);
                            return view('email.thank');
                          }
                          else {
                            array_push($errors,'Η ερώτηση για την γενική ικανοποίηση είναι υποχρεωτική');
                            return view('Questionnaire',['quest' => $quest,'errors' => $errors]);
                          }
                        }
                        else {
                          if ($requests['generals'][$Satisf->_id] >=1 && $requests['generals'][$Satisf->_id] <=10 ) {
                            $save = app()->make('FiledQuest')->update($result,$requests,$quest);
                            return view('email.thank');
                          }
                          else {
                            array_push($errors,'Η ερώτηση για την γενική ικανοποίηση είναι υποχρεωτική');
                            return view('Questionnaire',['quest' => $quest,'errors' => $errors]);
                          }
                        }

                }

            }
            else{
              return response()->json([
                   'error' => 'Δεν έχετε δικαίωμα πρόσβασης στο ερωτηματολόγιο'
               ], 403);
            }
         }
    }

    public function show(Request $request)
    {
      $result = Questionnaire::where('data.quest_id','=',intval($request->input('id')))->first();
      $hash = $request->input('hash');

      if (!$result) {
        return response()->json([
             'error' => 'Το ερωτηματολόγιο δεν βρέθηκε'
         ], 404);
        }
        else{
          $quest = json_decode(json_encode($result));
          if($hash == $quest->data->hash){
              if ($quest->data->isComplete) {
                $date = $result->stamp->setTimezone('Europe/Athens');
                $date = $date->format("Y-m-d H:i");
                return view('filledQuest',['quest' => $quest,'date' => $date]);
              }
              elseif ($quest->data->isCancelled) {
                return response()->json([
                     'error' => 'Το ερωτηματολόγιο έχει ακυρωθεί'
                 ], 200);
              }
              else{
                $errors = array();
                return view('Questionnaire',['quest' => $quest,'errors' => $errors]);
              }

          }
          else{
            return response()->json([
                 'error' => 'Δεν έχετε δικαίωμα πρόσβασης στο ερωτηματολόγιο'
             ], 403);
          }
        }


    }

}
