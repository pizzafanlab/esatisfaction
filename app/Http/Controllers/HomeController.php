<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function redirect_user()
    {
      if (auth()->user()->isAdmin) {
        return redirect('/admin');
      }
      elseif (auth()->user()->Moderator) {
        return redirect('/mod');
      }
      else {
        return redirect('/user');
      }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
}
