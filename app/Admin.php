<?php

namespace App;

use Illuminate\Notifications\Notifiable;
 use Illuminate\Foundation\Auth\User as Authenticatable;
//use Jenssegers\Mongodb\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    protected $connection = 'mongodb';
    protected $collection = 'admins';

    protected $guard = 'admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
