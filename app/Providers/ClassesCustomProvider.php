<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ClassesCustomProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Products', \App\Services\Orders\Classes\Products::class);
        $this->app->bind('Restaurants', \App\Services\Orders\Classes\Restaurants::class);
        $this->app->bind('OrderPusher', \App\Services\Orders\Classes\OrderPusher::class);
        $this->app->bind('OrderStore',\App\Services\Orders\Classes\OrderStore::class);
        $this->app->bind('TemplateCreator', \App\Services\QuestionnaireControl\Classes\TemplateCreator::class);
        $this->app->bind('QuestStorePush', \App\Services\Questionnaires\Classes\QuestStorePush::class);
        $this->app->bind('storeMail', \App\Services\Mail\Classes\storeMail::class);
        $this->app->bind('checkMail', \App\Services\Mail\Classes\checkMail::class);
        $this->app->bind('sendMail', \App\Services\Mail\Classes\sendMail::class);
        $this->app->bind('MailsToUsers', \App\Services\Mail\Classes\MailsToUsers::class);
        $this->app->bind('FiledQuest', \App\Services\Questionnaires\Classes\FiledQuest::class);
        $this->app->bind('GetQuestData', \App\Services\Statistics\Classes\GetQuestData::class);
        $this->app->bind('GetMailData', \App\Services\Statistics\Classes\GetMailData::class);
        $this->app->bind('CancelQuests', \App\Services\Statistics\Classes\CancelQuests::class);
        $this->app->bind('Questions', \App\Services\Statistics\Classes\Questions::class);
        $this->app->bind('GetAdvancedData', \App\Services\Statistics\Classes\GetAdvancedData::class);
    }
}
